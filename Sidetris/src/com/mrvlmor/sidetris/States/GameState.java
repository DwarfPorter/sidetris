package com.mrvlmor.sidetris.States;

import org.andengine.ui.activity.BaseGameActivity;


import com.mrvlmor.machinestate.BaseState;
import com.mrvlmor.machinestate.BaseStateScene;
import com.mrvlmor.machinestate.IEventSink;
import com.mrvlmor.sidetris.Events;
import com.mrvlmor.sidetris.MainActivity;
import com.mrvlmor.sidetris.Resource.GameResource;
import com.mrvlmor.sidetris.Resource.SideTrisResource;
import com.mrvlmor.sidetris.Scene.GameScene;

public class GameState extends BaseState<Events> implements ISave{
	
	public GameState(IEventSink<Events> machineState, BaseGameActivity activity) {
		super(machineState, activity);
	}

	@Override
	public BaseStateScene createScene(BaseGameActivity activity) {
    	if(scene == null){
    		GameResource resources = new GameResource((MainActivity)activity);
    		scene = new GameScene(resources, this);
    		
       	}
    	else{
    		scene.startScene();
    	}

       	return scene;
	}
	
	public void save(){
		if (scene == null) return;
		((GameScene)scene).save();
	}

	
}
