package com.mrvlmor.sidetris.States;

import org.andengine.ui.activity.BaseGameActivity;

import com.mrvlmor.machinestate.BaseState;
import com.mrvlmor.machinestate.BaseStateScene;
import com.mrvlmor.machinestate.IEventSink;
import com.mrvlmor.sidetris.Events;
import com.mrvlmor.sidetris.MainActivity;
import com.mrvlmor.sidetris.Resource.SideTrisResource;
import com.mrvlmor.sidetris.Scene.MainMenuScene;

public class MainMenuState extends BaseState<Events> {

	
	public MainMenuState(IEventSink<Events> machineState, BaseGameActivity activity) {
		super(machineState, activity);
	}

	@Override
	public BaseStateScene createScene(BaseGameActivity activity) {
    	if(scene == null){
    		SideTrisResource resources = new SideTrisResource((MainActivity)activity);
    		scene = new MainMenuScene(resources, this);
       	}

       	return scene;
	}


}
