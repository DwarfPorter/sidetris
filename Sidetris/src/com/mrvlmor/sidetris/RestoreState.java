package com.mrvlmor.sidetris;

import com.mrvlmor.sidetris.Entity.FactorySpriteTile;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Entity.Tile.SpriteTile;

import android.app.Activity;
import android.content.SharedPreferences;

public class RestoreState {
	private SharedPreferences sharedPreferences;
	private Setting setting;
	
	public RestoreState(Activity activity){
		setting = new Setting();
		sharedPreferences =  activity.getSharedPreferences(setting.getSavePrefName(), android.content.Context.MODE_PRIVATE);
	}
	
    public void restoreVolumeSound(){
        setting.setVolumeSound(sharedPreferences.getFloat(setting.getVolumeSoundName(), setting.getVolumeSound()));
    }

    public void restoreVolumeMusic(){
        setting.setVolumeMusic(sharedPreferences.getFloat(setting.getVolumeMusicName(), setting.getVolumeMusic()));
    }
	public long restoreScore(){
		return sharedPreferences.getLong(setting.getScoreName(), 0);
	}
	
	public long restoreDouble(){
		return sharedPreferences.getLong(setting.getDoubleName(), 0);
	}
		
	public long restoreTriple(){
		return sharedPreferences.getLong(setting.getTripleName(), 0);
	}
	
	public long restoreQuadruple(){
		return sharedPreferences.getLong(setting.getQuadrupleName(), 0);
	}
	
	public long restoreQuintuple(){
		return sharedPreferences.getLong(setting.getQuintupleName(), 0);
	}
	
	public long restoreCombo(){
		return sharedPreferences.getLong(setting.getComboName(), 0);
	}

	public long restoreCombo(int i){
		return sharedPreferences.getLong(setting.getComboName(i), 0);
	}

	public long restoreCurrentCombo(){
		return sharedPreferences.getLong(setting.getCurrentComboName(), 0);
	}

	public long restoreCurrentCombo(int i){
		return sharedPreferences.getLong(setting.getCurrentComboName(i), 0);
	}

	public long restoreCurrentQuintuple(){
		return sharedPreferences.getLong(setting.getCurrentQuintupleName(), 0);
	}

	public long restoreCurrentQuadruple(){
		return sharedPreferences.getLong(setting.getCurrentQuadrupleName(), 0);
	}
	
	public long restoreHextuple(){
		return sharedPreferences.getLong(setting.getHextupleName(), 0);
	}	
	
	public long restoreCurrentHextuple(){
		return sharedPreferences.getLong(setting.getCurrentHextupleName(), 0);
	}
	
	public long restoreCurrentTriple(){
		return sharedPreferences.getLong(setting.getCurrentTripleName(), 0);
	}
	
	public long restoreCurrentDouble(){
		return sharedPreferences.getLong(setting.getCurrentDoubleName(), 0);
	}
	
	public long restoreCurrentScore(){
		return sharedPreferences.getLong(setting.getCurrentScoreName(), 0);
	}
	
	public int restoreCurrentState(){
		return sharedPreferences.getInt(setting.getCurrentStateName(), 0);
	}
	
	public long restoreCurrentLong(){
		return sharedPreferences.getLong(setting.getCurrentLongName(), 0);
	}
	
	
	public long restoreCurrentStep(){
		return sharedPreferences.getLong(setting.getCurrentStepName(), 0);
	}
	

	public long restoreLong(){
		return sharedPreferences.getLong(setting.getLongName(), 0);
	}
	
	public long restoreCurrentEmpty(){
		return sharedPreferences.getLong(setting.getCurrentEmptyName(), 0);
	}

	public long restoreLastChance(){
		return sharedPreferences.getLong(setting.getLastChanceName(), 0);
	}
	
	public long restoreCurrentLastChance(){
		return sharedPreferences.getLong(setting.getCurrentLastChanceName(), 0);
	}

	public long restoreEmpty(){
		return sharedPreferences.getLong(setting.getEmptyName(), 0);
	}

	public long restoreCurrentLevel(){
		return sharedPreferences.getLong(setting.getCurrentLevelName(), 0);
	}

	public long restoreLevel(){
		return sharedPreferences.getLong(setting.getLevelName(), 0);
	}

	public void restoreField(ISpriteTile[][] field, FactorySpriteTile factoryCellSprite){
		for(int X=0; X < field.length; X++){
			for(int Y=0; Y < field[X].length; Y++){
				if(field[X][Y] != null) field[X][Y].detachSelf();
				int number = sharedPreferences.getInt(setting.getFieldName(X, Y), 0);
				if (number == 0)
					field[X][Y] = null;
				else{
					TileType tile;
					switch(number){
					case 0: tile = TileType.NOTHING;  break;
					case 1: tile = TileType.BLUE; break;
					case 2: tile = TileType.GREEN; break;
					case 3: tile = TileType.RED; break;
					case 4: tile = TileType.YELLOW; break;
					case 5: tile = TileType.VIOLET; break;
					case 6: tile = TileType.CYAN; break;
					case 7: tile = TileType.ORANGE; break;
					case 8: tile = TileType.BLACK; break;
					case 9: tile = TileType.PINK; break;
					case 10: tile = TileType.DARKGREEN; break;
					case 11: tile = TileType.DARKBLUE; break;
					case 12: tile = TileType.BROWN; break;
					case 100: tile = TileType.BOMB; break;
					case 101: tile = TileType.SAW; break;
					case 102: tile = TileType.DIAMOND; break;
					case 103: tile = TileType.MINE; break;
                    case 104: tile=TileType.SPARK;break;
                    case 105: tile=TileType.BOUMERANG; break;
					default:
						tile = TileType.NOTHING; break;
					}
					
					ISpriteTile cell = factoryCellSprite.createSimpleSpriteTile(X, Y, tile); 
					field[X][Y] = cell;
					
				}
			}
		}
	}
	
}