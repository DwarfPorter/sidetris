package com.mrvlmor.sidetris.Scene;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.animator.DirectMenuAnimator;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import android.view.KeyEvent;

import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.machinestate.BaseScene;
import com.mrvlmor.machinestate.IEventSink;
import com.mrvlmor.sidetris.Events;
import com.mrvlmor.sidetris.MainActivity;
import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Resource.SideTrisResource;

public class MainMenuScene extends BaseScene<SideTrisResource, Events> implements IOnMenuItemClickListener {
	
	private final static int MENU_PLAY = 1;
	private final static int MENU_QUIT = 2;
	
	private MenuScene menuChildScene;
	
	public MainMenuScene(SideTrisResource resource, IEventSink<Events> state) {
		super(resource, state);
	}

	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		switch(pMenuItem.getID())
        {
        case MENU_PLAY:
        	castEvent(Events.PLAY);
            return true;
        case MENU_QUIT: 
    		exit();
            return true;
        default:
            return false;
        }
	}

	@Override
	public void startScene() {
		Setting setting = new Setting();
		
		Sprite background = new Sprite(0, -setting.getGameOffset(), this.getResource().getMainmenuBackground(), this.getActivity().getVertexBufferObjectManager());
		this.attachChild(background);
		
		Sprite windowTitle = new Sprite(setting.getHorizontalOffsetTitle(), setting.getVerticalOffsetTitle(), this.getResource().getWindow(), this.getActivity().getVertexBufferObjectManager());
		this.attachChild(windowTitle);
		
	    Text gameName = new Text(setting.getHorizontalOffsetTitle(), setting.getVerticalOffsetTitle(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.title_activity_main), getActivity().getVertexBufferObjectManager());
	    Text gameNameShadow = new Text(setting.getHorizontalOffsetTitle(), setting.getVerticalOffsetTitle(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.title_activity_main), getActivity().getVertexBufferObjectManager());

	    gameNameShadow.setColor(0, 0, 0);
	    
	    gameName.setColor(0, 1, 1);
	    
	    gameName.setX(ConvertCoord.toCenterTextX(windowTitle, gameName));
	    gameName.setY(ConvertCoord.toCenterTextY(windowTitle, gameName));
	    
	    gameNameShadow.setX(gameName.getX()+setting.getOffsetShadow());
	    gameNameShadow.setY(gameName.getY()+setting.getOffsetShadow());

	    this.attachChild(gameNameShadow);
	    this.attachChild(gameName);
		
		menuChildScene = new MenuScene(((MainActivity) getActivity()).getCamera());
	    menuChildScene.setPosition(0,0); //((int) GameActivity.CAMERA_WIDTH / 2, (int) GameActivity.CAMERA_HEIGHT / 2);
	    menuChildScene.setMenuAnimator(new DirectMenuAnimator(10f));
		
	    final SpriteMenuItem playMenuItem = new SpriteMenuItem(MENU_PLAY, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
	    final SpriteMenuItem quitMenuItem = new SpriteMenuItem(MENU_QUIT, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		
	    menuChildScene.addMenuItem(playMenuItem);
	    menuChildScene.addMenuItem(quitMenuItem);
	    
	    menuChildScene.buildAnimations();
	    menuChildScene.setBackgroundEnabled(false);
	    
//	    Sprite playBack = new Sprite(playMenuItem.getX(), playMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
//	    Sprite quitBack = new Sprite(quitMenuItem.getX(), quitMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
//	    
//	    menuChildScene.attachChild(playBack);
//	    menuChildScene.attachChild(quitBack);   
//	    
	    Text play = new Text(playMenuItem.getX(), playMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Play), getActivity().getVertexBufferObjectManager());
	    Text quit = new Text(quitMenuItem.getX(), quitMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Exit), getActivity().getVertexBufferObjectManager());

	    
	    
	    play.setX(ConvertCoord.toCenterTextX(playMenuItem, play));
	    play.setY(ConvertCoord.toCenterTextY(playMenuItem, play));
	    
	    quit.setX(ConvertCoord.toCenterTextX(quitMenuItem, quit));
	    quit.setY(ConvertCoord.toCenterTextY(quitMenuItem, quit));
	    
	    menuChildScene.attachChild(play);
	    menuChildScene.attachChild(quit);
	    
	    Sprite playFront = new Sprite(playMenuItem.getX(), playMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
	    Sprite quitFront = new Sprite(quitMenuItem.getX(), quitMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
	    
	    menuChildScene.attachChild(playFront);
	    menuChildScene.attachChild(quitFront);   
	    
	    menuChildScene.setOnMenuItemClickListener(this);
	    
	    setChildScene(menuChildScene);
	}

	@Override
	public boolean onKeyPressed(int keyCode){
		if(keyCode == KeyEvent.KEYCODE_BACK){
			exit();
            return true;
		}
		return false;
	}

	private void exit(){
		((MainActivity) getActivity()).save();
		System.exit(0);

	}
	
}
