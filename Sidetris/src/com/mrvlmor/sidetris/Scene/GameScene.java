package com.mrvlmor.sidetris.Scene;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.animator.DirectMenuAnimator;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;

import com.mrvlmor.sidetris.Entity.GameResult;
import android.view.KeyEvent;

import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Entity.ConvertCoord;

import com.mrvlmor.machinestate.BaseScene;
import com.mrvlmor.machinestate.IEventSink;
import com.mrvlmor.machinestate.helper.TouchDirectionListener;
import com.mrvlmor.sidetris.Events;
import com.mrvlmor.sidetris.MainActivity;
import com.mrvlmor.machinestate.helper.Direction;
import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.Explode.PlaySound;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Entity.Tile.SpriteTile;
import com.mrvlmor.sidetris.Resource.GameResource;
import com.mrvlmor.sidetris.Resource.SideTrisResource;

public class GameScene extends BaseScene<GameResource, Events> {

	private MovingField moveComplete;
	private Field field;
	private TouchDirectionListener touchListener;
	private CurrentMenu menu = CurrentMenu.NONE;
	private MenuScene menuReturn;
	private MenuListener menuListener;
	private MenuScene menuMenu;
	private MenuScene menuGameover;
	
	private final static int MENU_RESTART = 1;
	private final static int MENU_RETURN = 2;
	private final static int MENU_CONTINUE = 3;
	
	private Sprite windowScore;
	private Sprite windowBest;
	private Sprite windowLevel;
	private Sprite progressBar;
	private boolean isNewGame = false;
	
	private Sprite localMenu = null;
	private TiledSprite menuSound = null;
	
	Setting setting = new Setting();
	
	public GameScene(GameResource resource, IEventSink<Events> state) {
		super(resource, state);
	}

	@Override
	public void startScene() {
		newGame();
	}

	public Sprite getWindowScore(){
		return windowScore;
	}
	
	public Sprite getWindowBest(){
		return windowBest;
	}
	
	public Sprite getWindowLevel(){
		return windowLevel;
	}
	
	public Sprite getProgressBar(){
		return progressBar;
	}
	
	private void newGame(){
		detachChildren();
		clearChildScene();

		Sprite background = new Sprite(0, -setting.getGameOffset(), this.getResource().getGameBackground(), this.getActivity().getVertexBufferObjectManager());
		this.attachChild(background);
		

		
		float offsetLocalMenu=-20;
		
		
		Sprite windowTitle = new Sprite(setting.getHorizontalOffsetTitle(), setting.getVerticalOffsetTitle(), this.getResource().getWindow(), this.getActivity().getVertexBufferObjectManager());
		this.attachChild(windowTitle);
		
	    Text gameName = new Text(setting.getHorizontalOffsetTitle(), setting.getVerticalOffsetTitle(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.title_activity_main), getActivity().getVertexBufferObjectManager());
	    Text gameNameShadow = new Text(setting.getHorizontalOffsetTitle(), setting.getVerticalOffsetTitle(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.title_activity_main), getActivity().getVertexBufferObjectManager());

	    gameNameShadow.setColor(0, 0, 0);
	    
	    gameName.setColor(0, 1, 1);
	    
	    gameName.setX(ConvertCoord.toCenterTextX(windowTitle, gameName));
	    gameName.setY(ConvertCoord.toCenterTextY(windowTitle, gameName));
	    
	    gameNameShadow.setX(gameName.getX()+setting.getOffsetShadow());
	    gameNameShadow.setY(gameName.getY()+setting.getOffsetShadow());

	    this.attachChild(gameNameShadow);
	    this.attachChild(gameName);
		if (setting.getVerticalButtonOffset() > 310){
		    offsetLocalMenu = windowTitle.getHeight()/2-50;
		}
		
		
//		windowLevel = new Sprite(setting.getHorizontalLevelOffset(), setting.getVerticalLocalMenuOffset()+offsetLocalMenu, setting.getWidthLevel(), setting.getHeighLevel(), this.getResource().getWindowScore() , this.getActivity().getVertexBufferObjectManager());
//		this.attachChild(windowLevel);
		
		windowLevel = new Sprite(setting.getHorizontalProgressOffset(), setting.getVerticalProgressOffset(), setting.getWidthProgress(), setting.getHeightProgress(), this.getResource().getWindowProgress() , this.getActivity().getVertexBufferObjectManager());
		this.attachChild(windowLevel);
		
		progressBar = new Sprite(setting.getHorizontalProgressOffset(), setting.getVerticalProgressOffset(), setting.getWidthProgress(), setting.getHeightProgress(), this.getResource().getProgressBar(), this.getActivity().getVertexBufferObjectManager());
		this.attachChild(progressBar);
		
		windowScore = new Sprite(setting.getHorizontalButtonScoreOffset(), setting.getVerticalButtonOffset(), setting.getWidthButton(), setting.getHeightButton(), this.getResource().getWindowScore() , this.getActivity().getVertexBufferObjectManager());
		this.attachChild(windowScore);
		
		windowBest = new Sprite(setting.getHorizontalButtonBestOffset(), setting.getVerticalButtonOffset(), setting.getWidthButton(), setting.getHeightButton(), this.getResource().getWindowScore() , this.getActivity().getVertexBufferObjectManager());
		this.attachChild(windowBest);
		
		if (localMenu == null)
		{
	        localMenu = new Sprite(setting.getHorizontalLocalMenuOffset(), setting.getVerticalLocalMenuOffset()+offsetLocalMenu, getResource().getLocalMenu(), getActivity().getVertexBufferObjectManager())
	        {
	        	@Override
	        	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY){
	        		if (menu != CurrentMenu.NONE) return true;
	        		menuMenu();
	                return false;
	        	}
	        };
	        registerTouchArea(localMenu);
		}
        attachChild(localMenu);
        
        if (menuSound == null)
        {
	        menuSound = new TiledSprite(0, setting.getVerticalLocalMenuOffset()+offsetLocalMenu, getResource().getMenuSound(), getActivity().getVertexBufferObjectManager()){
	        	private long currentPressMillis=200;
	        	private long prevPressMillis=0;
	        	@Override
	        	public boolean onAreaTouched(final TouchEvent pSceneTouchEvent, final float pTouchAreaLocalX, final float pTouchAreaLocalY){
	        		if (!pSceneTouchEvent.isActionDown()) return true;
	        		if (menu != CurrentMenu.NONE) return true;
	        		currentPressMillis = System.currentTimeMillis();
	        		if ((currentPressMillis - prevPressMillis) > 400)
	        		{
 	        			nextVolumeSound(this);
	        		}
	        		prevPressMillis = currentPressMillis;
	                return false;
	        	} 
	        };
	        registerTouchArea(menuSound);
	    	GameResult gameResult = new GameResult(this);
	    	gameResult.restoreSound();
	        setTileVolume(menuSound);
        }
        attachChild(menuSound);
        
		moveComplete  = new MovingField(); 
		field = new Field(this, moveComplete);
		moveComplete.setField(field);
		if (!isNewGame && field.getBest() !=0){
			field.restore();
		}		
		else{
			field.displayMessageLevel();
		}

		
		field.setRandomCell(); 
		
		isNewGame = false;
		
		touchListener = new TouchDirectionListener(moveComplete);
		setOnSceneTouchListener(touchListener);	
		
		if (field.checkGameOver()){
			gameOverMenu();
		}

	}
	
    private void nextVolumeSound(TiledSprite volumeSound){
    	if (setting.getVolumeSound() == 0){
    		setting.setVolumeSound(0.8f);
    	}
    	else{
    		setting.setVolumeSound(0f);
    	}
    	
    	GameResult gameResult = new GameResult(this);
    	gameResult.saveSound();
    	setTileVolume(volumeSound);
    }
	
    private void setTileVolume(TiledSprite volumeSound){
    	if (setting.getVolumeSound() == 0){
    		volumeSound.setCurrentTileIndex(0);
    		return;
    	}
    	menuSound.setCurrentTileIndex(1);
    }
    
	private void clearMenuScene(){
		menu = CurrentMenu.NONE;
		clearChildScene();
	}
	
	@Override
	public boolean onKeyPressed(int keyCode) {
		if(keyCode == KeyEvent.KEYCODE_BACK){
			if(menu == CurrentMenu.GAMEOVER){
				PlaySound.stopPlay();
				save();
				castEvent(Events.BACK);
				return true;
			}
			if(menu == CurrentMenu.NONE){
				returnMenu();
				return true;
			}
			menu = CurrentMenu.NONE;
			clearMenuScene();
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_MENU){
			if(menu == CurrentMenu.GAMEOVER){
				castEvent(Events.BACK);
				return true;
			}
			if (menu == CurrentMenu.NONE){ 
				menuMenu();
				return true;
			}
			menu = CurrentMenu.NONE;
			clearMenuScene();
			return true;
		}
		return false;
	}

	public GameResource getResource(){
		return (GameResource) super.getResource();
	}
	
	public MainActivity getActivity(){
		return (MainActivity) super.getActivity();
	}
	
	public void save() {
		if (field == null) return;
		field.save();
	}
	
	
	
	private class MovingField implements IMoveFinished{
		private Field field;
		private boolean moving = false;
		
//		private DirectionNullable nextMove = new DirectionNullable();
		
		public MovingField(){
			
			((MainActivity) getActivity()).setFramesPerSecond(Setting.FPS_RUN);
		}
		
		public void setField(Field field){
			this.field = field;
		}
		
		public void move(Direction direction){
			if (direction == Direction.STOP){
				stop();
				return;
			}
			synchronized(this){
				if (moving){
//					nextMove.setValue(direction);
					return;
				}
				moving = true;
			}
			((MainActivity) getActivity()).setFramesPerSecond(Setting.FPS_RUN);
			field.move(direction);
		}
		
		public void stop(){
//			synchronized(this){
//				moving = false;
//			}
		}
		
		public void moveComplete() {
			field.moveComplete();
			field.explode();
			field.allComplete();
			if(field.isAllComplete()){
				synchronized(this){
					moving = false;
				}
				if (field.checkGameOver()){
					gameOverMenu();
				}
			}
		}
		
		
		public void destroyComplete(ISpriteTile cell){
			((MainActivity) getActivity()).setFramesPerSecond(Setting.FPS_STOP);
			field.destroyCell(cell);
			field.allComplete();
			if(field.isAllComplete()){
				synchronized(this){
					moving = false;
				}
				if (field.checkGameOver()){
					gameOverMenu();
				}
			}
			
		}
		
		public void createComplete(){
			field.createComplete();
			if(field.isAllComplete()){
				synchronized(this){
					moving = false;
				}
				if (field.checkGameOver()){
					gameOverMenu();
				}
			}
		}

	}

	private void returnMenu(){
		menu = CurrentMenu.RETURN;
		
		if (menuReturn == null){
			Setting setting = new Setting();
			menuReturn = new MenuScene((getActivity()).getCamera());
			menuReturn.setPosition(0,0); 
			menuReturn.setMenuAnimator(new DirectMenuAnimator(10f));
			
		    final SpriteMenuItem continueMenuItem = new SpriteMenuItem(MENU_CONTINUE, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    final SpriteMenuItem returnMenuItem = new SpriteMenuItem(MENU_RETURN, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    
		    menuReturn.addMenuItem(continueMenuItem);
		    menuReturn.addMenuItem(returnMenuItem);
		    
		    menuReturn.buildAnimations();
		    menuReturn.setBackgroundEnabled(false);
		    
		    //Sprite continueBack = new Sprite(continueMenuItem.getX(), continueMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    //Sprite returnBack = new Sprite(returnMenuItem.getX(), returnMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    
		    //menuReturn.attachChild(continueBack);
		    //menuReturn.attachChild(returnBack);   
		    
		    Text continue_ = new Text(continueMenuItem.getX(), continueMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Continue), getActivity().getVertexBufferObjectManager());
		    Text return_ = new Text(returnMenuItem.getX(), returnMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Return), getActivity().getVertexBufferObjectManager());
	    
		    
		    continue_.setX(ConvertCoord.toCenterTextX(continueMenuItem, continue_));
		    continue_.setY(ConvertCoord.toCenterTextY(continueMenuItem, continue_));
		    
		    return_.setX(ConvertCoord.toCenterTextX(returnMenuItem, return_));
		    return_.setY(ConvertCoord.toCenterTextY(returnMenuItem, return_));
		    
		    menuReturn.attachChild(continue_);
		    menuReturn.attachChild(return_);
		    
		    Sprite continueFront = new Sprite(continueMenuItem.getX(), continueMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
		    Sprite returnFront = new Sprite(returnMenuItem.getX(), returnMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
		    
		    menuReturn.attachChild(continueFront);
		    menuReturn.attachChild(returnFront);   
		    
		    if (menuListener==null){
		    	menuListener = new MenuListener();
		    }
		    menuReturn.setOnMenuItemClickListener(menuListener);

		}

	    setChildScene(menuReturn);
		
	}

	private void gameOverMenu(){
		isNewGame = true;
		menu = CurrentMenu.GAMEOVER;
		Setting setting = new Setting();
		
		if (menuGameover == null){

			menuGameover = new MenuScene((getActivity()).getCamera());
			menuGameover.setPosition(0,0); 

			menuGameover.setMenuAnimator(new DirectMenuAnimator(10f));
			
			Sprite background = new Sprite(0, -setting.getGameOffset(), this.getResource().getGameoverBackground(), this.getActivity().getVertexBufferObjectManager());
			menuGameover.attachChild(background);
			
		    final SpriteMenuItem restartMenuItem = new SpriteMenuItem(MENU_RESTART, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    
		    menuGameover.addMenuItem(restartMenuItem);
		    
		    menuGameover.buildAnimations();
		    menuGameover.setBackgroundEnabled(false);
		    
		    
		    restartMenuItem.setPosition(restartMenuItem.getX(), setting.getVerticalOffsetButtonGameover());
		    
		    
		    //Sprite restartBack = new Sprite(restartMenuItem.getX(), restartMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    
		    //menuGameover.attachChild(restartBack);
		    
		    Text restart_ = new Text(restartMenuItem.getX(), restartMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Restart), getActivity().getVertexBufferObjectManager());
	    
		    restart_.setX(ConvertCoord.toCenterTextX(restartMenuItem, restart_));
		    restart_.setY(ConvertCoord.toCenterTextY(restartMenuItem, restart_));
		    	    
		    menuGameover.attachChild(restart_);
		    
		    Sprite restartFront = new Sprite(restartMenuItem.getX(), restartMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
		    
		    menuGameover.attachChild(restartFront);
		    
		    Text gameover1 = new Text(restartMenuItem.getX(), setting.getVerticalOffsetGameover1(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Gameover1), getActivity().getVertexBufferObjectManager());
		    Text gameover2 = new Text(restartMenuItem.getX(), setting.getVerticalOffsetGameover2(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Gameover2), getActivity().getVertexBufferObjectManager());
		    Text gameover1Shadow = new Text(restartMenuItem.getX(), setting.getVerticalOffsetGameover1(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Gameover1), getActivity().getVertexBufferObjectManager());
		    Text gameover2Shadow = new Text(restartMenuItem.getX(), setting.getVerticalOffsetGameover2(), getResource().getFontCongratulation(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Gameover2), getActivity().getVertexBufferObjectManager());

		    gameover1Shadow.setColor(0, 0, 0);
		    gameover2Shadow.setColor(0, 0, 0); 
		    
		    gameover2.setColor(0, 1, 1);
		    gameover1.setColor(0, 1, 1);
		    
		    
		    gameover1.setX(ConvertCoord.toCenterTextX(restartMenuItem, gameover1));

		    gameover2.setX(ConvertCoord.toCenterTextX(restartMenuItem, gameover2));
		    
		    gameover1Shadow.setX(gameover1.getX()+setting.getOffsetShadow());
		    gameover1Shadow.setY(gameover1.getY()+setting.getOffsetShadow());

		    gameover2Shadow.setX(gameover2.getX()+setting.getOffsetShadow());
		    gameover2Shadow.setY(gameover2.getY()+setting.getOffsetShadow());

		    menuGameover.attachChild(gameover2Shadow);
		    menuGameover.attachChild(gameover1Shadow);
		    		    
		    menuGameover.attachChild(gameover2);
		    menuGameover.attachChild(gameover1);
		    
		    
		    Text HeaderCurrent = new Text(setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetHeaderResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Current), getActivity().getVertexBufferObjectManager());
		    HeaderCurrent.setColor(1,1,0);
		    menuGameover.attachChild(HeaderCurrent);		    
		    
		    Text HeaderBest = new Text(setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetHeaderResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Best), getActivity().getVertexBufferObjectManager());
		    HeaderBest.setColor(1,1,0);
		    menuGameover.attachChild(HeaderBest);

		    Text Long = new Text(setting.getHorizontalOffsetTypeResult(), setting.getVerticalOffsetLongResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Long), getActivity().getVertexBufferObjectManager());
		    Long.setColor(1,1,0);
		    menuGameover.attachChild(Long);
		    
		    Text Combo = new Text(setting.getHorizontalOffsetTypeResult(), setting.getVerticalOffsetComboResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Combo), getActivity().getVertexBufferObjectManager());
		    Combo.setColor(1,1,0);
		    menuGameover.attachChild(Combo);
		    
		    Text Double = new Text(setting.getHorizontalOffsetTypeResult(), setting.getVerticalOffsetDoubleResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Double), getActivity().getVertexBufferObjectManager());
		    Double.setColor(1,1,0);
		    menuGameover.attachChild(Double);
		    
		    Text Triple = new Text(setting.getHorizontalOffsetTypeResult(), setting.getVerticalOffsetTripleResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Triple), getActivity().getVertexBufferObjectManager());
		    Triple.setColor(1,1,0);
		    menuGameover.attachChild(Triple);
		    
		    Text Quadruple = new Text(setting.getHorizontalOffsetTypeResult(), setting.getVerticalOffsetQuadrupleResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Quadruple), getActivity().getVertexBufferObjectManager());
		    Quadruple.setColor(1,1,0);
		    menuGameover.attachChild(Quadruple);
		    
		    Text ScoreResult = new Text(setting.getHorizontalOffsetTypeResult(), setting.getVerticalOffsetScoreResult(), getResource().getFontMenu(),setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Score), getActivity().getVertexBufferObjectManager());
		    ScoreResult.setColor(1,1,0);
		    menuGameover.attachChild(ScoreResult);
		    
		    if (menuListener==null){
		    	menuListener = new MenuListener();
		    }
		    menuGameover.setOnMenuItemClickListener(menuListener);

		}
		
		GameResult gameResult = field.getGameResult();

		LongCurrent = setResult(LongCurrent, setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetLongResult(), String.format("%8d", gameResult.getCurLong()));
		LongBest = setResult(LongBest, setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetLongResult(), String.format("%8d", gameResult.getBestLong()));
		ComboCurrent = setResult(ComboCurrent, setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetComboResult(), String.format("%8d", gameResult.getCurCombo()));
		ComboBest = setResult(ComboBest, setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetComboResult(), String.format("%8d", gameResult.getBestCombo()));
		DoubleCurrent = setResult(DoubleCurrent, setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetDoubleResult(), String.format("%8d", gameResult.getCurDouble(0)));
		DoubleBest = setResult(DoubleBest, setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetDoubleResult(), String.format("%8d", gameResult.getBestDouble(0)));
		TripleCurrent = setResult(TripleCurrent, setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetTripleResult(), String.format("%8d", gameResult.getCurDouble(1)));
		TripleBest = setResult(TripleBest, setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetTripleResult(), String.format("%8d", gameResult.getBestDouble(1)));
		QuadrupleCurrent = setResult(QuadrupleCurrent, setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetQuadrupleResult(), String.format("%8d", gameResult.getCurDouble(2)));
		QuadrupleBest = setResult(QuadrupleBest, setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetQuadrupleResult(), String.format("%8d", gameResult.getBestDouble(2)));
		Score = setResult(Score, setting.getHorizontalOffsetCurrentResult(), setting.getVerticalOffsetScoreResult(), String.format("%8d", field.getScore()));
		Best = setResult(Best, setting.getHorizontalOffsetBestResult(), setting.getVerticalOffsetScoreResult(), String.format("%8d", field.getBest()));
		
		if (field.getScore() >= field.getBest()){
			PlaySound.play(getResource().getNewRecord_sound());
		}
		else{
			PlaySound.play(getResource().getGameOver_sound());
		}
		
	    setChildScene(menuGameover);
	}

	private Text setResult(final Text txt, int X, int Y, final String str){
		if (txt == null){
			Text result = new Text(X, Y, getResource().getFontMenu(), str, getActivity().getVertexBufferObjectManager());
			result.setColor(1,1,0);
		    menuGameover.attachChild(result);	
			return result;
		}
		getActivity().runOnUiThread(new Runnable() {
			public void run() {
				txt.setText(str);
			}
		});
		return txt;
	

	}
	
	private Text LongCurrent;
	private Text LongBest;
	private Text ComboCurrent;
	private Text ComboBest;
	private Text DoubleCurrent;
	private Text DoubleBest;
	private Text TripleCurrent;
	private Text TripleBest;
	private Text QuadrupleCurrent;
	private Text QuadrupleBest;
	private Text Score;
	private Text Best;
	
	
	private void menuMenu(){
		menu = CurrentMenu.MENU;
		
		if (menuMenu == null){
			Setting setting = new Setting();
			menuMenu = new MenuScene((getActivity()).getCamera());
			menuMenu.setPosition(0,0); 
			menuMenu.setMenuAnimator(new DirectMenuAnimator(10f));
			
		    final SpriteMenuItem restartMenuItem = new SpriteMenuItem(MENU_RESTART, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    final SpriteMenuItem continueMenuItem = new SpriteMenuItem(MENU_CONTINUE, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    final SpriteMenuItem returnMenuItem = new SpriteMenuItem(MENU_RETURN, getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
		    
		    menuMenu.addMenuItem(restartMenuItem);
		    menuMenu.addMenuItem(continueMenuItem);
		    menuMenu.addMenuItem(returnMenuItem);
		    
		    menuMenu.buildAnimations();
		    menuMenu.setBackgroundEnabled(false);
		    
//		    Sprite restartBack = new Sprite(restartMenuItem.getX(), restartMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
//		    Sprite continueBack = new Sprite(continueMenuItem.getX(), continueMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
//		    Sprite returnBack = new Sprite(returnMenuItem.getX(), returnMenuItem.getY(),  getResource().getMenuBack(), getActivity().getVertexBufferObjectManager());
//		    
//		    menuMenu.attachChild(restartBack);
//		    menuMenu.attachChild(continueBack);
//		    menuMenu.attachChild(returnBack);   
		    
		    Text restart_ = new Text(restartMenuItem.getX(), restartMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Restart), getActivity().getVertexBufferObjectManager());
		    Text continue_ = new Text(continueMenuItem.getX(), continueMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Continue), getActivity().getVertexBufferObjectManager());
		    Text return_ = new Text(returnMenuItem.getX(), returnMenuItem.getY(), getResource().getFontMenu(), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Return), getActivity().getVertexBufferObjectManager());
	    
		    restart_.setX(ConvertCoord.toCenterTextX(restartMenuItem, restart_));
		    restart_.setY(ConvertCoord.toCenterTextY(restartMenuItem, restart_));
		    
		    continue_.setX(ConvertCoord.toCenterTextX(continueMenuItem, continue_));
		    continue_.setY(ConvertCoord.toCenterTextY(continueMenuItem, continue_));
		    
		    return_.setX(ConvertCoord.toCenterTextX(returnMenuItem, return_));
		    return_.setY(ConvertCoord.toCenterTextY(returnMenuItem, return_));
		    
		    menuMenu.attachChild(restart_);
		    menuMenu.attachChild(continue_);
		    menuMenu.attachChild(return_);
		    
		    Sprite restartFront = new Sprite(restartMenuItem.getX(), restartMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
		    Sprite continueFront = new Sprite(continueMenuItem.getX(), continueMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
		    Sprite returnFront = new Sprite(returnMenuItem.getX(), returnMenuItem.getY(),  getResource().getMenuFront(), getActivity().getVertexBufferObjectManager());
		    
		    menuMenu.attachChild(restartFront);
		    menuMenu.attachChild(continueFront);
		    menuMenu.attachChild(returnFront);   
		    
		    if (menuListener==null){
		    	menuListener = new MenuListener();
		    }
		    menuMenu.setOnMenuItemClickListener(menuListener);

		}

	    setChildScene(menuMenu);
		
	}
	
	
	
	private class MenuListener implements IOnMenuItemClickListener{

		public boolean onMenuItemClicked(MenuScene pMenuScene,
				IMenuItem pMenuItem, float pMenuItemLocalX,
				float pMenuItemLocalY) {
			switch(pMenuItem.getID())
	        {
	        case MENU_CONTINUE:
	        	clearMenuScene();
	            return true;
	        case MENU_RETURN:
	        	PlaySound.stopPlay();
	        	clearMenuScene();
	        	save();
	        	castEvent(Events.BACK);
	            return true;
	        case MENU_RESTART:
	        	PlaySound.stopPlay();
	        	clearMenuScene();
	        	isNewGame = true;
	        	newGame();
	            return true;
	        default:
	        	clearMenuScene();
	            return false;
	        }

		}
			
	}
	
	private enum CurrentMenu{
		NONE,
		RETURN,
		GAMEOVER,
		MENU
	}
	

}
