package com.mrvlmor.sidetris.Entity.Explode;

import java.util.ArrayList;
import java.util.List;

import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Entity.Tile.SparkTile;

public class DestroySpark extends DestroyBase implements IDestroyCell {


	
	public DestroySpark(IScoreParams scoreParams) {
		super(scoreParams);
	}

	public DestroySpark(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, destroyCell);
	}

	@Override
	public DestroyResult destroy(int X, int Y){
	    if (scoreParams.getField(X,Y) == null) return new DestroyResult();
	    if (scoreParams.getField(X,Y).getTileType() != TileType.SPARK) return new DestroyResult();
	    
	    List<ISpriteTile> target;
	    target=new ArrayList<ISpriteTile>();	
        TileType tileUp=TileType.NOTHING;
        TileType tileDown=TileType.NOTHING;      
        TileType tileLeft=TileType.NOTHING;
        TileType tileRight=TileType.NOTHING;

        if(X>Field.minCell && scoreParams.getField(X-1,Y)!=null) tileLeft=scoreParams.getField(X-1,Y).getTileType();
	    if(X<Field.maxCell && scoreParams.getField(X+1,Y)!=null) tileRight=scoreParams.getField(X+1,Y).getTileType();
	    if(Y>Field.minCell && scoreParams.getField(X,Y-1)!=null) tileUp= scoreParams.getField(X,Y-1).getTileType();
	    if(Y<Field.maxCell && scoreParams.getField(X,Y+1)!=null) tileDown= scoreParams.getField(X,Y+1).getTileType();

        for(int x=Field.minCell; x<=Field.maxCell; x++){
            for(int y=Field.minCell;y<=Field.maxCell; y++){
                if( scoreParams.getField(x,y) ==null) continue;
                TileType t=scoreParams.getField(x,y).getTileType();
                 if(t==tileLeft||t==tileRight||t==tileUp||t==tileDown){
                    target.add(scoreParams.getField(x,y));
                    explodeCell(x,y,0.5f);
                 }
            }
        }
        SparkTile sparkTile = (SparkTile) scoreParams.getField(X,Y);
        sparkTile.setNumberSpark(target);
        explodeCell(X,Y,0);


        scoreParams.addScore(4);
        return new DestroyResult(scoreParams.getField(X,Y), 1);
	    }
	

}
