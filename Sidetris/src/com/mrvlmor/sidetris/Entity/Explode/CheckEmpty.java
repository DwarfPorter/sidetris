package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;

public class CheckEmpty extends CheckBase implements ICheckCell{
	
	public CheckEmpty(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		super(scoreParams, gameResult, showSlideMessage, checkCell);
	}
	
	public CheckEmpty(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage){
		super(scoreParams, gameResult, showSlideMessage);
	}
	
	private boolean isEmpty(){
	    for(int x=Field.minCell;x<=Field.maxCell; x++)
	        for(int y=Field.minCell;y<=Field.maxCell; y++)
	            if(scoreParams.getField(x,y) != null)
	               if(!scoreParams.getField(x,y).isExploding())
	                   return false;
	    return true;
	}
	
	@Override
	public boolean checkMy(){
	
	    if(!isEmpty()) return false;
	    float x = 400;
	    float y = ConvertCoord.toRealY(5);
	    showSlideMessage.showMessageSuperScore(x, y, setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Empty), 0);
	    gameResult.addCurScore(100);
	    gameResult.incCurEmpty();
	    return true;
	}

}