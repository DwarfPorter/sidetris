package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;

public class CheckDouble extends CheckBase implements ICheckCell{
	
	public CheckDouble(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		super(scoreParams, gameResult, showSlideMessage, checkCell);
	}
	
	public CheckDouble(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage){
		super(scoreParams, gameResult, showSlideMessage);
	}	
	
	@Override
	public boolean checkMy(){
		if (!(getCheckLong().size() > 1)) return false;
		scoreParams.multiScore(getCheckLong().size());
		// show double, triple or quadruple
		String dbl;
		switch(getCheckLong().size()){
		case 2: dbl = setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Double);
			gameResult.incCurDouble(0);
		break;
		case 3: dbl = setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Triple);
			gameResult.incCurDouble(1);
		break;
		case 4: dbl = setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Quadruple);
			gameResult.incCurDouble(2);
		break;
		default:dbl = setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Quintuple);
			gameResult.incCurDouble(3);
			
		}
		
		showSlideMessage.showMessageScore(getCellForMessage().get(getCellForMessage().size()-1), dbl, 0);

		return true;
	}

}