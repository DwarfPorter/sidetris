package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Field.DirectionStrategy;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Tile.SawTile;

public class DestroySaw extends DestroyBase implements IDestroyCell {
	
	
    public DestroySaw(IScoreParams scoreParams) {
		super(scoreParams);
	}
    
    public DestroySaw(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, destroyCell);
	}
    
	@Override
	public DestroyResult destroy(int X, int Y)
    { 	
        
        if (scoreParams.getField(X,Y) == null) return new DestroyResult();
        if (scoreParams.getField(X,Y).getTileType() != TileType.SAW) return new DestroyResult();
		scoreParams.addScore(3);
        SawTile sawTile = (SawTile) scoreParams.getField(X,Y);
		DirectionStrategy directionStrategy = scoreParams.getDirectionStrategy();
        if (directionStrategy.checkLimitDirection(X,Y)){
        	explodeCell(X,Y,0);
        	return new DestroyResult(scoreParams.getField(X,Y), 0);
        }
    	int X1 = directionStrategy.nextX(X);
		int Y1 = directionStrategy.nextY(Y);
		if (scoreParams.getField(X1,Y1) == null) return new DestroyResult();
		sawTile.setSawed(scoreParams.getField(X1,Y1));
		explodeCell(X1, Y1,0);
        return new DestroyResult(scoreParams.getField(X1,Y1), 1);
   }
	
	public boolean isDestroyFirst(){
		return true;
	}
}
