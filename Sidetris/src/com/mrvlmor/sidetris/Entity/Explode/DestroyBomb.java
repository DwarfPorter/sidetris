package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.TileType;

public class DestroyBomb  extends DestroyBase implements IDestroyCell{

    public DestroyBomb(IScoreParams scoreParams) {
		super(scoreParams);
	}
    
    public DestroyBomb(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, destroyCell);
	}
    
	@Override
	public DestroyResult destroy(int X, int Y)
    { 		

        if (scoreParams.getField(X,Y) == null) return new DestroyResult();
        if (scoreParams.getField(X,Y).getTileType() != TileType.BOMB) return new DestroyResult();
        //bomb = (BombTile) _field[X][Y];
        int minX = X<=Field.minCell ? Field.minCell : X-1;
        int maxX = X>=Field.maxCell ? Field.maxCell : X+1;
        int minY = Y<=Field.minCell ? Field.minCell : Y-1;
        int maxY= Y>=Field.maxCell ? Field.maxCell : Y+1;
        for (int x=minX;x<=maxX;x++)
            for (int y=minY;y<=maxY;y++){
                if(scoreParams.getField(x,y) == null) continue;
                explodeCell(x,y,0);
            }
        scoreParams.addScore(10);
        return new DestroyResult(scoreParams.getField(X,Y), 1);
    }

}
