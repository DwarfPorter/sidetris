package com.mrvlmor.sidetris.Entity.Explode;



public abstract class DestroyBase{
	protected IDestroyCell _destroyCell;
	protected IScoreParams scoreParams;

	protected DestroyBase(IScoreParams scoreParams){
		this.scoreParams = scoreParams;
	}
	protected DestroyBase(IScoreParams scoreParams, IDestroyCell destroyCell){
		this(scoreParams);
		this._destroyCell=destroyCell;
	}

	public abstract DestroyResult destroy(int X, int Y);
	
	public void destroyCell(int X, int Y){
		if (_destroyCell != null) _destroyCell.destroyCell(X,Y);
		if (!isDestroyLast())
			scoreParams.SetScoreParams(destroy(X,Y));
	}
	
	public boolean isDestroyFirst(){
		return false;
	}
	
	public boolean isDestroyLast(){
		return false;
	}
	
	public void destroyLast(int X, int Y){
		if (_destroyCell != null) _destroyCell.destroyLast(X,Y);
		if (isDestroyLast())
			scoreParams.SetScoreParams(destroy(X,Y));
	}
	
	public void destroyFirst(int X, int Y){
		if (_destroyCell != null) _destroyCell.destroyFirst(X,Y);
		if (isDestroyFirst())
			scoreParams.SetScoreParams(destroy(X,Y));
	}
	
    protected int explodeCell(int X, int Y, float duration){
			if (!scoreParams.getField(X, Y).isExploding()){
				scoreParams.getField(X, Y).destroyCell(duration);
				scoreParams.incExploding();
				scoreParams.incScore();
				return 1;
			}
			return 0;
		}
}