package com.mrvlmor.sidetris.Entity.Explode;


public interface IDestroyCell {

    void destroyCell(int X, int Y);
    DestroyResult destroy(int X, int Y);
    void destroyFirst(int X, int Y);
    void destroyLast(int X, int Y);
    boolean isDestroyFirst();
    boolean isDestroyLast();
}