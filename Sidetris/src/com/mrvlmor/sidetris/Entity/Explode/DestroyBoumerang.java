package com.mrvlmor.sidetris.Entity.Explode;

import java.util.Random;

import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Field.DirectionStrategy;
import com.mrvlmor.sidetris.Entity.Tile.BoumerangTile;

public class DestroyBoumerang extends DestroyBase implements IDestroyCell {

	private Random rnd = new Random();
	
	public DestroyBoumerang(IScoreParams scoreParams) {
		super(scoreParams);
	}

	public DestroyBoumerang(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, destroyCell);
	}

	@Override
	public DestroyResult destroy(int X, int Y) {
        if (scoreParams.getField(X,Y) == null) return new DestroyResult();
        if (scoreParams.getField(X,Y).getTileType() != TileType.BOUMERANG) return new DestroyResult();
        scoreParams.addScore(5);
        BoumerangTile bTile=(BoumerangTile)scoreParams.getField(X,Y);
        DirectionStrategy directionStrategy = scoreParams.getDirectionStrategy();
        int SX[]=new int[4];
        int SY[]=new int[4];
        int distance=0;
        if (!directionStrategy.checkLimitDirection(X, Y)){
	        for(int C1=directionStrategy.getCoordfromC1C2(Y, X); directionStrategy.limitC1(C1); C1=directionStrategy.nextC1(C1)){
	        	SX[0] = directionStrategy.getCoordfromC1C2(X,C1);
	            SY[0] = directionStrategy.getCoordfromC1C2(C1,Y);
	            if (scoreParams.getField(SX[0],SY[0]) != null && scoreParams.getField(SX[0],SY[0]).getTileType() != TileType.BOUMERANG) {
	            	explodeCell(SX[0],SY[0], distance*0.05f);
	            }
	            distance++;
	        }
        }
        else{
        	SX[0] = X;
        	SY[0] = Y;
        }
        int i=0;
        while(i<3){
        
	        directionStrategy = directionStrategy.toPerpendicular(SX[i],SY[i]);
	        i++;
        
	        for(int C1= directionStrategy.getCoordfromC1C2(SY[i-1], SX[i-1]);directionStrategy.limitC1(C1); C1=directionStrategy.nextC1(C1)){
	        	SX[i] = directionStrategy.getCoordfromC1C2(SX[i-1],C1);
	            SY[i] = directionStrategy.getCoordfromC1C2(C1,SY[i-1]);
	            if (scoreParams.getField(SX[i],SY[i]) != null && scoreParams.getField(SX[i],SY[i]).getTileType() != TileType.BOUMERANG) {
	            	explodeCell(SX[i],SY[i], distance*0.05f);
	            }
	            distance++; 
	        }
	        
	        //if (i>= 3) break;
        }
        
        bTile.setThrought(SX, SY); 
        explodeCell(X,Y,0);
        
        
        return new DestroyResult(scoreParams.getField(X,Y), 1);

	}

	
	public boolean isDestroyFirst(){
		return true;
	}
}
