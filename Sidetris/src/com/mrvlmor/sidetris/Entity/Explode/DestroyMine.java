package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.TileType;

public class DestroyMine extends DestroyBase implements IDestroyCell {

	public DestroyMine(IScoreParams scoreParams) {
		super(scoreParams);
	}

	public DestroyMine(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, destroyCell);
	}

    private boolean isMined(int X, int Y){
        int minX = X<=Field.minCell ? Field.minCell : X-1;
        int maxX = X>=Field.maxCell ? Field.maxCell : X+1;
        int minY = Y<=Field.minCell ? Field.minCell : Y-1;
        int maxY= Y>=Field.maxCell ? Field.maxCell : Y+1;
        for (int x=minX;x<=maxX;x++){
            for (int y=minY;y<=maxY;y++){
                if(scoreParams.getField(x,y) == null) continue;
                if(scoreParams.getField(x,y).isExploding()) return true;
            }
        }
        return false;
    }
    
    @Override
	public boolean isDestroyLast(){
		return true;
	}
    
	@Override
    public DestroyResult destroy(int X, int Y){
    	if (scoreParams.getField(X,Y) == null) return new DestroyResult();
    	if (scoreParams.getField(X,Y).getTileType() != TileType.MINE) return new DestroyResult();
    	if (!isMined(X, Y)) return new DestroyResult();
    	
    	int distance=0;
    	for (int x=X- 1; x>= Field.minCell; x--){
    		if (scoreParams.getField(x,Y) != null){
    			explodeCell(x,Y, distance*0.05f);
    		}
    		distance++;
    	}
    	distance=0;
    	for (int x=X+ 1; x<= Field.maxCell; x++){
    		if (scoreParams.getField(x,Y) != null) {
    		explodeCell(x,Y, distance*0.05f);
    		}
    		distance++;
    	}
    	distance=0;
    	for (int y=Y-1; y>= Field.minCell; y--){
    		if (scoreParams.getField(X,y) != null) {
    			explodeCell(X,y, distance*0.05f);
    		}
    		distance++;
    	}
    	distance=0;
    	for (int y=Y+ 1; y<= Field.maxCell; y++){
    		if (scoreParams.getField(X,y) != null) {
    			explodeCell(X,y, distance*0.05f);
    		}
    		distance++;
    	}
    		explodeCell(X,Y,0);
//    	for (int C1=Field.minCell; C1<=Field.maxCell; C1++){
//    		if (scoreParams.getField(C1,Y) != null) explodeCell(C1,Y);
//    		if (scoreParams.getField(X,C1) != null) explodeCell(X,C1);
//    	}

    	scoreParams.addScore(4);
    	return new DestroyResult(scoreParams.getField(X,Y), 1);
    }
	
	

}
