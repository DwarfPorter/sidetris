package com.mrvlmor.sidetris.Entity.Explode;


public class DestroyCellCondition extends DestroyBase implements IDestroyCell{

	protected DestroyCellCondition(IScoreParams scoreParams) {
		super(scoreParams, setInit(scoreParams, null));
	}
	protected DestroyCellCondition(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, setInit(scoreParams, destroyCell));
	}
	
	private static IDestroyCell setInit(IScoreParams scoreParams, IDestroyCell destroyCell){
		return new DestroyAngle4Condition(scoreParams, 
				new DestroyAngle3Condition(scoreParams, 
						new DestroyAngle2Condition(scoreParams,
								new DestroyAngle1Condition(scoreParams,
										new DestroyHorizontalCondition(scoreParams,
												new DestroyVerticalCondition(scoreParams, destroyCell))))));
	}
	
	@Override
	public DestroyResult destroy(int X, int Y) {
			return _destroyCell.destroy(X,Y);
	}
	

	
}




