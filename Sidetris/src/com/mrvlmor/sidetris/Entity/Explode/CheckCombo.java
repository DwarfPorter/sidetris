package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;

public class CheckCombo extends CheckBase implements ICheckCell{
	
	public CheckCombo(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		super(scoreParams, gameResult, showSlideMessage, checkCell);
	}
	
	public CheckCombo(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage){
		super(scoreParams, gameResult, showSlideMessage);
	}	
	
	@Override
	public boolean checkMy(){
		if (!(getCntCombo() > 1 && getPrepScore() > 1)) return false;
		scoreParams.multiScore(getCntCombo());
		gameResult.incCurCombo(getCntCombo()-2);
		// show combo
		showSlideMessage.showMessageScore(getCellForMessage().get(0), String.format("%s X %d", setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Combo), getCntCombo()), 0);
		return true;
	}

}