package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.TileType;

public class DestroyDiamond  extends DestroyBase implements IDestroyCell{

    public DestroyDiamond(IScoreParams scoreParams) {
		super(scoreParams);
	}
    
    public DestroyDiamond(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, destroyCell);
	}

	@Override
	public DestroyResult destroy(int X, int Y)
    { 		

        if (scoreParams.getField(X,Y) == null) return new DestroyResult();
        if (scoreParams.getField(X,Y).getTileType() != TileType.DIAMOND) return new DestroyResult();
        scoreParams.addScore(5);
        return new DestroyResult(scoreParams.getField(X,Y), 1);
    }

}