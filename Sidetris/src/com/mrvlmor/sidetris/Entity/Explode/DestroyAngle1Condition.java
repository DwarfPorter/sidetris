package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Field;

public class DestroyAngle1Condition extends DestroyThreeCondition implements IDestroyCell{
	
	public DestroyAngle1Condition(IScoreParams scoreParams){
		super(scoreParams);
	}
	
	public DestroyAngle1Condition(IScoreParams scoreParams, IDestroyCell destroyCell){
		super(scoreParams, destroyCell);
	}
	
	@Override
	public int getX1(int X) {
		return X;
	}

	@Override
	public int getX2(int X) {
		return X;
	}

	@Override
	public int getX3(int X) {
		return X+1;
	}

	@Override
	public int getY1(int Y) {
		return Y+1;
	}

	@Override
	public int getY2(int Y) {
		return Y;
	}

	@Override
	public int getY3(int Y) {
		return Y;
	}

	@Override
	public boolean bordurCondition(int X, int Y) {
		return Y >= Field.maxCell || X >= Field.maxCell;
	}
	
}