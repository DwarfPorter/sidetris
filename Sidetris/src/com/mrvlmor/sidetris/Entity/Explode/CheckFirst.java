package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;

public class CheckFirst extends CheckBase implements ICheckCell {

	public CheckFirst(IScoreParams scoreParams, GameResult gameResult,
			ShowSlideMessage showSlideMessage, ICheckCell checkCell) {
		super(scoreParams, gameResult, showSlideMessage, checkCell);
	}

	public CheckFirst(IScoreParams scoreParams, GameResult gameResult,
			ShowSlideMessage showSlideMessage) {
		super(scoreParams, gameResult, showSlideMessage);
	}

	@Override
	public boolean checkMy() {
		if (gameResult.getCurScore() == 0 && getPrepScore() !=0){
			// show score
			showSlideMessage.showMessageScore(getCellForMessage().get(0), setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Score), 0);
			return true;
		}
		return false;
	}

}
