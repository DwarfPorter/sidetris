package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;

public class DestroyResult {

    public final boolean IsDestroy;
    public final ISpriteTile Tile;
	public final int QntDestroyedTiles;
	  
	public DestroyResult(){
		  IsDestroy = false;
		  Tile = null;
		  QntDestroyedTiles = 0;
	}
	  
	public DestroyResult(ISpriteTile tile, int qntDestroyTile){
		IsDestroy = true;
		Tile = tile;
		QntDestroyedTiles = qntDestroyTile;
	}

  }