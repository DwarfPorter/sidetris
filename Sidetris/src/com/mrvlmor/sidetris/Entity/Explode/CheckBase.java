package com.mrvlmor.sidetris.Entity.Explode;

import java.util.List;
import java.util.Map;

import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;

public abstract class CheckBase implements ICheckCell{
	
	private ICheckCell checkCell;
	protected IScoreParams scoreParams;
	protected GameResult gameResult;
	protected ShowSlideMessage showSlideMessage;
	protected Setting setting = new Setting();
	
	protected CheckBase(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		this(scoreParams, gameResult, showSlideMessage);
		this.checkCell = checkCell;
	}
	
	protected CheckBase(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage){
		this.scoreParams = scoreParams;
		this.gameResult = gameResult;
		this.showSlideMessage = showSlideMessage;
	}
	
	protected List<ISpriteTile> getCellForMessage(){
		return scoreParams.getCellForMessage();
	}
	
	protected Map<TileType, Integer> getCheckLong(){
		return scoreParams.getCheckLong();
	}
	
	protected int getCntCombo(){
		return scoreParams.getCntCombo();
	}
	
	protected int getPrepScore(){
		return scoreParams.getPrepScore();
	}
	
	public void initNextTurn(){
		if (checkCell == null) return;
		checkCell.initNextTurn();
	}
	
	public boolean check(){
		boolean answer = false;
		if (checkCell == null) answer=false;
		else answer = checkCell.check();
		boolean answer2 = checkMy();
		return answer || answer2;
	}
	
	public abstract boolean checkMy();
	
}