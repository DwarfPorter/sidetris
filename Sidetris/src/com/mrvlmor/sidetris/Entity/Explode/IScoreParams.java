package com.mrvlmor.sidetris.Entity.Explode;

import java.util.List;
import java.util.Map;

import com.mrvlmor.sidetris.Entity.Field.DirectionStrategy;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Entity.TileType;

public interface IScoreParams{

	void SetScoreParams( DestroyResult destroyResult);
	ISpriteTile[][] getField();
	ISpriteTile getField(int X, int Y);
	void incExploding(); //	cntExploding++;
	void incScore(); //	prepScore++;
	void addScore(int score);
	DirectionStrategy getDirectionStrategy();
	Map<TileType, Integer> getCheckLong();
	List<ISpriteTile> getCellForMessage();
	void multiScore(int score);
	int getCntCombo();
	int getPrepScore();
}