package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;

public class CheckAll extends CheckBase implements ICheckCell {

	public CheckAll(IScoreParams scoreParams, GameResult gameResult,
			ShowSlideMessage showSlideMessage, ICheckCell checkCell) {
		super(scoreParams, gameResult, showSlideMessage, setInit(scoreParams, gameResult, showSlideMessage, checkCell));
	}

	public CheckAll(IScoreParams scoreParams, GameResult gameResult,
			ShowSlideMessage showSlideMessage) {
		super(scoreParams, gameResult, showSlideMessage, setInit(scoreParams, gameResult, showSlideMessage, null));
	}

	private static ICheckCell setInit(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		return new CheckLastChance(scoreParams, gameResult, showSlideMessage,
				new CheckEmpty(scoreParams, gameResult, showSlideMessage, 
						new CheckCombo(scoreParams, gameResult, showSlideMessage,
								new CheckLong(scoreParams, gameResult, showSlideMessage,
										new CheckDouble(scoreParams, gameResult, showSlideMessage,
												new CheckFirst(scoreParams, gameResult, showSlideMessage, checkCell))))));
	}
	
	
	@Override
	public boolean checkMy() {
		return false;
	}

}
