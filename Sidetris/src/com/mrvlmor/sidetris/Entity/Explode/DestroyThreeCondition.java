package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Field;

public abstract class DestroyThreeCondition extends DestroyBase implements IDestroyCell{
	
	protected DestroyThreeCondition(IScoreParams scoreParams){
		super(scoreParams);
	}
	
	protected DestroyThreeCondition(IScoreParams scoreParams, IDestroyCell destroyCell){
		super(scoreParams, destroyCell);
	}
	
	@Override
    public DestroyResult destroy(int X, int Y){
		if (bordurCondition(X, Y)) return new DestroyResult();
		if (scoreParams.getField(getX1(X), getY1(Y)) == null) return new DestroyResult();
		if (scoreParams.getField(getX2(X), getY2(Y)) == null) return new DestroyResult();
		if (scoreParams.getField(getX3(X), getY3(Y)) == null) return new DestroyResult();
		
		if(scoreParams.getField(getX1(X), getY1(Y)).isEqual(scoreParams.getField(getX2(X), getY2(Y))) 
				&& scoreParams.getField(getX2(X), getY2(Y)).isEqual(scoreParams.getField(getX3(X), getY3(Y)))){
			int qntExplode = 0;
			qntExplode += explodeCell(getX1(X), getY1(Y),0);
			qntExplode += explodeCell(getX2(X), getY2(Y),0);
			qntExplode += explodeCell(getX3(X), getY3(Y),0);
			return new DestroyResult(scoreParams.getField(getX2(X), getY2(Y)), qntExplode);
		}
		return new DestroyResult();
	}
	
	public abstract boolean bordurCondition(int X, int Y);
    
    public abstract int getX1(int X);
    public abstract int getX2(int X);
    public abstract int getX3(int X);
    public abstract int getY1(int Y);
    public abstract int getY2(int Y);
    public abstract int getY3(int Y);
    
}
