package com.mrvlmor.sidetris.Entity.Explode;

import org.andengine.audio.sound.Sound;

import com.mrvlmor.sidetris.Setting;

public class PlaySound {

	private static Sound sound = null;
	private static Setting setting = new Setting();
	private static int priority =0;
	
	public PlaySound() {
		PlaySound.sound = null;
		priority =0;
	}
	
	public PlaySound(Sound sound) {
		if (priority == 0)
			PlaySound.sound = sound;
		
	}

	public static void clearSound(){
		sound=null;
		priority=0;
	}
	
	public static void setSound(Sound sound){
		if (priority == 0){
			priority++;
			PlaySound.sound = sound;
		}
	}
	
	public static void setSound(Sound sound, boolean isExplode){
		if(isExplode){
			if (priority<3){
				priority++;
				PlaySound.sound=sound;
				return;
			}
		}
		else{
			if(priority<2){
				priority++;
				PlaySound.sound=sound;
			}
		}
	}
	
	
	public static void play(){
		if (setting.getVolumeSound() == 0) return;
		if (sound == null) return;
		sound.play();
	}
	
	public static void play(Sound sound){
		if (setting.getVolumeSound() == 0) return;
		PlaySound.sound = sound;
		if (sound == null) return;
		sound.play();	
	}
	
	public static void stopPlay(){
		if (setting.getVolumeSound() == 0) return;
		if (sound == null) return;
		sound.stop();
	}
	
}
