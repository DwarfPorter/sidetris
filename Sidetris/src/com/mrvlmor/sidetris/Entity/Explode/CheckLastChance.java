package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;

public class CheckLastChance  extends CheckBase implements ICheckCell{

	public CheckLastChance(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		super(scoreParams, gameResult, showSlideMessage, checkCell);
	}
	
	public CheckLastChance(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage){
		super(scoreParams, gameResult, showSlideMessage);
	}
	
	private boolean isPrevOneTile(){
		int cntlastChance=0;
		for(int x=Field.minCell;x<=Field.maxCell; x++)
			for(int y=Field.minCell;y<=Field.maxCell; y++){
				if(scoreParams.getField(x,y) == null || scoreParams.getField(x,y).isExploding()) {
					prevLastChanceX = x;
					prevLastChanceY = y;
					cntlastChance++;
				}
				if (cntlastChance >= 2) 
                   return false;
			}
		return cntlastChance==1;
	}

	@Override
	public void initNextTurn()
  	{
		super.initNextTurn();
	  	prevLastChance=isPrevOneTile();
  	}
	
	private boolean prevLastChance = false;
	private int prevLastChanceX = 0;
	private int prevLastChanceY = 0;

	public boolean checkMy(){
		if (prevLastChance && getPrepScore() >0){
			showSlideMessage.showMessageLastChance(prevLastChanceX, prevLastChanceY, setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.LastChance), 0);
			gameResult.addCurScore(25);
			prevLastChance=false;
          	gameResult.incCurLastChance();
			return true;
		}
		//prevLastChance = isPrevOneTile();
		return false;
	}
	

	
}