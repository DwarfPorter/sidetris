package com.mrvlmor.sidetris.Entity.Explode;


public interface ICheckCell{
    boolean check();
    void initNextTurn();
}
