package com.mrvlmor.sidetris.Entity.Explode;

import com.mrvlmor.sidetris.Entity.GameResult;
import com.mrvlmor.sidetris.Entity.ShowSlideMessage;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;

public class CheckLong extends CheckBase implements ICheckCell{

	public CheckLong(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage, ICheckCell checkCell){
		super(scoreParams, gameResult, showSlideMessage, checkCell);
	}
	
	public CheckLong(IScoreParams scoreParams, GameResult gameResult, ShowSlideMessage showSlideMessage){
		super(scoreParams, gameResult, showSlideMessage);
	}	
	
	@Override
	public boolean checkMy(){
		boolean result = false;
		for(TileType t : TileType.values()){
			Integer number = getCheckLong().get(t);
			if(number!=null && number > 5){
				ISpriteTile cell = null;
				int cntCell = 0;
				for (ISpriteTile tile : getCellForMessage()){
					if(tile.getTileType() == t){
						cntCell++;
						cell = tile;
						if(cntCell == 4) break;
					}
				}
				showSlideMessage.showMessageScore(cell, setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Long), 0);
				result=true;
				scoreParams.addScore(number);
				gameResult.incCurLong();
			}
		}
		return result;
	}

}