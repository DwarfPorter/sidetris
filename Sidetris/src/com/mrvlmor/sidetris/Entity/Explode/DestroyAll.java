package com.mrvlmor.sidetris.Entity.Explode;

public class DestroyAll extends DestroyBase implements IDestroyCell {
	
	public DestroyAll(IScoreParams scoreParams) {
		super(scoreParams, setInit(scoreParams, null));
	}

	public DestroyAll(IScoreParams scoreParams, IDestroyCell destroyCell) {
		super(scoreParams, setInit(scoreParams, destroyCell));
	}

	private static IDestroyCell setInit(IScoreParams scoreParams, IDestroyCell destroyCell){
		return new DestroySaw(scoreParams, 
				new DestroyBoumerang(scoreParams,
					new DestroyBomb(scoreParams, 
							new DestroyDiamond(scoreParams,
									new DestroyMine(scoreParams,
											new DestroySpark(scoreParams,
													new DestroyCellCondition(scoreParams, destroyCell)))))));
	}
	
	
	
	@Override
	public DestroyResult destroy(int X, int Y) {
		return _destroyCell.destroy(X,Y);
	}

}
