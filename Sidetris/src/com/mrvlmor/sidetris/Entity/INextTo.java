package com.mrvlmor.sidetris.Entity;

public interface INextTo {
	void setNextTo(TileType top, TileType left, TileType right, TileType bottom);
}
