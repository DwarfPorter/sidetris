package com.mrvlmor.sidetris.Entity;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Entity.Field.IDisplayScoreBest;
import com.mrvlmor.sidetris.Entity.Field.IShowMessage;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Scene.GameScene;

public class ShowSlideMessage implements IShowMessage{
		
	
	
		private IDisplay display;
		private Setting setting;
		private GameScene gameScene;
		private FactoryText factoryText;
		
		public ShowSlideMessage(GameScene gameScene, FactoryText factoryText, IDisplay display) {
			this.gameScene = gameScene;
			this.factoryText = factoryText;
			setting = new Setting();
			this.display = display;
		}

		
		
		public void showMessageSuperScore(float x, float y, String text, long partScore)
		{
			showMessageEmpty(text, x, y, gameScene.getWindowScore(), new IDisplayScoreBest() {
				
				public void display(long partScore) {
//					synchronized (this) {
//						//score += partScore;
//					}
					display.displayScore();
				}
			}, partScore);
		}

		
		public void showMessageScore(ISpriteTile tileXY, String text, long partScore)
		{
			showMessage(text, tileXY, gameScene.getWindowScore(), new IDisplayScoreBest() {
				
				public void display(long partScore) {
//					synchronized (this) {
//						//score += partScore;
//					}
					display.displayScore();
				}
			}, partScore);
		}
		
		public void showMessageBest(String text, long partScore){
			showMessage(text, gameScene.getWindowScore(), gameScene.getWindowBest(), new IDisplayScoreBest() {
				
				public void display(long partScore) {
//					synchronized (this) {
//						best = partScore;
//					}
					display.displayBest();
				}

			}, partScore);

		}
		
		public void showMessageLevel(String text, long partScore){
			showMessageLevel(text, gameScene.getWindowScore(), gameScene.getWindowLevel(), new IDisplayScoreBest() {
				
				public void display(long partScore) {
//					synchronized (this) {
//						best = partScore;
//					}
					display.displayLevel();
				}

			}, partScore);

		}
		
		public void showMessageLastChance(int x, int y, String text, long partScore){
			showMessageLastChance(text, x, y, gameScene.getWindowScore(), new IDisplayScoreBest() {
				
				public void display(long partScore) {
//					synchronized (this) {
//						best = partScore;
//					}
					display.displayScore();
				}

			}, partScore);

		}
		
		public void showMessage(String text, IEntity source, Sprite dest, IDisplayScoreBest display, long partScore){
			Text combo = showMessageStep1(text);
			combo.setY(ConvertCoord.toCenterTextY((IAreaShape)source, combo));
			combo.setX(ConvertCoord.toCenterTextX((IAreaShape)source, combo));
			showMessageStep2(combo, dest, display, partScore);
		}
		
		public void showMessageEmpty(String text, float x, float y, Sprite dest, IDisplayScoreBest display, long partScore){
			Text combo = showMessageStep1(text);
			combo.setY(ConvertCoord.toCenterY(combo, y));
			combo.setX(ConvertCoord.toCenterX(combo, x));
//			combo.setY(y);
//			combo.setX(x);
			showMessageStep2Shake(combo, dest, display, partScore, 2f, 1, 0);
		}
		
		public void showMessageLevel(String text, Sprite source, Sprite dest, IDisplayScoreBest display, long partScore){
			Text combo = showMessageStep1Level(text);
			combo.setY(ConvertCoord.toCenterTextY(source, combo));
			combo.setX(ConvertCoord.toCenterTextX(source, combo));
			showMessageStep2Shake(combo, dest, display, partScore, 2f, 1, 0);
		}
		
		public void showMessageLastChance(String text,  int x, int y, Sprite dest, IDisplayScoreBest display, long partScore){
			Text combo = showMessageStep1(text);
			float rx = ConvertCoord.toRealX(x) + setting.getCellWidth()/2;
			float ry = ConvertCoord.toRealY(y) + setting.getCellHeight()/2;
			combo.setY(ConvertCoord.toCenterY(combo, ry));
			combo.setX(ConvertCoord.toCenterX(combo, rx));
			showMessageStep2Shake(combo, dest, display, partScore, 0.5f, 0.8f, 1.2f);
		}
		
		
		protected Text showMessageStep1(String text)
		{
			return showMessageStep1Core(text, 1, 1, 0, 0.5f);
		}
		
		protected Text showMessageStep1Level(String text){
			return showMessageStep1Core(text, 1, 0, 0, 0.5f);
		}
		
		private Text showMessageStep1Core(String text, float pRed, float pGreen, float pBlue, float scale){
			Text combo = factoryText.createText(text);//new Text(0, 0, gameScene.getResource().getFontCongratulation(), text , gameScene.getActivity().getVertexBufferObjectManager());
			combo.setColor(pRed,pGreen,pBlue);
			combo.setScale(scale);
			return combo;
		}
		
		protected void showMessageStep2(Text combo, Sprite destination, final IDisplayScoreBest display, final long partScore){
			
			final float velocity = 0.9f;
			final float velocityLong = 1f;

			gameScene.attachChild(combo);

			combo.registerEntityModifier(new ScaleModifier(Speed.getVelocity(velocity), 0.5f, 1));
			
			combo.registerEntityModifier(new MoveModifier(Speed.getVelocity(velocityLong), combo.getX(), ConvertCoord.toCenterTextX(destination, combo), combo.getY(), ConvertCoord.toCenterTextY(destination, combo), new IEntityModifierListener(){
		
				public void onModifierStarted(IModifier<IEntity> pModifier,
						IEntity pItem) {
				}
		
				public void onModifierFinished(IModifier<IEntity> pModifier,
						final IEntity pItem) {
					display.display(partScore);
					if (pItem.hasParent()){
						gameScene.getActivity().runOnUpdateThread(new Runnable() {
				            public void run() {
				                /* Now it is safe to remove the entity! */
				            	gameScene.detachChild(pItem);
				            	factoryText.disposeText((Text) pItem);
				            }
				        });
		
					}
				}
				
			}));
			combo.registerEntityModifier(new AlphaModifier(Speed.getVelocity(velocity), 1, 0));
		}	
		
		
		protected void showMessageStep2Shake(Text combo, Sprite destination, final IDisplayScoreBest display, final long partScore, float beginScale, float endScale, float middleScale){
			
			final float velocity = 1f*3;
			final float velocityLong = 1f;

			gameScene.attachChild(combo);

			if (middleScale == 0)
				combo.registerEntityModifier(new ScaleModifier(Speed.getVelocity(velocity), beginScale, endScale));
			else
				combo.registerEntityModifier(new SequenceEntityModifier(
						new ScaleModifier(Speed.getVelocity(velocity/2), beginScale, middleScale),
						new ScaleModifier(Speed.getVelocity(velocity/2), middleScale, endScale)));
			
			combo.registerEntityModifier(new SequenceEntityModifier(
					
					new MoveModifier(Speed.getVelocity(velocityLong), combo.getX(), ConvertCoord.toCenterX(combo, 400), combo.getY(), ConvertCoord.toCenterY(combo, setting.getVerticalOffset()+400)),
					new RotationModifier(Speed.getVelocity(velocityLong/6), 0, 30),
					new RotationModifier(Speed.getVelocity(velocityLong/3), 30, -30),
					new RotationModifier(Speed.getVelocity(velocityLong/3), -30, 30),
					new RotationModifier(Speed.getVelocity(velocityLong/6), 30, 0),
			new MoveModifier(Speed.getVelocity(velocityLong), ConvertCoord.toCenterX(combo, 400), ConvertCoord.toCenterTextX(destination, combo), ConvertCoord.toCenterY(combo, setting.getVerticalOffset()+400), ConvertCoord.toCenterTextY(destination, combo), new IEntityModifierListener(){
				
		public void onModifierStarted(IModifier<IEntity> pModifier,
				IEntity pItem) {
		}

		public void onModifierFinished(IModifier<IEntity> pModifier,
				final IEntity pItem) {
			display.display(partScore);
			if (pItem.hasParent()){
				gameScene.getActivity().runOnUpdateThread(new Runnable() {
		            public void run() {
		                /* Now it is safe to remove the entity! */
		            	gameScene.detachChild(pItem);
		            	factoryText.disposeText((Text) pItem);
		            }
		        });

			}
		}
		
	})		
			
					));
			combo.registerEntityModifier(new AlphaModifier(Speed.getVelocity(velocity), 1, 0));
		}	
		
		
	}