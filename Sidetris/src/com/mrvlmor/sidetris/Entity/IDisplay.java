package com.mrvlmor.sidetris.Entity;

public interface IDisplay{
	void displayLevel();
	void displayBest();
	void displayScore();
}