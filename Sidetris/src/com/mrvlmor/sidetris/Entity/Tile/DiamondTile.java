package com.mrvlmor.sidetris.Entity.Tile;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.sprite.vbo.ITiledSpriteVertexBufferObject;
import org.andengine.opengl.shader.ShaderProgram;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.DrawType;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.INextTo;
import com.mrvlmor.sidetris.Entity.TileType;

public class DiamondTile extends TiledSprite implements ISpriteTile, INextTo {
	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;
	
	private boolean _isExploding = false;
	
	private TiledSprite neighborUp;
	private TiledSprite neighborLeft;
	private TiledSprite neighborRight;
	private TiledSprite neighborBottom;
	private Sound sound;
	private Sound createSound;
	
	public DiamondTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		this.helperTile = new HelperSpriteTile(this, moveFinished);
		type = pTileType;
		
		neighborUp = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		neighborLeft = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		neighborRight = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		neighborBottom = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		neighborUp.setCurrentTileIndex(5);
		neighborLeft.setCurrentTileIndex(3);
		neighborRight.setCurrentTileIndex(4);
		neighborBottom.setCurrentTileIndex(2);
	}
	
	public Sound getSound(){
		return sound;
	}

	public Sound getCreateSound(){
		return createSound;
	}
	
	public int getCellX() {
		return mCellX;
	}

	public int getCellY() {
		return mCellY;
	}
	public void setCellX(int x){
		mCellX = x;
	}
	
	public void setCellY(int y){
		mCellY = y;
	}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
		neighborUp.setPosition(this);
		neighborLeft.setPosition(this);
		neighborRight.setPosition(this);
		neighborBottom.setPosition(this);
	}

	public void setNextTo(TileType up, TileType left, TileType right, TileType bottom){
		neighborUp.setColor(up.getRedColor(), up.getGreenColor(), up.getBlueColor());
		neighborLeft.setColor(left.getRedColor(), left.getGreenColor(), left.getBlueColor());
		neighborRight.setColor(right.getRedColor(), right.getGreenColor(), right.getBlueColor());
		neighborBottom.setColor(bottom.getRedColor(), bottom.getGreenColor(), bottom.getBlueColor());
	}
	
	public void startCell(){
		
	}
	
	public TileType getTileType() {
		return type;
	}

	public void setMoveCell(int cellX, int cellY) {
		neighborUp.registerEntityModifier(new MoveModifier(
				Speed.getVelocity(0.25f), getX(),
				ConvertCoord.toRealX(cellX),
				getY(),
				ConvertCoord.toRealY(cellY)));
		neighborLeft.registerEntityModifier(new MoveModifier(
				Speed.getVelocity(0.25f), getX(),
				ConvertCoord.toRealX(cellX),
				getY(),
				ConvertCoord.toRealY(cellY)));
		neighborRight.registerEntityModifier(new MoveModifier(
				Speed.getVelocity(0.25f), getX(),
				ConvertCoord.toRealX(cellX),
				getY(),
				ConvertCoord.toRealY(cellY)));
		neighborBottom.registerEntityModifier(new MoveModifier(
				Speed.getVelocity(0.25f), getX(),
				ConvertCoord.toRealX(cellX),
				getY(),
				ConvertCoord.toRealY(cellY)));
		
		helperTile.setMoveCell(cellX, cellY);

	}

	public boolean isExploding() {
		return _isExploding;
	}

	public void setExploding(){
	_isExploding =true;}
	public void clearExploding()
	{_isExploding = false;
	}
	public void createCell() {
		
		helperTile.createCell();
	}

	public void destroyCell(float duration) {
		neighborUp.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) , new ScaleModifier(Speed.getVelocity(0.24f), 1f, 0.1f)));
		neighborLeft.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new ScaleModifier(Speed.getVelocity(0.24f), 1f, 0.1f)));
		neighborRight.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new ScaleModifier(Speed.getVelocity(0.24f), 1f, 0.1f)));
		neighborBottom.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new ScaleModifier(Speed.getVelocity(0.24f), 1f, 0.1f)));
		helperTile.destroyCell(duration);
	}
	public void attachMe(Scene scene) {
		scene.attachChild(neighborUp);
		scene.attachChild(neighborLeft);
		scene.attachChild(neighborRight);
		scene.attachChild(neighborBottom);
		
		scene.attachChild(this);
	}

	public void detachMe(Scene scene) {
        neighborUp.setScale(1);
        neighborLeft.setScale(1);
        neighborRight.setScale(1);
        neighborBottom.setScale(1);

		neighborUp.setColor(1,1,1);
		neighborLeft.setColor(1,1,1) ;
		neighborRight.setColor(1,1,1);
		neighborBottom.setColor(1,1,1);

		scene.detachChild(neighborUp);
		scene.detachChild(neighborLeft);
		scene.detachChild(neighborRight);
		scene.detachChild(neighborBottom);
		
		scene.detachChild(this);
	}
	public boolean isEqual(ISpriteTile eq){
        return helperTile.isEqual(eq);
    }

	public boolean isInitialExplode() {
		return false;
	}
}
