package com.mrvlmor.sidetris.Entity.Tile;

import java.util.Random;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.RotationAtModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Explode.PlaySound;
import com.mrvlmor.sidetris.Scene.GameScene;

import org.andengine.entity.IEntity;

public class SawTile extends AnimatedSprite implements ISpriteTile {
	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;
	
	private boolean _isExploding = false;
	
	private TiledSprite pieces[];
	private int bottomScreen;
	private Random rnd;
	private Sound sound;
	private Setting setting = new Setting();
	private Sound createSound;
	
	public SawTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		this.helperTile = new HelperSpriteTile(this, moveFinished);
		type = pTileType;
		
		Setting setting = new Setting();
		bottomScreen = setting.getFinishCellY() + setting.getVerticalOffset() + setting.getCellHeight();
		rnd = new Random();
		
		pieces = new TiledSprite[7];
		for(int i=0; i < 7; i++){
			pieces[i] = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY), pTextureRegion, pVertexBufferObjectManager);
			pieces[i].setCurrentTileIndex(i+3);
		}
	}
	
	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}
	
	public int getCellX() {
		return mCellX;
	}

	public int getCellY() {
		return mCellY;
	}
	public void setCellX(int x){
		mCellX = x;
	}
	
	public void setCellY(int y){
		mCellY = y;
	}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
	}

	public void startCell(){
		animate(new long[]{ Speed.getVelocity (50),Speed.getVelocity (50),Speed.getVelocity (50)}, new int[]{2,1,0}, true);
	}
	
	public TileType getTileType() {
		return type;
	}

	public void setMoveCell(int cellX, int cellY) {
		helperTile.setMoveCell(cellX, cellY);
	}

	public boolean isExploding() {
		return _isExploding;
	}

	public void setExploding(){
	_isExploding =true;}
	public void clearExploding()
	{_isExploding = false;
	}
	public void createCell() {
		
		helperTile.createCell();
	}

	public void destroyCell(float duration) {
		helperTile.destroyCell(duration);
	}

	public void setSawed(ISpriteTile tile){
		if (tile == null) return;
		tile.setVisible(false);
		if (getCellX() == Field.startCell) registerEntityModifier(new MoveXModifier(Speed.getVelocity(0.1f), getX(), getX() + setting.getCellWidth()/2));
		if (getCellY() == Field.startCell) registerEntityModifier(new MoveYModifier(Speed.getVelocity(0.1f), getY(), getY() + setting.getCellHeight()/2));
		if (getCellX() == Field.finishCell) registerEntityModifier(new MoveXModifier(Speed.getVelocity(0.1f), getX(), getX() - setting.getCellWidth()/2));
		if (getCellY() == Field.finishCell) registerEntityModifier(new MoveYModifier(Speed.getVelocity(0.1f), getY(), getY() - setting.getCellHeight()/2));
		

		for(int i=0; i < 7; i++){
			if (pieces[i].getParent() == null)
				getParent().attachChild(pieces[i]);
			pieces[i].setColor(tile.getTileType().getRedColor(), tile.getTileType().getGreenColor(), tile.getTileType().getBlueColor());
			pieces[i].setPosition(tile);
			pieces[i].registerEntityModifier(new MoveModifier(Speed.getVelocity(0.4f), pieces[i].getX(), rnd.nextInt(800), pieces[i].getY(), bottomScreen));

		}
		
		PlaySound.play(sound);
		
	}
	
	public void attachMe(Scene scene) {
		// TODO Auto-generated method stub
		scene.attachChild(this);
	}

	public void detachMe(Scene scene) {
		// TODO Auto-generated method stub
		scene.detachChild(this);
		
		for(int i=0; i < 7; i++){
			if (pieces[i].getParent() == null) continue;
			setColor(1,1,1);
			scene.detachChild(pieces[i]);
		}
		
	}
	
	public boolean isEqual(ISpriteTile eq){
        return helperTile.isEqual(eq);
    }
//    public boolean isSpecial(){
//    	return true;
//    }

	public boolean isInitialExplode() {
		
		return true;
	}
}
