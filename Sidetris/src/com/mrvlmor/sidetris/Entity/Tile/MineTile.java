package com.mrvlmor.sidetris.Entity.Tile;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Direction;
import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.Field;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Scene.GameScene;

public class MineTile extends TiledSprite implements ISpriteTile {
	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;
	
	private boolean _isExploding = false;
	
	private IMoveFinished moveFinished;
	
    private TiledSprite up;
    private TiledSprite down;
    private TiledSprite left;
	private TiledSprite right;
	private Sound sound;
	private Sound createSound;
	
	public MineTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		this.moveFinished = new MyMoveFinished(moveFinished);
		this.helperTile = new HelperSpriteTile(this, this.moveFinished);
		type = pTileType;
	
	    up = new TiledSprite(0,0, pTextureRegion, pVertexBufferObjectManager );
	    down = new TiledSprite(0,0, pTextureRegion, pVertexBufferObjectManager );
	    left = new TiledSprite(0,0, pTextureRegion, pVertexBufferObjectManager );
	    right = new TiledSprite(0,0, pTextureRegion, pVertexBufferObjectManager );
	    up.setCurrentTileIndex(2);
	    down.setCurrentTileIndex(5);
	    left.setCurrentTileIndex(3);
	    right.setCurrentTileIndex(4);
        up.setVisible(false);
        down.setVisible(false);
        right.setVisible(false);
        left.setVisible(false);
	}
	
	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}

	public int getCellX() {
		return mCellX;
	}

	public int getCellY() {
		return mCellY;
	}
	public void setCellX(int x){
		mCellX = x;
	}
	
	public void setCellY(int y){
		mCellY = y;
	}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
	}

	public void startCell(){
        startAnimate();
	}

    private LoopEntityModifier tossing;

    private void initTossing(){
        float startX = ConvertCoord.toRealX(getCellX())-2;
        float finishX = ConvertCoord.toRealX( getCellX()) + 2;
        float startY= ConvertCoord.toRealY(getCellY())-2;
        float finishY = ConvertCoord.toRealY(getCellY())+ 2;
        tossing = new LoopEntityModifier(new ParallelEntityModifier(
        		new SequenceEntityModifier(
        				new RotationModifier(Speed.getVelocity(2f), -10, 10), 
        				new RotationModifier(Speed.getVelocity(2f), 10, -10)),
        		new SequenceEntityModifier(
        				new MoveModifier(Speed.getVelocity(1f), startX, finishX, startY, startY),
        				new MoveModifier(Speed.getVelocity(1f), finishX, finishX, startY, finishY),  
        				new MoveModifier(Speed.getVelocity(1f), finishX, startX, finishY, finishY), 
        				new MoveModifier(Speed.getVelocity(1f), startX, startX, finishY, startY))));
    }
	
    public void startAnimate(){
       initTossing();
       registerEntityModifier(tossing);
    }

   public void clearAnimate(){
         unregisterEntityModifier(tossing);
    }

	public TileType getTileType() {
		return type;
	}

	public void setMoveCell(int cellX, int cellY) {
		clearAnimate();
		helperTile.setMoveCell(cellX, cellY);
	}

	public boolean isExploding() {
		return _isExploding;
	}

	public void setExploding(){
	_isExploding =true;
	}
	
	public void clearExploding()
	{_isExploding = false;
	}
	public void createCell() {
		
		helperTile.createCell();
	}

	public void destroyCell(float duration) {
		setExploding();
		explodeCell(duration);
	}
	
	public void explodeCell(float duration){
		clearAnimate();
		setCurrentTileIndex(1);
		
		final MineTile that = this;
		
		registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new ScaleModifier(Speed.getVelocity(0.3f), 1f, 2f),
				new DelayModifier(Speed.getVelocity(0.1f), new IEntityModifierListener() {
					public void onModifierStarted(IModifier<IEntity> pModifier,
							IEntity pItem) {
					}

					public void onModifierFinished(
							IModifier<IEntity> pModifier, IEntity pItem) {
								clearExploding();
								moveFinished.destroyComplete(that);
				        }

					}) {
				}));
		
		registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new DelayModifier(Speed.getVelocity(0.1f)), new AlphaModifier(Speed.getVelocity(0.3f), 1,0)));
		
		if (getCellY() > Field.minCell){
			up.setVisible(true);
        
			up.setPosition(ConvertCoord.toRealX(getCellX()), ConvertCoord.toRealY(getCellY()));

			up.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveYModifier(Speed.getVelocity(0.1f * (getCellY() - Field.minCell)), getY(), ConvertCoord.toRealY(Field.minCell), 
					new IEntityModifierListener() {
				public void onModifierStarted(IModifier<IEntity> pModifier,
						IEntity pItem) {
				}

				public void onModifierFinished(
						IModifier<IEntity> pModifier, IEntity pItem) {
						pItem.setVisible(false);
			        }

				})));
		}
		if (getCellX() > Field.minCell){
			left.setVisible(true);
        
			left.setPosition(ConvertCoord.toRealX(getCellX()), ConvertCoord.toRealY(getCellY()));

			left.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveXModifier(Speed.getVelocity(0.1f * (getCellX() - Field.minCell)), getX(), ConvertCoord.toRealX(Field.minCell), 
					new IEntityModifierListener() {
				public void onModifierStarted(IModifier<IEntity> pModifier,
						IEntity pItem) {
				}

				public void onModifierFinished(
						IModifier<IEntity> pModifier, IEntity pItem) {
						 pItem.setVisible(false);
			        }

				})));
		}		
		if (getCellY() < Field.maxCell){
			down.setVisible(true);
      
			down.setPosition(ConvertCoord.toRealX(getCellX()), ConvertCoord.toRealY(getCellY()));

			down.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveYModifier(Speed.getVelocity(0.1f * (Field.maxCell - getCellY())), getY(), ConvertCoord.toRealY(Field.maxCell), 
					new IEntityModifierListener() {
				public void onModifierStarted(IModifier<IEntity> pModifier,
						IEntity pItem) {
				}

				public void onModifierFinished(
						IModifier<IEntity> pModifier, IEntity pItem) {
						    pItem.setVisible(false);
			        }

				})));
		}
		if (getCellX() < Field.maxCell){
			right.setVisible(true);
			right.setPosition(ConvertCoord.toRealX(getCellX()), ConvertCoord.toRealY(getCellY()));

			right.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveXModifier(Speed.getVelocity(0.1f * (Field.maxCell - getCellX())), getX(), ConvertCoord.toRealX(Field.maxCell), 
					new IEntityModifierListener() {
				public void onModifierStarted(IModifier<IEntity> pModifier,
						IEntity pItem) {
				}

				public void onModifierFinished(
						IModifier<IEntity> pModifier, IEntity pItem) {
							 pItem.setVisible(false);
			        }

				})));
		}	

	}
	
	public void attachMe(Scene scene) {
		scene.attachChild(up);
        scene.attachChild(down);
        scene.attachChild(left);
        scene.attachChild(right);
		scene.attachChild(this);
	}


	 
	public void detachMe(Scene scene) {
		setAlpha(1);
		setScale(1);
		setCurrentTileIndex(0);
		scene.detachChild(this);
        if (up.getParent()!=null) scene.detachChild(up);
        if(down.getParent()!=null) scene.detachChild(down);
        if(right.getParent()!=null) scene.detachChild(right);
        if(left.getParent()!=null) scene.detachChild(left);
        up.setVisible(false);
        down.setVisible(false);
        left.setVisible(false);
        right.setVisible(false);
	}

	public boolean isEqual(ISpriteTile eq) {
		return false;
	}

	public boolean isInitialExplode() {
		return false;
	}

	private class MyMoveFinished implements IMoveFinished
	{

    IMoveFinished moveFinished;
	    public MyMoveFinished(IMoveFinished moveF){
	        moveFinished = moveF;
	
	    }
	
	    public void move(Direction d){
	    	moveFinished.move(d);
	    }
	
	    public void moveComplete(){
	    	startAnimate();
	    	moveFinished.moveComplete();
	    }
		public void destroyComplete(ISpriteTile spriteTile){
			moveFinished.destroyComplete(spriteTile);
		}
		public void createComplete(){
			moveFinished.createComplete();
		}



    
	}


}