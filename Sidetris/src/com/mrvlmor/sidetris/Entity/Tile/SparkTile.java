package com.mrvlmor.sidetris.Entity.Tile;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.PathModifier.Path;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.INextTo;
import com.mrvlmor.sidetris.Entity.TileType;



public class SparkTile extends TiledSprite implements ISpriteTile, INextTo {

	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;
	
	private boolean _isExploding = false;
	private IMoveFinished moveFinished;
    private TiledSprite neighbors[];
	private TiledSprite pieces[];
	private int bottomScreen;
	private int leftScreen = -100;
	private int rightScreen = 800;
	private Random rnd;
    private ITiledTextureRegion textureRegion;
    private VertexBufferObjectManager vertexBufferObjectManager;
    private Setting setting;
    private Sound sound;
    private Sound createSound;
 
    public SparkTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
        this.sound = sound;
        this.createSound = createSound;
		this.moveFinished = moveFinished;
		this.helperTile = new HelperSpriteTile(this, this.moveFinished);
		type = pTileType;
		textureRegion=pTextureRegion;
		vertexBufferObjectManager = pVertexBufferObjectManager;
        neighbors=new TiledSprite[4];
		for(int i=0; i < 4; i++){
			TiledSprite n;
			n = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
					pTextureRegion, pVertexBufferObjectManager);
			n.setCurrentTileIndex(1);
			n.setPosition(this);
			neighbors[i]=n;
		}
		
		setting = new Setting();
		bottomScreen = setting.getFinishCellY() + setting.getVerticalOffset() + setting.getCellHeight();
		rnd = new Random();
		
		pieces = new TiledSprite[4];
		for(int i=0; i < 4; i++){
			pieces[i] = new TiledSprite(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY), pTextureRegion, pVertexBufferObjectManager);
			pieces[i].setCurrentTileIndex(i+2);
			pieces[i].setPosition(this);
		}
		
	}
	
	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}
	
	public int getCellX() {
		return mCellX;
	}

	public int getCellY() {
		return mCellY;
	}
	public void setCellX(int x){
		mCellX = x;
	}
	
	public void setCellY(int y){
		mCellY = y;
	}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
		for(int i=0; i < 4; i++){
			neighbors[i].setPosition(this);
		}
	}

	public void setNextTo(TileType up, TileType left, TileType right, TileType bottom){
		//up
		neighbors[0].setColor(up.getRedColor(), up.getGreenColor(), up.getBlueColor());
		//left
		neighbors[1].setColor(left.getRedColor(), left.getGreenColor(), left.getBlueColor());
		//right
		neighbors[2].setColor(right.getRedColor(), right.getGreenColor(), right.getBlueColor());
		//bottom
		neighbors[3].setColor(bottom.getRedColor(), bottom.getGreenColor(), bottom.getBlueColor());
	}
	
	public void startCell(){
		startAnimate();

	}

    private IEntityModifier sparkling []={null,null,null,null} ;


   private void startAnimate(){
        float x[][]={{20,80,20,80},
			{80,20,80,20},
			{20,20,20,20},
			{80,20,20,20}};

        float y[][]={{20,20,80,20},
			{20,20,20,20},
			{20,80,20,80},
			{20,20,80,80}};
        
        x[0][0]+=rnd.nextInt(60);
        x[0][2]+=rnd.nextInt(60);
        y[0][1]+=rnd.nextInt(60);
        y[0][3]+=rnd.nextInt(60);
        
        x[1][1]+=rnd.nextInt(60);
        y[1][0]+=rnd.nextInt(60);
        y[1][2]+=rnd.nextInt(60);
        y[1][3]+=rnd.nextInt(60);
        
        x[2][0]+=rnd.nextInt(60);
        x[2][1]+=rnd.nextInt(60);
        x[2][2]+=rnd.nextInt(60);
        x[2][3]+=rnd.nextInt(60);

        y[3][0]+=rnd.nextInt(60);
        y[3][1]+=rnd.nextInt(60);
        x[3][2]+=rnd.nextInt(60);
        x[3][3]+=rnd.nextInt(60);
	
        for(int i=0;i<4;i++ ){
             for(int j=0;j<4;j++){
                 x[i][j]=ConvertCoord.toRealX(getCellX())+x[i][j]-setting.getCellWidth()/2;
                 y[i][j]=ConvertCoord.toRealY(getCellY())+y[i][j]-setting.getCellHeight()/2;
         }
        	TiledSprite n = neighbors[i];
        	n.setPosition(x[i][0],y[i][0]);
        	sparkling[i]=new LoopEntityModifier(new PathModifier(Speed.getVelocity(1f), new Path(5)
        																					.to(x[i][0],y[i][0])
        																					.to(x[i][1],y[i][1])
        																					.to(x[i][2],y[i][2])
        																					.to(x[i][3],y[i][3])
        																					.to(x[i][0],y[i][0])));
        	n.registerEntityModifier(sparkling[i]);
		}
    }

    private void clearAnimate(){
    	for( int i=0;i<4;i++ ){
    		neighbors[i].unregisterEntityModifier(sparkling[i]);
    	}

    }
	
	public TileType getTileType() {
		return type;
	}

	public void setMoveCell(int cellX, int cellY) {


		clearAnimate();
		for(int i=0; i < 4; i++){
			TiledSprite n = neighbors[i];
			n.registerEntityModifier(new MoveModifier(
					Speed.getVelocity(0.25f), n.getX(),
					ConvertCoord.toRealX(cellX),
					n.getY(),
					ConvertCoord.toRealY(cellY)));
		}

		
		helperTile.setMoveCell(cellX, cellY);

	}

	public boolean isExploding() {
		return _isExploding;
	}

	public void setExploding(){
	_isExploding =true;}
	public void clearExploding()
	{_isExploding = false;
	}
	public void createCell() {
		
		helperTile.createCell();
	}
	
	public void attachMe(Scene scene) {
		
		for(int i=0; i < 4; i++){
			TiledSprite n = neighbors[i];
			n.setPosition(this);
            scene.attachChild(n);
		}

		scene.attachChild(this);
	}
	
	public void detachMe(Scene scene) {
		
		for( int i=0;i<4;i++ ){
    		neighbors[i].detachSelf();
    	}
		
		for(TiledSprite n:spanTarget){
			n.setColor(1,1,1);
			if (n.hasParent()) n.detachSelf();
		}
		
		for(int i = 0; i< 4; i++){
			pieces[i].detachSelf();
		}
		
		scene.detachChild(this);
	}
	public boolean isEqual(ISpriteTile eq){
        return helperTile.isEqual(eq);
    }

	public boolean isInitialExplode() {
		return false;
	}

	public void destroyCell(float duration) {
		setExploding();
		explodeCell(duration);
	}
	
	
	public void setNumberSpark(List<ISpriteTile> target){
		
        int i=0;
        for(ISpriteTile t:target){
            TiledSprite p= addSpan(t.getTileType(), i);
            if(!p.hasParent()) 	getParent().attachChild(p); 
            p.registerEntityModifier(new MoveModifier (Speed.getVelocity(0.6f),p.getX(),ConvertCoord.toRealX(t.getCellX()),p.getY(),ConvertCoord.toRealY(t.getCellY())));
            i++;
        }

		
	}
	
    private List<TiledSprite> spanTarget=new ArrayList<TiledSprite>();

    private TiledSprite addSpan(TileType tileType, int pos)
    {
         if(spanTarget.size()>pos){
            TiledSprite t=spanTarget.get(pos);
             t.setColor(tileType.getRedColor(), tileType.getGreenColor(), tileType.getBlueColor());
             t.setPosition(this);
             return t;
         }
        if(pos<4){
            neighbors[pos].setColor(tileType.getRedColor(), tileType.getGreenColor(), tileType.getBlueColor());
            spanTarget.add(neighbors[pos]);
            return neighbors[pos];
         }
        TiledSprite n=new TiledSprite(getX(), getY(), textureRegion, vertexBufferObjectManager);
        n.setPosition(this);
			n.setCurrentTileIndex(1);
        n.setColor(tileType.getRedColor(), tileType.getGreenColor(), tileType.getBlueColor());
        spanTarget.add(n);
        return n;
    }

	public void explodeCell(float duration){
        

		for(int k = 0; k< 4; k++){
			pieces[k].setPosition(this);
			getParent().attachChild(pieces[k]);
		}
		
		final SparkTile that = this;
		
        pieces[0].registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) , new MoveModifier(Speed.getVelocity(0.3f),getX(),leftScreen, getY(),rnd.nextInt(800)+200)));
        pieces[1].registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveModifier(Speed.getVelocity(0.3f),getX(),rightScreen,getY(),rnd.nextInt(800)+400)));
        pieces[2].registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveModifier(Speed.getVelocity(0.3f),getX(),rnd.nextInt(500)+400,getY(), bottomScreen)));
        pieces[3].registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new MoveModifier(Speed.getVelocity(0.3f),getX(),rnd.nextInt(300),getY(), bottomScreen)));
        
        registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new DelayModifier(Speed.getVelocity(0.5f), 
        		new IEntityModifierListener() {
				public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
				}
	
				public void onModifierFinished(
						IModifier<IEntity> pModifier, IEntity pItem) {
							clearExploding();
							moveFinished.destroyComplete(that);
		        }

        })));

		// TODO:
		setVisible(false);
          
		
	}
	
/*	private class MyMoveFinished implements IMoveFinished
	{

    IMoveFinished moveFinished;
	    public MyMoveFinished(IMoveFinished moveF){
	        moveFinished = moveF;
	
	    }
	
	    public void move(Direction d){
	    	moveFinished.move(d);
	    }
	
	    public void moveComplete(){
	    	startAnimate();
	    	moveFinished.moveComplete();
	    }
		public void destroyComplete(ISpriteTile spriteTile){
			moveFinished.destroyComplete(spriteTile);
		}
		public void createComplete(){
			moveFinished.createComplete();
		}



    
	}*/
	
}
