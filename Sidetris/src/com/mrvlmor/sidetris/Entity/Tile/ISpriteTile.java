package com.mrvlmor.sidetris.Entity.Tile;
import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;

import com.mrvlmor.sidetris.Entity.TileType;

public interface ISpriteTile extends IEntity {

	public abstract int getCellX();

	public abstract int getCellY();
	public abstract void setCellX(int x);
	public abstract void setCellY(int y);

	public abstract void setCell(int cellX, int cellY);

	public abstract TileType getTileType();

	public abstract void setMoveCell(int cellX, int cellY);

	public abstract boolean isExploding();
	public abstract void setExploding();
	public abstract void clearExploding();

	public abstract void createCell();

	public abstract void destroyCell(float duration);
	

	public abstract void startCell();
	
	public abstract void attachMe(Scene scene);
	public abstract void detachMe(Scene scene);
    public abstract boolean isEqual(ISpriteTile eq);
    //public abstract boolean isSpecial();
    public abstract boolean isInitialExplode();
    
    public abstract Sound getSound();
    public abstract Sound getCreateSound();
}