package com.mrvlmor.sidetris.Entity.Tile;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.TileType;

public class BombTile extends AnimatedSprite implements ISpriteTile {
	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;
	
	private boolean _isExploding = false;
	private Sound sound;
	private IMoveFinished moveFinished;
	private Sound createSound;
	
	public BombTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		this.helperTile = new HelperSpriteTile(this, moveFinished);
		type = pTileType;
		this.moveFinished = moveFinished;
	}

	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}
	
	public int getCellX() {
		return mCellX;
	}

	public int getCellY() {
		return mCellY;
	}
	public void setCellX(int x){
		mCellX = x;
	}
	
	public void setCellY(int y){
		mCellY = y;
	}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
	}

	public void startCell(){
		animate(new long[]{ Speed.getVelocity (80),Speed.getVelocity (110),Speed.getVelocity (120),Speed.getVelocity (100),
				Speed.getVelocity (80),Speed.getVelocity (110),Speed.getVelocity (120),Speed.getVelocity (100),
				Speed.getVelocity (100),Speed.getVelocity (110),Speed.getVelocity (120),Speed.getVelocity (90)
				}, 
				new int[] {1, 0, 2, 0, 1, 0, 2, 3, 2, 0, 2, 0}, true);
	}
	
	public TileType getTileType() {
		// TODO Auto-generated method stub
		return type;
	}

	public void setMoveCell(int cellX, int cellY) {
		// TODO Auto-generated method stub
		helperTile.setMoveCell(cellX, cellY);
	}

	public boolean isExploding() {
		// TODO Auto-generated method stub
		return _isExploding;
	}

	public void setExploding(){
	_isExploding =true;
	}
	
	public void clearExploding()
	{_isExploding = false;
	}
	public void createCell() {
		
		helperTile.createCell();
	}

	public void destroyCell(float duration) {
		setExploding();
		explodeCell(duration);
	}
	public void attachMe(Scene scene) {
		scene.attachChild(this);
	}

	public void explodeCell(float duration){
		stopAnimation(4);
		
		final BombTile that = this;
		
		registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new ScaleModifier(Speed.getVelocity(0.4f),
				1f, 4f, new IEntityModifierListener() {

					public void onModifierStarted(IModifier<IEntity> pModifier,
							IEntity pItem) {

					}

					public void onModifierFinished(
							IModifier<IEntity> pModifier, IEntity pItem) {
								clearExploding();
								moveFinished.destroyComplete(that);
				        }

					}

				)));
		
		registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)) ,new DelayModifier(Speed.getVelocity(0.2f)), new AlphaModifier(Speed.getVelocity(0.2f), 1, 0.5f)));

	}
	
	public void detachMe(Scene scene) {
		setAlpha(1);
		setScale(1);
		setCurrentTileIndex(0);
		scene.detachChild(this);

	}
	
	public boolean isEqual(ISpriteTile eq){
        return helperTile.isEqual(eq);
    }

	public boolean isInitialExplode() {
		return false;
	}
}
