package com.mrvlmor.sidetris.Entity.Tile;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.TileType;

import org.andengine.entity.modifier.PathModifier.Path;

public class BoumerangTile extends TiledSprite implements ISpriteTile {

	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;

	private boolean _isExploding = false;
	private IMoveFinished moveFinished;
	private Sound sound;
	private Sound createSound;
	
	public BoumerangTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished,Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		this.moveFinished = moveFinished;
		helperTile = new HelperSpriteTile(this, moveFinished);
		type = pTileType;
	}

	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}
	
	public int getCellX() {
		return this.mCellX;
	}

	public int getCellY() {
		return this.mCellY;
	}	
	public void setCellX(int x)
	{mCellX = x;}
	public void setCellY(int y){
		mCellY = y;}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
		
	}

	public TileType getTileType() {
		return type;
	}

	public void startCell(){
		
		registerEntityModifier(new LoopEntityModifier(new RotationModifier(Speed.getVelocity(2f), 0, -360))
		);
	}
	
	public void setMoveCell(int cellX, int cellY) {
		helperTile.setMoveCell(cellX, cellY);
	}


	public boolean isExploding() {
		return _isExploding;
	}
	public void setExploding(){
	_isExploding=true;
	}
	public void clearExploding()
	{
	_isExploding=false;}

	public void createCell() {
		helperTile.createCell();
	
	}


	private int[] SX;
	private int[] SY;

	public void setThrought(int[] SX, int[] SY){

		this.SX=SX;
		this.SY=SY;

		
	}
	
	public void destroyCell(float duration) {
		setExploding();
		final BoumerangTile that = this;
		registerEntityModifier( 
				new PathModifier(Speed.getVelocity(1f), new Path(4)
																.to(ConvertCoord.toRealX(SX[0]), ConvertCoord.toRealY(SY[0]))
																.to(ConvertCoord.toRealX(SX[1]), ConvertCoord.toRealY(SY[1]))
																.to(ConvertCoord.toRealX(SX[2]), ConvertCoord.toRealY(SY[2]))
																.to(ConvertCoord.toRealX(SX[3]), ConvertCoord.toRealY(SY[3]))
																

					, new IEntityModifierListener() {

					public void onModifierStarted(IModifier<IEntity> pModifier,
							IEntity pItem) {

					}

					public void onModifierFinished(
							IModifier<IEntity> pModifier, IEntity pItem) {
							clearExploding();
							moveFinished.destroyComplete(that);
				        }

				}
						));
	//	helperTile.destroyCell(0);//duration+1.5f);
	}

	public void attachMe(Scene scene) {
		scene.attachChild(this);
	}

	public void detachMe(Scene scene) {
		scene.detachChild(this);
	}
    public boolean isEqual(ISpriteTile eq){

        return helperTile.isEqual(eq);
    }

	public boolean isInitialExplode() {
		return true;
	}
}


