package com.mrvlmor.sidetris.Entity.Tile;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.modifier.ColorModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.TileType;

public class BlackTile extends TiledSprite implements ISpriteTile {

	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;

	private HelperSpriteTile helperTile;
	private Sound sound;
	private boolean _isExploding = false;
	private Sound createSound;

	public BlackTile(final int pCellX, final int pCellY,
			final ITiledTextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		helperTile = new HelperSpriteTile(this, moveFinished);
		type = pTileType;
	}

	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}
	
	public int getCellX() {
		return this.mCellX;
	}

	public int getCellY() {
		return this.mCellY;
	}	
	public void setCellX(int x)
	{mCellX = x;}
	public void setCellY(int y){
		mCellY = y;}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
		
	}

	public TileType getTileType() {
		return type;
	}

	public void startCell(){
		registerEntityModifier(new LoopEntityModifier(new SequenceEntityModifier(
				new ColorModifier(Speed.getVelocity(1f), 1f, 0.2f, 1f, 0.2f, 1f, 0.2f),
				new ColorModifier(Speed.getVelocity(1f), 0.2f, 1f, 0.2f, 1f, 0.2f, 1f)))
		);
	}
	
	public void setMoveCell(int cellX, int cellY) {
		helperTile.setMoveCell(cellX, cellY);
	}


	public boolean isExploding() {
		return _isExploding;
	}
	public void setExploding(){
	_isExploding=true;
	}
	public void clearExploding()
	{
	_isExploding=false;}

	public void createCell() {
		helperTile.createCell();
	
	}

	public void destroyCell(float duration) {

		helperTile.destroyCell(duration);


	}

	public void attachMe(Scene scene) {
		scene.attachChild(this);
	}

	public void detachMe(Scene scene) {
		scene.detachChild(this);
	}
    public boolean isEqual(ISpriteTile eq){

        return helperTile.isEqual(eq);
    }

	public boolean isInitialExplode() {
		return false;
	}
}
