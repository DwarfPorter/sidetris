package com.mrvlmor.sidetris.Entity.Tile;

import org.andengine.audio.sound.Sound;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.ConvertCoord;
import com.mrvlmor.sidetris.Entity.HelperSpriteTile;
import com.mrvlmor.sidetris.Entity.IMoveFinished;
import com.mrvlmor.sidetris.Entity.TileType;

public class SpriteTile extends Sprite implements ISpriteTile {

	protected int mCellX;
	protected int mCellY;

	protected TileType type = TileType.NOTHING;
	private HelperSpriteTile helperTile;
	private boolean _isExploding = false;
	private Sound sound;
	private Sound createSound;

	public SpriteTile(final int pCellX, final int pCellY,
			final ITextureRegion pTextureRegion,
			final VertexBufferObjectManager pVertexBufferObjectManager,
			TileType pTileType, IMoveFinished moveFinished, Sound sound, Sound createSound) {
		super(ConvertCoord.toRealX(pCellX), ConvertCoord.toRealY(pCellY),
				pTextureRegion, pVertexBufferObjectManager);
		this.mCellX = pCellX;
		this.mCellY = pCellY;
		this.sound = sound;
		this.createSound = createSound;
		helperTile = new HelperSpriteTile(this, moveFinished);
		type = pTileType;
	}
	
	public Sound getSound(){
		return sound;
	}
	
	public Sound getCreateSound(){
		return createSound;
	}

	public int getCellX() {
		return this.mCellX;
	}

	public int getCellY() {
		return this.mCellY;
	}	
	public void setCellX(int x)
	{mCellX = x;}
	public void setCellY(int y){
		mCellY = y;}
	public void setCell(int cellX, int cellY) {
		helperTile.setCell(cellX, cellY);
		
	}

	public TileType getTileType() {
		return type;
	}

	public void startCell(){}
	
	public void setMoveCell(int cellX, int cellY) {
		helperTile.setMoveCell(cellX, cellY);
	}


	public boolean isExploding() {
		return _isExploding;
	}
	public void setExploding(){
	_isExploding=true;
	}
	public void clearExploding()
	{
	_isExploding=false;}

	public void createCell() {
		helperTile.createCell();
	
	}

	public void destroyCell(float duration) {
		// this.animate(new long[] {Speed.getVelocity(120),
		// Speed.getVelocity(120)}, 1, 2, false);
		
		helperTile.destroyCell(duration);


	}

	public void attachMe(Scene scene) {
		scene.attachChild(this);
	}

	public void detachMe(Scene scene) {
		scene.detachChild(this);
	}
    public boolean isEqual(ISpriteTile eq){

        return helperTile.isEqual(eq);
    }
//    public boolean isSpecial(){
//    	return false;
//    }

	public boolean isInitialExplode() {
		return false;
	}
}
