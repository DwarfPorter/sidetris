package com.mrvlmor.sidetris.Entity;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random; 
   
import org.andengine.audio.sound.Sound;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import android.util.Log;

import com.mrvlmor.machinestate.helper.Direction;

import com.mrvlmor.sidetris.RestoreState;
import com.mrvlmor.sidetris.SaveState; 
import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Entity.Explode.CheckAll;
import com.mrvlmor.sidetris.Entity.Explode.DestroyAll;
import com.mrvlmor.sidetris.Entity.Explode.DestroyResult;
import com.mrvlmor.sidetris.Entity.Explode.ICheckCell;
import com.mrvlmor.sidetris.Entity.Explode.IDestroyCell;
import com.mrvlmor.sidetris.Entity.Explode.IScoreParams;
import com.mrvlmor.sidetris.Entity.Explode.PlaySound;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Scene.GameScene;

public class Field {

	private ISpriteTile[][] _field={{null,null,null,null,null,null,null},
								   {null,null,null,null,null,null,null},
								   {null,null,null,null,null,null,null},
								   {null,null,null,null,null,null,null},
								   {null,null,null,null,null,null,null},
								   {null,null,null,null,null,null,null},
								   {null,null,null,null,null,null,null}};
	
	private FactorySpriteTile factorySpriteTile;
	FactoryText factoryText;
	GameScene gameScene;
	private Random rnd;
	private IMoveFinished moveFinished;
	
	public static final int minCell = 1;	// start game field
	public static final int maxCell = 5;	// finish game field
    public static int midCell=3;
	public static final int startCell = 0; // start whole field
	public static final int finishCell = 6;// finish whole field
	
	private DirectionStrategy directionStrategy;
	private UpStrategy upStrategy = new UpStrategy();
	private DownStrategy downStrategy = new DownStrategy();
	private LeftStrategy leftStrategy = new LeftStrategy();
	private RightStrategy rightStrategy = new RightStrategy();	
	
	private int cntMoving=0;
	private int cntExploding = 0;
	private int cntCreating=0;
	
	
	int prepScore = 0;
	
	
	private Sound soundMove = null;
	//private Sound soundCreate = null;
	private Display display;
	private Explode explode;

	
	private GameResult gameResult;
	
	private Setting setting = new Setting();
	private ShowSlideMessage showSlideMessage;
	
	private Sprite help;
	private Text helpText;
	private boolean isHelp = false;
	
	private RandomCell randomCell;
	
	public Field(GameScene gameScene, IMoveFinished moveFinished){
		this.gameScene = gameScene;
		this.moveFinished = moveFinished;
		
		gameResult = new GameResult(gameScene);
		//soundMove = gameScene.getResource().getMove_sound();
				
		factorySpriteTile = new FactorySpriteTile(gameScene.getResource(), this.moveFinished);
		factoryText = new FactoryText(gameScene.getResource());
		rnd = new Random();
		
		randomCell = new RandomCell();
		
		display = new Display();
		showSlideMessage = new ShowSlideMessage(gameScene, factoryText, display);
		gameResult.restoreBest();
		
		display.displayScore();
		display.displayBest();
		display.displayLevel();
	
		explode = new Explode();
		

		if (getBest() == 0){
			showHelp();
			isHelp = true; 
		}
		
		
	}
	
	private void showHelp(){
		help = new Sprite(setting.getCellWidth() + setting.getMinCellX(), setting.getCellHeight() + setting.getVerticalOffset() + setting.getMinCellY(), gameScene.getResource().getHelp(), gameScene.getActivity().getVertexBufferObjectManager());
		gameScene.attachChild(help);
		helpText = new Text(ConvertCoord.toCenterShapeX(help),  ConvertCoord.toCenterShapeY(help) - 100, gameScene.getResource().getFontCongratulation(),  setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Swipe), gameScene.getActivity().getVertexBufferObjectManager());
		helpText.setX(ConvertCoord.toCenterTextX(help, helpText));
		helpText.setY(ConvertCoord.toCenterTextY(help, helpText) - 100);
		
		gameScene.attachChild(helpText);
	}
	
	private void removeHelp(){
		gameScene.getActivity().runOnUiThread(new Runnable() {
			public void run() {
				if (help != null){
					help.detachSelf();
					help = null;
				}
				if (helpText != null){
					helpText.detachSelf();
					helpText = null;
				}
			}
		});
	}
	

	public GameResult getGameResult(){
		return gameResult;
	}
	
	public long getScore(){
		return gameResult.getCurScore();
	}
	public long getBest(){
		return gameResult.getBestScore();
	}
	
	public long getLevel(){
		return gameResult.getCurLevel();
	}
	
	public boolean isAllComplete(){
		if(cntMoving > 0) return false;
		if(cntExploding > 0) return false;
		if(cntCreating > 0) return false;
		return true;
	}
	
	public boolean checkGameOver(){
		if(!isAllComplete()) return false; 
	
		for(int i = startCell; i <= finishCell; i++ ){
			if (_field[i][startCell] != null && _field[i][startCell].isInitialExplode()) return false;
			if (_field[i][finishCell] != null && _field[i][finishCell].isInitialExplode()) return false;
			if (_field[startCell][i] != null && _field[startCell][i].isInitialExplode()) return false;
			if (_field[finishCell][i] != null && _field[finishCell][i].isInitialExplode()) return false;
		}

		for(int X=minCell; X<=maxCell; X++ ){
			for(int Y=minCell; Y<=maxCell; Y++){
				if (_field[X][Y] == null) return false;
			}
		}

		gameResult.save();

		return true;
	}
	

	public void allComplete(){
		if(!isAllComplete()) return;
		
		move();

		if(cntMoving > 0) return;
		
		displayMessageLevel();
		
		setRandomCell();
		
		explode.setInitComplete();

	}
	
	public void displayMessageLevel(){
		if (gameResult.setCurLevel()){
			showSlideMessage.showMessageLevel(String.format("%s %d",setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Level), gameResult.getCurLevel()), 0);
		}
	}
	
	public void setRandomCell(){

		if(!isAllComplete()) return;
		
		if (gameResult.setBestScore()){
			showSlideMessage.showMessageBest(setting.getLanguage().getString(com.mrvlmor.sidetris.R.string.Best), 0);
		}
		
		explode.clearCombo();
		PlaySound.clearSound();
		//PlaySound.setSound(soundCreate);
		
		for(int i=minCell; i<=maxCell; i++){
			initCell(i, startCell);
			initCell(i, finishCell);
			initCell(startCell, i);
			initCell(finishCell, i);
		}
        setNeighbor();
        PlaySound.play();
	}

	private void initCell(int x, int y){
		if (_field[x][y] != null) return;
		_field[x][y] = factorySpriteTile.createSpriteTile(x, y, randomCell.getRandomTile());
		PlaySound.setSound(_field[x][y].getCreateSound(), _field[x][y].getTileType().isSpecial());
		cntCreating++;
		_field[x][y].attachMe(gameScene);
		_field[x][y].startCell();
	}
	

	
	public void move(Direction direction){
		if (isHelp){
			isHelp = false;
			removeHelp();
		}
		gameResult.incCurStep();
		showProgressBar();
		
		switch(direction){
		case DOWN:
			directionStrategy = downStrategy;
			break;
		case UP:
			directionStrategy = upStrategy;
			break;
		case LEFT:
			directionStrategy = leftStrategy;
			break;
		case RIGHT:
			directionStrategy = rightStrategy;
			break;
		default:
			return;
		}
		move();
	}
	
	
	private void move(){
       explode.incCombo();
		setCoordMove();
		movingCells();
		if (cntMoving == 0) explode();
	}
	
	private void setCoordMove(){
		for(int X=directionStrategy.initXfor(); directionStrategy.checkLimitXfor(X) ; X = directionStrategy.nextXfor(X)){
			for(int Y=directionStrategy.initYfor(); directionStrategy.checkLimitYfor(Y); Y = directionStrategy.nextYfor(Y)){
				setCoordMoveCell(X, Y);
			}
		} 
	}
	
	private void setCoordMoveCell(int X, int Y){
		if (directionStrategy.checkLimitDirection(X, Y)) return;
		if (_field[X][Y] == null) return;
		int X1 = directionStrategy.nextX(X);
		int Y1 = directionStrategy.nextY(Y);
		if (_field[X1][Y1] != null) return;
		_field[X1][Y1] = _field[X][Y];
		_field[X][Y] = null;
		setCoordMoveCell(X1, Y1);
		return;
	}
	
	private void movingCells(){
		for(int X=startCell; X <= finishCell; X++){
			for(int Y=startCell; Y <= finishCell; Y++){
				if (_field[X][Y] != null){
					if (_field[X][Y].getCellX() != X || _field[X][Y].getCellY() != Y){
						_field[X][Y].setMoveCell(X, Y);
						cntMoving++;
					}
				}
			}
		}
	}

	
	public void moveComplete(){
		cntMoving--;
	}
	
	public void createComplete() {
		cntCreating--;
	}
	public void destroyCell(final ISpriteTile cell) {
		if (cell.hasParent()){
			gameScene.getActivity().runOnUpdateThread(new Runnable() {
	            public void run() {
	                /* Now it is safe to remove the entity! */
	            	cell.detachMe(gameScene);
	            	
	            }
	        });

		}
		cntExploding--;
		_field[cell.getCellX()][cell.getCellY()] = null;
		factorySpriteTile.disposeSpriteTile(cell);
	}
	
	public void save() {
		gameResult.save();
		SaveState saveState = new SaveState(gameScene.getActivity());
	
		saveState.saveField(_field);
		saveState.commit();
		
	}
	
	public void restore() {
		gameResult.restore();
		
		RestoreState restoreState = new RestoreState(gameScene.getActivity());

		display.displayScore();
		display.displayBest();
		display.displayLevel();
		
		if (getBest() == 0) return;
				
		restoreState.restoreField(_field, factorySpriteTile);
		for(int X=0; X < _field.length; X++){
			for(int Y=0; Y < _field[X].length; Y++){
				if(_field[X][Y] == null) continue;
				_field[X][Y].attachMe(gameScene);
				_field[X][Y].startCell();
			
			}
		}
		showProgressBar();
		setNeighbor();
	}
	
	private void showProgressBar(){

		Sprite progressBar = gameScene.getProgressBar();
		progressBar.setPosition(gameResult.getPositionXProgressBar() + setting.getHorizontalProgressOffset(), progressBar.getY());
		progressBar.setWidth(gameResult.getWidthProgressBar());

	}
	
	private void setNeighbor(){
		TileType up;
        TileType bottom;
        TileType right;
        TileType left;
        for(int x = startCell;x<=finishCell;x++)
	        for(int y=startCell;y<=finishCell;y++){
	           if(_field[x][y]== null) continue;
	           if(!(_field[x][y] instanceof INextTo)) continue;
	           up = y==startCell || _field[x][y-1] ==null ? TileType.NOTHING : _field[x][y-1].getTileType();
	           bottom = y==finishCell || _field[x][y+1] ==null ? TileType.NOTHING : _field[x][y+1].getTileType();
	           left = x==startCell || _field[x-1][y] ==null ? TileType.NOTHING : _field[x-1][y].getTileType();
	           right = x==finishCell || _field[x+1][y] ==null ? TileType.NOTHING : _field[x+1][y].getTileType();
	           INextTo neighbor = (INextTo) _field[x][y];
	           neighbor.setNextTo(up,left,right,bottom);
	        }
	}
	
	public void explode(){
		explode.processExplode();
	}
	

	class Explode{
	
	    private ICheckCell checkings;
	    private ScoreParams scoreParams = new ScoreParams();
	    private IDestroyCell destroyers;
	    
    	private int cntCombo=0;
		
	    public Explode(){
	    	checkings = new CheckAll(scoreParams, gameResult, showSlideMessage);
	    	destroyers = new DestroyAll(scoreParams);
			setInitComplete();
	    }
	    
	    public void clearCombo(){
            cntCombo=0;
	    }

    	public void incCombo(){
            cntCombo++;
    	}
		
		public void setInitComplete(){
			checkings.initNextTurn();
		}
		
		public void processExplode(){
		
			if(cntMoving > 0) return;
			
			PlaySound.clearSound();
			PlaySound.setSound(soundMove);
			
			scoreParams.nextTurn();
            setNeighbor();
            
//Special Tiles
			int C2=directionStrategy.initPosition();
			for(int C1=minCell; C1 <= maxCell; C1++){
					destroyers.destroyFirst(directionStrategy.getCoordfromC1C2(C1,C2), directionStrategy.getCoordfromC1C2(C2, C1));
			}

			for(int X=minCell; X<=maxCell; X++ ){
				for(int Y=minCell; Y<=maxCell; Y++){
					destroyers.destroyCell(X, Y);
				}
			}
			
			for(int X=minCell; X<=maxCell; X++ ){
				for(int Y=minCell; Y<=maxCell; Y++){
					destroyers.destroyLast(X,Y);
				}
			}
			
			PlaySound.play();
			
			if (prepScore == 0) return;
			
			boolean isShowMessage = false;
			
			if (checkings.check())
				isShowMessage=true;
			
			gameResult.addCurScore(prepScore);
			
			if (!isShowMessage)
				display.displayScore();
	
			gameResult.setBest();
			
		}

		public class ScoreParams implements IScoreParams{
		    
			private Map<TileType, Integer> checkLong = new EnumMap<TileType, Integer>(TileType.class); 
		    private List<ISpriteTile> cellForMessage = new ArrayList<ISpriteTile>();
		
			public void SetScoreParams(DestroyResult dr) {
				if (!dr.IsDestroy) return;
				if (dr.Tile == null) return;
			
				PlaySound.setSound(dr.Tile.getSound(), dr.Tile.getTileType().isSpecial());
				
				TileType type = dr.Tile.getTileType();
				Integer cLong = checkLong.get(type);
				if (cLong == null) cLong = 0;
				cLong += dr.QntDestroyedTiles;
				checkLong.put(type, cLong);
				cellForMessage.add(dr.Tile);
			}

			public void nextTurn(){
				checkLong.clear();
				cellForMessage.clear();
				prepScore=0;
			}
			
			public Map<TileType, Integer> getCheckLong(){
				return checkLong;
			}
			
			public List<ISpriteTile> getCellForMessage(){
				return cellForMessage;
			}
			
			
			public ISpriteTile[][] getField() {
				return _field;
			}

			public ISpriteTile getField(int X, int Y) {
				return _field[X][Y];
			}

			public void incExploding() {
				cntExploding++;
			}

			public void incScore() {
				addScore(1);
			}
			
			public void addScore(int score) {
				prepScore+=score;
			}
			
			public void multiScore(int score) {
				prepScore*=score;
			}

			public DirectionStrategy getDirectionStrategy() {
				return directionStrategy;
			}
			
			public int getCntCombo(){
				return cntCombo;
			}
		
			public int getPrepScore(){
				return prepScore;
			}
			
			public long getTotalScore(){
				return getScore();
			}
		}
	}
	
	
	private class RandomCell{
		private static final int MaxTile =11;
		private static final int MaxSpecTile = 7;
		
		private static final long levelBomb =4;
		private static final long levelDiamond =5;
		private static final long levelSaw =5;
		private static final long levelMine =6;
        private static final long levelSpark=7;
  		private static final long levelBoumerang =7;
		private static final long levelBlack = 10;

		
		private boolean isSpecialTile(){
			for(int i = startCell; i <= finishCell; i++ ){
				if (_field[i][startCell] != null && _field[i][startCell].getTileType().isSpecial()) return true;
				if (_field[i][finishCell] != null && _field[i][finishCell].getTileType().isSpecial()) return true;
				if (_field[startCell][i] != null && _field[startCell][i].getTileType().isSpecial()) return true;
				if (_field[finishCell][i] != null && _field[finishCell][i].getTileType().isSpecial()) return true;
			}
			return false;
		}	
		
		private int countSpecialTile(){
			int cnt=0;
			for(int i = startCell; i <= finishCell; i++ ){
				if (_field[i][startCell] != null && _field[i][startCell].getTileType().isSpecial()) cnt++;
				if (_field[i][finishCell] != null && _field[i][finishCell].getTileType().isSpecial()) cnt++;
				if (_field[startCell][i] != null && _field[startCell][i].getTileType().isSpecial()) cnt++;
				if (_field[finishCell][i] != null && _field[finishCell][i].getTileType().isSpecial()) cnt++;
			}
			return cnt;
		}
		
		private int countFreeTilesField(){
			int count = 0;
			for(int X=minCell; X<=maxCell; X++){
				for(int Y=minCell; Y<=maxCell; Y++)
				{
					if(_field[X][Y] == null) count++;
                 if(_field[X][Y] != null && _field[X][Y].isExploding()) count ++;
				}
			}
			return count;
		}
		
		
		public TileType getRandomTile(){
			// level = 1 then 3 tiles!!!
			
			//if (!isSpecialTile()){
				
				if (getLevel() >= levelBomb || getLevel() >= levelDiamond || getLevel() >= levelSaw || getLevel() >= levelMine || getLevel() >= levelBlack || getLevel() >= levelSpark || getLevel() >= levelBoumerang) {
				
					int maxSpecTile = countFreeTilesField()*2 + MaxSpecTile + MaxTile*(countSpecialTile()+1);
					
					int rndWhat = rnd.nextInt(maxSpecTile);
					switch(rndWhat){
					case 0: 
						if (getLevel() >= levelSaw)	return TileType.SAW;
					case 1:
						if (getLevel() >= levelDiamond)	return TileType.DIAMOND;
					case 2: 
						if(getLevel() >= levelBomb)	return TileType.BOMB;
					case 3:
						if(getLevel() >= levelMine) return TileType.MINE;
					case 4: 
						if(getLevel() >= levelBlack) return TileType.BLACK;
                    case 5:
                    	if(getLevel()>=levelSpark) return TileType.SPARK;
                    case 6:
                    	if(getLevel()>=levelBoumerang) return TileType.BOUMERANG;
					}
				}
			//}
	      	
			int maxTile = (int) getLevel() + 2;
			if (maxTile > MaxTile ) maxTile = MaxTile;
			int tile = rnd.nextInt(maxTile);
			switch(tile){
			case 0: return TileType.BLUE;
			case 1: return TileType.GREEN;
			case 2: return TileType.RED;
			case 3:	return TileType.YELLOW;
			case 4: return TileType.VIOLET;
			case 5: return TileType.CYAN;
			case 6: return TileType.ORANGE;
			case 7: return TileType.DARKBLUE;
			case 8: return TileType.DARKGREEN;
			case 9: return TileType.BROWN;
			case 10: return TileType.PINK;
			}
			return TileType.NOTHING;	
		}
	}
	
	public abstract class DirectionStrategy{
		
		public abstract boolean checkLimitDirection(int X, int Y);
		public abstract int nextX(int X); 
		public abstract int prevX(int X);
		public abstract int nextY(int Y);
		public abstract int prevY(int Y);
		
		public abstract int initXfor();
		public abstract int initYfor();

		public abstract int nextYfor(int Y);
		public abstract int nextXfor(int X);
		public abstract boolean checkLimitXfor(int X);
		public abstract boolean checkLimitYfor(int Y);
		
		public abstract int initPosition();

		public abstract int getCoordfromC1C2(int C1, int C2);
	
		public abstract int initC1();
		public abstract int nextC1(int C1);
        public abstract boolean limitC1(int C1);
	    public abstract DirectionStrategy toLeft();
	    public abstract DirectionStrategy toRight();
        public abstract int checkMiddle(int x,int y);
        public DirectionStrategy toPerpendicular(int x,int y){
            if(checkMiddle(x,y)>0) return toLeft();
            if(checkMiddle(x,y)<0) return toRight();
            if(rnd.nextBoolean()) return toLeft();
            return toRight();
        }
	}
	
	private class UpStrategy extends DirectionStrategy{

		@Override
		public boolean checkLimitDirection(int X, int Y) {
			return Y == minCell;
		}

		@Override
		public int initPosition() {
			return finishCell;
		}

		@Override
		public int getCoordfromC1C2(int C1, int C2){
		 return C1;
		}


		@Override
		public int nextX(int X) {
			return X;
		}

		@Override
		public int nextY(int Y) {
			return Y-1;
		}

		@Override
		public int initXfor() {
			return minCell;
		}

		@Override
		public int initYfor() {
			return minCell+1;
		}

		@Override
		public int nextYfor(int Y) {
			return Y+1;
		}

		@Override
		public int nextXfor(int X) {
			return X+1;
		}

		@Override
		public boolean checkLimitXfor(int X) {
			return X <= maxCell;
		}

		@Override
		public boolean checkLimitYfor(int Y) {
			return Y <= finishCell;
		}

		@Override
		public int prevX(int X) {
			return X;
		}

		@Override
		public int prevY(int Y) {
			return Y+1;
		}
		
		@Override
		public int initC1(){
			return maxCell;
		}
		
		@Override
		public int nextC1(int C1){
			return C1-1;
		}

        public boolean limitC1(int C1){
            return C1>minCell;
        }

        public int checkMiddle(int x,int y){
        	return x-3;
        }
        
	    public DirectionStrategy toLeft(){
            return leftStrategy;
        }

	    public DirectionStrategy toRight(){
            return rightStrategy;
        }
		
	}
	
	private class DownStrategy extends DirectionStrategy{

		@Override
		public boolean checkLimitDirection(int X, int Y) {
			return Y == maxCell;
		}

		@Override
		public int initPosition() {
			return startCell;
		}
		 	
	
		@Override
		public int getCoordfromC1C2(int C1, int C2){
		 return C1;
		}

		@Override
		public int nextX(int X) {
			return X;
		}

		@Override
		public int nextY(int Y) {
			return Y+1;
		}

		@Override
		public int initXfor() {
			return minCell;
		}

		@Override
		public int initYfor() {
			return maxCell-1;
		}

		@Override
		public int nextYfor(int Y) {
			return Y-1;
		}

		@Override
		public int nextXfor(int X) {
			return X+1;
		}

		@Override
		public boolean checkLimitXfor(int X) {
			return X <= maxCell;
		}

		@Override
		public boolean checkLimitYfor(int Y) {
			return Y >= startCell;
		}

		@Override
		public int prevX(int X) {
			return X;
		}

		@Override
		public int prevY(int Y) {
			return Y-1;
		}
		
		@Override
		public int initC1(){
			return minCell;
		}
		
		
		@Override
		public int nextC1(int C1){
			return C1+1;
		}

        public boolean limitC1(int C1){
            return C1<maxCell;
        }

	    public DirectionStrategy toLeft(){
            return leftStrategy;
        }

	    public DirectionStrategy toRight(){
            return rightStrategy;
        }
		
        public int checkMiddle(int x,int y){
        	return x-3;
        }
	}
	
	private class LeftStrategy extends DirectionStrategy{

		@Override
		public boolean checkLimitDirection(int X, int Y) {
			return X == minCell;
		}
		
		@Override
		public int initPosition() {
			return finishCell;
		}


		@Override
		public int getCoordfromC1C2(int C1, int C2){
		 return C2;
		}

		@Override
		public int nextX(int X) {
			return X-1;
		}

		@Override
		public int nextY(int Y) {
			return Y;
		}

		@Override
		public int initXfor() {
			return minCell+1;
		}

		@Override
		public int initYfor() {
			return minCell;
		}

		@Override
		public int nextYfor(int Y) {
			return Y+1;
		}

		@Override
		public int nextXfor(int X) {
			return X+1;
		}

		@Override
		public boolean checkLimitXfor(int X) {
			return X <= finishCell;
		}

		@Override
		public boolean checkLimitYfor(int Y) {
			return Y <= maxCell;
		}

		@Override
		public int prevX(int X) {
			return X+1;
		}

		@Override
		public int prevY(int Y) {
			return Y;
		}
		
		@Override
		public int initC1(){
			return maxCell;
		}
		
		
		@Override
		public int nextC1(int C1){
			return C1-1;
		}

        public boolean limitC1(int C1){
            return C1>minCell;
        }

	    public DirectionStrategy toLeft(){
            return upStrategy;
        }

	    public DirectionStrategy toRight(){
            return downStrategy;
        }

        public int checkMiddle(int x,int y){
        	return y-3;
        }


	}

	private class RightStrategy extends DirectionStrategy{

		@Override
		public boolean checkLimitDirection(int X, int Y) {
			return X == maxCell;
		}

		@Override
		public int initPosition() {
			return startCell;
		}
		
		@Override
		public int getCoordfromC1C2(int C1, int C2){
		 return C2;
		}

		@Override
		public int nextX(int X) {
			return X+1;
		}

		@Override
		public int nextY(int Y) {
			return Y;
		}

		@Override
		public int initXfor() {
			return maxCell-1;
		}

		@Override
		public int initYfor() {
			return minCell;
		}

		@Override
		public int nextYfor(int Y) {
			return Y+1;
		}

		@Override
		public int nextXfor(int X) {
			return X-1;
		}

		@Override
		public boolean checkLimitXfor(int X) {
			return X >= startCell;
		}

		@Override
		public boolean checkLimitYfor(int Y) {
			return Y <= maxCell;
		}

		@Override
		public int prevX(int X) {
			return X-1;
		}

		@Override
		public int prevY(int Y) {
			return Y;
		}
		
		@Override
		public int initC1(){
			return minCell;
		}
		
		@Override
		public int nextC1(int C1){
			return C1+1;
		}
		
      public boolean limitC1(int C1){
            return C1<maxCell;
        }

	    public DirectionStrategy toLeft(){
            return upStrategy;
        }

	    public DirectionStrategy toRight(){
            return downStrategy;
        }
	    
        public int checkMiddle(int x,int y){
        	return y-3;
        }
	}


	private class Display implements IDisplay{
		
		private Text scoreText;
		private Text bestText;
		private Text level;
		
		public Display(){
			scoreText = new Text(gameScene.getWindowScore().getX(), gameScene.getWindowScore().getY(), gameScene.getResource().getFontMenu(), String.format("%6d", gameResult.getCurScore()), gameScene.getActivity().getVertexBufferObjectManager());
		    scoreText.setY(ConvertCoord.toCenterTextY(gameScene.getWindowScore(), scoreText));
		    scoreText.setX(ConvertCoord.toCenterTextX(gameScene.getWindowScore(), scoreText));
			gameScene.attachChild(scoreText); 

			
		    bestText = new Text(gameScene.getWindowBest().getX(), gameScene.getWindowBest().getY(), gameScene.getResource().getFontMenu(), String.format("%6d", gameResult.getBestScore()), gameScene.getActivity().getVertexBufferObjectManager());
		    bestText.setY(ConvertCoord.toCenterTextY(gameScene.getWindowBest(), bestText));
		    bestText.setX(ConvertCoord.toCenterTextX(gameScene.getWindowBest(), bestText));
			gameScene.attachChild(bestText); 
			
			level = new Text(gameScene.getWindowBest().getX(), gameScene.getWindowBest().getY(), gameScene.getResource().getFontMenu(), String.format("%6d", gameResult.getCurLevel()), gameScene.getActivity().getVertexBufferObjectManager());
			level.setY(ConvertCoord.toCenterTextY(gameScene.getWindowLevel(), level));
			level.setX(ConvertCoord.toCenterTextX(gameScene.getWindowLevel(), level));
			gameScene.attachChild(level); 
		}
		
		public void displayScore(){
			final String StrScore = String.format("%d", getScore());
			gameScene.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					scoreText.setText(StrScore);
					scoreText.setX(ConvertCoord.toCenterTextX(gameScene.getWindowScore(), scoreText));
				}
			});
		}
		
		public void displayBest(){
			final String StrScore = String.format("%d", getBest());
			gameScene.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					bestText.setText(StrScore);
					bestText.setX(ConvertCoord.toCenterTextX(gameScene.getWindowBest(), bestText));
				}
			});
		}
		
		public void displayLevel(){
			final String StrScore = String.format("%d", getLevel());
			gameScene.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					level.setText(StrScore);
					level.setX(ConvertCoord.toCenterTextX(gameScene.getWindowLevel(), level));
				}
			});
		}
	}
	
	public interface IShowMessage{
		void showMessageSuperScore(float x, float y, String text, long partScore);
		void showMessageScore(ISpriteTile tileXY, String text, long partScore);
		void showMessageBest(String text, long partScore);
		void showMessageLevel(String text, long partScore);
		void showMessageLastChance(int x, int y, String text, long partScore);
	}
	
	public interface IDisplayScoreBest{
		public void display(long partScore);
	}
	

}
