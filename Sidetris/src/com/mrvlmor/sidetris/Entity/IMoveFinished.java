package com.mrvlmor.sidetris.Entity;

import com.mrvlmor.machinestate.helper.IMove;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;

public interface IMoveFinished extends IMove {
	public void moveComplete();
	public void destroyComplete(ISpriteTile spriteTile);
	public void createComplete();
	//public void setRandomCell();
	//public void move(Direction direct);
}
