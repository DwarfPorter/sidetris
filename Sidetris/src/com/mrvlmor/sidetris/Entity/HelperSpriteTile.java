package com.mrvlmor.sidetris.Entity;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.util.modifier.IModifier;

import com.mrvlmor.machinestate.helper.Speed;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;

public class HelperSpriteTile {


	private IMoveFinished moveFinished;
	private ISpriteTile spriteTile;

	public HelperSpriteTile(ISpriteTile pSpriteTile, IMoveFinished moveFinished) {
	
		spriteTile = pSpriteTile;
		this.moveFinished = moveFinished;
	}


	public void setCell(int cellX, int cellY) {
      spriteTile.setCellX(cellX);
       spriteTile.setCellY(cellY);
      spriteTile.clearExploding();
		spriteTile.setPosition(ConvertCoord.toRealX(spriteTile.getCellX()),
				ConvertCoord.toRealY(spriteTile.getCellY()));
}

	public void setMoveCell(int cellX, int cellY) {
		setMoveCellCore(cellX, cellY, 0.25f, new IEntityModifierListener() {

			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {
			}

			public void onModifierFinished(IModifier<IEntity> pModifier,
					IEntity pItem) {
				moveFinished.moveComplete();
			}

		});
	}

	private void setMoveCellCore(int cellX, int cellY, float duration,
			IEntityModifierListener listener) {
		if ((cellX == spriteTile.getCellX()) & (cellY == spriteTile.getCellY()))
			return; 

		// save current coords
//		int oldCellX = spriteTile.getCellX();
//		int oldCellY = spriteTile.getCellY();
		float oldX = spriteTile.getX();
		float oldY = spriteTile.getY(); 

		// new coords
		spriteTile.setCellX(cellX);
		spriteTile.setCellY(cellY);

		MoveModifier moveModifier = new MoveModifier(
				Speed.getVelocity(duration), oldX,
				ConvertCoord.toRealX(spriteTile.getCellX()),
				oldY,
				ConvertCoord.toRealY(spriteTile.getCellY()), listener);
		spriteTile.registerEntityModifier(moveModifier);
	}


	public void createCell() {
		spriteTile.registerEntityModifier(new ScaleModifier(Speed.getVelocity(0.20f),
				0.1f, 1f, new IEntityModifierListener() {

			public void onModifierStarted(IModifier<IEntity> pModifier,
					IEntity pItem) {

			}

			public void onModifierFinished(
					IModifier<IEntity> pModifier, IEntity pItem) {
				moveFinished.createComplete();
			}

		}));
	}
 
    public boolean isEqual(ISpriteTile eq){
        if (eq == null) return false;
        if (eq.getTileType() == TileType.DIAMOND) return true;
        if ( spriteTile.getTileType() == TileType.DIAMOND) return true;
    	if (eq.getTileType() == TileType.BLACK) return false;
    	if (spriteTile.getTileType() == TileType.BLACK) return false;
        return spriteTile.getTileType() ==eq.getTileType();
    }
	
	public void destroyCell(float duration) {
		// this.animate(new long[] {Speed.getVelocity(120),
		// Speed.getVelocity(120)}, 1, 2, false);
		spriteTile.setExploding();
		spriteTile.registerEntityModifier(new SequenceEntityModifier(new DelayModifier(Speed.getVelocity(duration)), new ScaleModifier(Speed.getVelocity(0.24f),
				1f, 0.1f, new IEntityModifierListener() {

					public void onModifierStarted(IModifier<IEntity> pModifier,
							IEntity pItem) {

					}

					public void onModifierFinished(
							IModifier<IEntity> pModifier, IEntity pItem) {
						spriteTile.clearExploding();
						moveFinished.destroyComplete(spriteTile);
					}

				})));

	}

}
