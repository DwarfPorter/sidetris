package com.mrvlmor.sidetris.Entity;


import org.andengine.entity.IEntity;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.entity.shape.RectangularShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;

import com.mrvlmor.sidetris.Setting;

public class ConvertCoord {

	private static Setting setting = null;
	
	public ConvertCoord() {
	}
	
	// ��������� �������� ������ ������ � ����������� �� ������� �������� �� ��� ���� ����������� (����� ������� ��������)
	public static float toRealY(final int pCellY)
	{
		if (setting == null) setting = new Setting();
		switch(pCellY){
		case 0: return setting.getStartCellY() + setting.getVerticalOffset();
		case 6: return setting.getFinishCellY() + setting.getVerticalOffset();
		}
		return pCellY * setting.getCellHeight() + setting.getVerticalOffset() + setting.getMinCellY();
	}
	 
	// ���� �� �� X
	public static float toRealX(final int pCellX)
	{
		if (setting == null) setting = new Setting();
		switch(pCellX){
		case 0: return setting.getStartCellX();
		case 6: return setting.getFinishCellX();
				
		}
		return pCellX * setting.getCellWidth() + setting.getMinCellX();
	}
	
	public static float toCenterTextX(IAreaShape sprite, Text text){
		return (sprite.getWidth() - text.getWidth()) / 2 + sprite.getX();
	}
	
	public static float toCenterTextY(IAreaShape sprite, Text text){
		return (sprite.getHeight() - text.getHeight()) / 2 + sprite.getY();
	}
	
	public static float toCenterShapeX(RectangularShape shape){
		return shape.getX() + shape.getWidth()/2;
	}
	
	public static float toCenterShapeY(RectangularShape shape){
		return shape.getY() + shape.getHeight()/2;
	}
	
	public static float toCenterX(RectangularShape shape, float X){
		return X - shape.getWidth()/2;
	}
	
	public static float toCenterY(RectangularShape shape, float Y){
		return Y - shape.getHeight()/2;
	}
	
	
}