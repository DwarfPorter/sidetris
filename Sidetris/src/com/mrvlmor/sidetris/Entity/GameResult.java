package com.mrvlmor.sidetris.Entity;

import com.mrvlmor.sidetris.RestoreState;
import com.mrvlmor.sidetris.SaveState;
import com.mrvlmor.sidetris.Setting;
import com.mrvlmor.sidetris.Scene.GameScene;

// Extract this to file!!!
public class GameResult{
	
	// Statistic of game
	
	private GameScene gameScene;
	
	private static final long levelcnt = 30;
	
    private long[] curCombo = {0,0,0,0,0,0,0,0};
    private long[] bestCombo = {0,0,0,0,0,0,0,0};
    private long[] curDouble = {0,0,0,0,0};
    private long[] bestDouble = {0,0,0,0,0};
    private long curLong=0;
    private long bestLong=0;
    private long curEmpty=0;
    private long bestEmpty=0;
    private long curScore=0;
    private long bestScore=0;
    private long curLevel = 0;
    private long bestLevel = 0;
    
    private long curLastchance=0;
    private long bestLastchance=0;
    
	
	private long curStep = 0;

    public GameResult(GameScene scene){
	  gameScene = scene;
    }
	
    
    public int getPositionXProgressBar(){
    	int w = (int) getCurLevelStep();
    	return w*(int)(Setting.WidthProgress/levelcnt);
    }
    
    public int getWidthProgressBar(){
    	return Setting.WidthProgress - getPositionXProgressBar();
    }
    
	public long getCurStep()
    {
    	return curStep;
    }

	public void incCurStep()
    {
    	curStep++;
    }

    public long getCurLevel()
    {
    	return curLevel;
    }
    
    public long getCurLevelStep(){
    	return (curStep % levelcnt);
    }
    
    public boolean setCurLevel(){
    	if (getCurLevelStep() != 0) return false;
		curLevel++;
		return true;
    }
    
    public long getBestLevel(){
    	return bestLevel;
    }
    
    public void setBestLevel(){
    	if(curLevel > bestLevel) bestLevel = curLevel;
    }
    
	public void setCurEmpty(long score){
		  curEmpty = score;
	}
		
	public void incCurEmpty(){
		  curEmpty++;
	}
	
	public long getCurEmpty(){
	  return curEmpty;
	}
	
	public void setBestEmpty(){
	  if (curEmpty > bestEmpty) bestEmpty = curEmpty;
	}
	
	public long getBestEmpty(){
	  return bestEmpty;
	}

	public void setCurLastChance(long score) {
		curLastchance = score;
	}

	public void incCurLastChance() {
		curLastchance++;
	}

	public long getCurLastChance() {
		return curLastchance;
	}

	public void setBestLastChance() {
		if (curLastchance > bestLastchance)
			bestLastchance = curLastchance;
	}

	public long getBestLastChance() {
		return bestLastchance;
	}	
	
    public void setCurScore(long score){
      curScore = score;
    }

    public void addCurScore(long score){
        curScore += score * curLevel;
    }
    
	public long getCurScore(){
	  return curScore;
	}
	
	public boolean setBestScore(){
	  if (curScore <= bestScore) return false;
	  bestScore = curScore;
	  return true;
	}
	
	public long getBestScore(){
	  return bestScore;
	}
	
	public void setCurLong(long score){
	  curLong = score;
	}

	public void incCurLong(){
	  curLong++;
	}
	
	public long getCurLong(){
	  return curLong;
	}
	
	public void setBestLong(){
	  if (curLong > bestLong) bestLong = curLong;
	}
	
	public long getBestLong(){
	  return bestLong;
	}

	public void setCurCombo(long score, int index){
		curCombo[index] = score;
	}
	
	public void incCurCombo(int index){
		curCombo[index]++;
	}
	
	
	public void setBestCombo(int index){
		if (curCombo[index] > bestCombo[index]) bestCombo[index] = curCombo[index];
	}

	public void setCurDouble(long score, int index){
		curDouble[index] = score;
	}
	
	public long getCurDouble(int index){
		return curDouble[index];
	}

	public long getBestDouble(int index){
		return bestDouble[index];
	}

	public void incCurDouble(int index){
		curDouble[index]++;
	}

	public void setBestDouble(int index){
		if (curDouble[index] > bestDouble[index]) bestDouble[index] = curDouble[index];
	}
	
	public long getCurCombo(){
		long sum=0;
		for(long i: curCombo){
			sum+=i;
		}
		return sum;
	}
	
	public long getBestCombo(){
		long sum=0;
		for(long i: bestCombo){
			sum+=i;
		}
		return sum;
	}
	
	public long getCurDouble(){
		long sum=0;
		for(long i: curDouble){
			sum+=i;
		}
		return sum;
	}
	
	public long getBestDouble(){
		long sum=0;
		for(long i: bestDouble){
			sum+=i;
		}
		return sum;
	}
	
	public void saveSound(){
		SaveState saveState = new SaveState(gameScene.getActivity());
		saveState.saveVolumeSound();
		saveState.commit();
	}
	
	public void restoreSound(){
		RestoreState restoreState = new RestoreState(gameScene.getActivity());
		restoreState.restoreVolumeSound();
	}
	
	public void setBest(){
		//setBestScore();
		setBestLong();
		setBestEmpty();
		setBestLevel();
		setBestLastChance();
		for(int i=0; i< bestCombo.length; i++){
			setBestCombo(i);
		}
		for(int i=0; i<bestDouble.length;i++){
			setBestDouble(i);
		}
	}
	
	public void restoreBest(){
		RestoreState restoreState = new RestoreState(gameScene.getActivity());
		restoreBestCore(restoreState);
	}
	
	private void restoreBestCore(RestoreState restoreState){
		bestScore = restoreState.restoreScore();
		bestCombo[0] = restoreState.restoreCombo();
		bestCombo[0] += restoreState.restoreCombo(0);
		for(int i=1; i< bestCombo.length; i++){
			bestCombo[i] = restoreState.restoreCombo(i);
		}
		bestDouble[0] = restoreState.restoreDouble();
		bestDouble[1] = restoreState.restoreTriple();
		bestDouble[2] = restoreState.restoreQuadruple();
		bestDouble[3] = restoreState.restoreQuintuple();
		bestDouble[4] = restoreState.restoreHextuple();
		bestLong = restoreState.restoreLong();
		bestEmpty = restoreState.restoreEmpty();
		bestLevel = restoreState.restoreLevel();
		bestLastchance = restoreState.restoreLastChance();
	}
	
	
	public void restore(){
		RestoreState restoreState = new RestoreState(gameScene.getActivity());
		restoreBestCore(restoreState);
		
		curScore = restoreState.restoreCurrentScore();
		curCombo[0] = restoreState.restoreCurrentCombo();
		curCombo[0] += restoreState.restoreCurrentCombo(0);
		for(int i=1; i< curCombo.length; i++){
			curCombo[i] = restoreState.restoreCurrentCombo(i);
		}
		curDouble[0] = restoreState.restoreCurrentDouble();
		curDouble[1] = restoreState.restoreCurrentTriple();
		curDouble[2] = restoreState.restoreCurrentQuadruple();
		curDouble[3] = restoreState.restoreCurrentQuintuple();
		curDouble[4] = restoreState.restoreCurrentHextuple();
		curLong = restoreState.restoreCurrentLong();
		curEmpty = restoreState.restoreCurrentEmpty();
		curLevel = restoreState.restoreCurrentLevel();
		curStep = restoreState.restoreCurrentStep();
		curLastchance = restoreState.restoreCurrentLastChance();
	}
	
	public void save(){
		SaveState saveState = new SaveState(gameScene.getActivity());
		
		for(int i=1; i< curCombo.length; i++){
			 saveState.saveCurrentCombo(curCombo[i], i);
		}
		
		for(int i=1; i< bestCombo.length; i++){
			 saveState.saveCombo(bestCombo[i], i);
		}
		
		saveState.saveDouble(bestDouble[0]);
		saveState.saveTriple(bestDouble[1]);
		saveState.saveQuadruple(bestDouble[2]);
		saveState.saveQuintuple(bestDouble[3]);
		saveState.saveHextuple(bestDouble[4]);
		saveState.saveLong(bestLong);
		saveState.saveEmpty(bestEmpty);
		saveState.saveScore(bestScore);
		saveState.saveCurrentScore(curScore);
		saveState.saveCurrentLevel(curLevel);
		saveState.saveLevel(bestLevel);
		saveState.saveLastChance(bestLastchance);
		
		saveState.saveCurrentDouble(curDouble[0]);
		saveState.saveCurrentTriple(curDouble[1]);
		saveState.saveCurrentQuadruple(curDouble[2]);
		saveState.saveCurrentQuintuple(curDouble[3]);
		saveState.saveCurrentHextuple(curDouble[4]);
		saveState.saveCurrentEmpty(curEmpty);
		saveState.saveCurrentLong(curLong);
		saveState.saveCurrentStep(curStep);
		saveState.saveCurrentLastChance(curLastchance);
		saveState.commit();
	}
	
}