package com.mrvlmor.sidetris.Entity;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.text.Text;

import com.mrvlmor.sidetris.Resource.SideTrisResource;

public class FactoryText {
	
	private SideTrisResource sideTrisResource;
	private List<Text> cache = new ArrayList<Text>();
		
	public FactoryText(SideTrisResource sideTrisResource)
	{
		this.sideTrisResource = sideTrisResource;
	}
	
	public Text createText(String text){
		Text result = null;
		for(Text txt: cache){
			if(text.compareTo(txt.getText().toString()) == 0){
				result = txt;
				break;
			}
		}
		if (result != null){
			cache.remove(result);
		}
		else{
			result = new Text(0, 0, sideTrisResource.getFontCongratulation(), text , sideTrisResource.getActivity().getVertexBufferObjectManager());
		}
		return result;
	}
	
	public void disposeText(Text cell){
		cache.add(cell);
	}
	
	
}
