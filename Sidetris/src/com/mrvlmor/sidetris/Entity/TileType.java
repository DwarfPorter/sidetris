package com.mrvlmor.sidetris.Entity;

public enum TileType {
	NOTHING,
	RED,
	GREEN,
	YELLOW,
	BLUE,
	VIOLET,
	CYAN,
	ORANGE,
	BLACK,
	PINK,
	DARKGREEN,
	DARKBLUE,
	BROWN,
	BOMB,
	SAW,
	DIAMOND,
	MINE,
    SPARK,
    BOUMERANG;
	
	public boolean isSpecial(){
		switch(this){
		case BLACK:
		case DIAMOND:
		case SAW:
        case SPARK:
		case MINE: 
		case BOUMERANG:
		case BOMB: return true;
        }
		return false;
		
		
	}
	
	
	public float getRedColor(){
        switch(this){
        case SAW: return 17f/255f;
		case BLACK: return 10f/255f;
        case BLUE: return 67f/255f;
		case BROWN: return 152f/255f;
		case CYAN: return 50f/255f;
		case DARKBLUE: return 51f/255f;         
        case BOMB: return 17f/255f;
		case DARKGREEN: return 13f/255f;
		case GREEN: return 82f/255f;
		case ORANGE: return 228f/255f;
		case PINK: return 238f/255f;
		case RED: return 212f/255f;
		case VIOLET: return 169f/255f;
		case YELLOW: return 243f/255f;
		case MINE: return 32f/255f;
        case SPARK: return 0.5f;
        case BOUMERANG: return 144/255f;
        }
        return 1f;
	}
	
	public float getGreenColor(){
        switch(this){
        case SAW: return 169f/255f;
        case BLACK: return 10f/255f;
		case BLUE: return 134f/255f;
		case BROWN: return 110f/255f;
		case CYAN: return 241f/255f;
		case DARKBLUE: return 48f/255f;
		case BOMB: return 80f/255f;
		case DARKGREEN: return 66f/255f;
		case GREEN: return 197f/255f;
		case ORANGE: return 144f/255f;
		case PINK: return 22f/255f;
		case RED: return 25f/255f;
		case VIOLET: return 105f/255f;
		case YELLOW: return 237f/255f;
		case MINE: return 13f/255f;
		case SPARK: return 0.5f;
        case BOUMERANG: return 95/255f;
        }
        return 1f;
	}
	
	public float getBlueColor(){

        switch(this){
        case SAW: return 169f/255f;
		case BLACK: return 10f/255f;
		case BLUE: return 249f/255f;
		case BROWN: return 97f/255f;
		case CYAN: return 218f/255f;
		case DARKBLUE: return 136f/255f;
		case BOMB: return 17f/255f;
		case DARKGREEN: return 1f/255f;
		case GREEN: return 82f/255f;
		case ORANGE: return 17f/255f;
		case PINK: return 144f/255f;
		case RED: return 32f/255f;
		case VIOLET: return 207f/255f;
		case YELLOW: return 66f/255f;
		case MINE: return 115f/255f;
		case SPARK: return 0.5f;
        case BOUMERANG: return 10/255f;
         }
         return 1f;
		
	}
	
}
