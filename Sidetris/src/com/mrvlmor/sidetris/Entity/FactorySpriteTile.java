package com.mrvlmor.sidetris.Entity;

import java.util.ArrayList;
import java.util.List;

import org.andengine.audio.sound.Sound;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.util.Log;

import com.mrvlmor.sidetris.Entity.Tile.BlackTile;
import com.mrvlmor.sidetris.Entity.Tile.BombTile;
import com.mrvlmor.sidetris.Entity.Tile.BoumerangTile;
import com.mrvlmor.sidetris.Entity.Tile.DiamondTile;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;
import com.mrvlmor.sidetris.Entity.Tile.MineTile;
import com.mrvlmor.sidetris.Entity.Tile.SawTile;
import com.mrvlmor.sidetris.Entity.Tile.SparkTile;
import com.mrvlmor.sidetris.Entity.Tile.SpriteTile;
import com.mrvlmor.sidetris.Resource.GameResource;
import com.mrvlmor.sidetris.Resource.SideTrisResource;

public class FactorySpriteTile {

	private GameResource sideTrisResource;
	private List<ISpriteTile> cache = new ArrayList<ISpriteTile>();
	private IMoveFinished moveFinished;
	
	public FactorySpriteTile(GameResource sideTrisResource, IMoveFinished moveFinished)
	{
		this.sideTrisResource = sideTrisResource;
		this.moveFinished = moveFinished;
	}
	
	public ISpriteTile createSimpleSpriteTile(int x, int y, TileType type){
		if (type == TileType.NOTHING) return null;
		ISpriteTile result = null;
		for(ISpriteTile cell: cache){
			if(cell.getTileType() == type && !cell.hasParent()){
				result = cell;
				break;
			}
		}
		if (result != null){
			cache.remove(result);
			result.setCell(x, y);
		}
		else{
          if (!type.isSpecial()){
        	  TextureRegion tr = getTexture(type);
        	  if (tr==null) Log.d("FactorySpriteTile", "getTexture(type) == null");
        	  VertexBufferObjectManager vb = sideTrisResource.getActivity().getVertexBufferObjectManager();
        	  if (vb == null) Log.d("FactorySpriteTile", "sideTrisResource.getActivity().getVertexBufferObjectManager() == null");
			  result = new SpriteTile(x, y, tr, vb, type, moveFinished, getSound(type), getCreateSound(type));
          }
          else {
        	  ITiledTextureRegion tiled = getTiledTexture(type);
        	  switch(type){
        	  case BOMB:
        		  result = new BombTile(x,y,tiled, sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        		  break;
        	  case DIAMOND:
        		  result = new DiamondTile(x, y, tiled, sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        		  break;
        	  case MINE:
        		result = new MineTile(x, y, tiled, sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        		break;
        	  case BLACK:
        		  result = new BlackTile(x, y, tiled, sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        		  break;
        	  case SPARK:
        		  result=new SparkTile(x,y,tiled,sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        		  break;
        	  case BOUMERANG:
        		  result=new BoumerangTile(x, y,tiled, sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        		  break;
        	  default:
        		  result = new SawTile(x,y,tiled, sideTrisResource.getActivity().getVertexBufferObjectManager(),type,moveFinished, getSound(type), getCreateSound(type));
        	  }
          	}
		}
		return result;
	}
	
	public ISpriteTile createSpriteTile(int x, int y, TileType type)
	{
		ISpriteTile result = null;
		result = createSimpleSpriteTile(x, y, type);
		if (result == null) return null;
		result.setScale(0.1f);
        result.setVisible(true);
		result.createCell();		
		return result; 
	}
	
	public void disposeSpriteTile(ISpriteTile cell){
		cache.add(cell);
	}

	private Sound getCreateSound(TileType type){
	  	switch(type){
	  	case BOMB: return sideTrisResource.getCreateBomb_sound();
	  	case SAW: return sideTrisResource.getCreateSaw_sound();
        case DIAMOND: return sideTrisResource.getCreateDiamond_sound();
        case MINE: return sideTrisResource.getCreateMine_sound();
        case SPARK:return sideTrisResource.getCreateSpark_sound();
        case BOUMERANG: return sideTrisResource.getCreateBoumerang_sound();
		default: 	return sideTrisResource.getCreate_sound();
	  	}
		
	}
	
	private Sound getSound(TileType type){
	  	switch(type){
	  	case BOMB: return sideTrisResource.getBomb_sound();
	  	case SAW: return sideTrisResource.getSaw_sound();
        case DIAMOND: return sideTrisResource.getDiamond_sound();
        case MINE: return sideTrisResource.getMine_sound();
        case BLACK: return sideTrisResource.getDiamond_sound();
        case SPARK:return sideTrisResource.getSpark_sound();
        case BOUMERANG: return sideTrisResource.getBoumerang_sound();
		case BLUE: 	return sideTrisResource.getBlue_sound();
		case GREEN: return sideTrisResource.getGreen_sound();
		case RED: return sideTrisResource.getRed_sound();
		case YELLOW: return sideTrisResource.getYellow_sound();
		case VIOLET: return sideTrisResource.getViolet_sound();
		case CYAN: return sideTrisResource.getCyan_sound();
		case ORANGE: return sideTrisResource.getOrange_sound();
		case PINK: return sideTrisResource.getPink_sound();
		case DARKGREEN: return sideTrisResource.getDarkgreen_sound();
		case DARKBLUE: return sideTrisResource.getDarkblue_sound();
		case BROWN: return sideTrisResource.getBrown_sound();
  		default: return null;
	  	}
	}
	
	private ITiledTextureRegion getTiledTexture(TileType type)
	{
	  	switch(type){
	  	case BOMB: return sideTrisResource.getTileBomb();
	  	case SAW: return sideTrisResource.getTileSaw();
        case DIAMOND: return sideTrisResource.getTileDiamond();
        case MINE: return sideTrisResource.getTileMine();
        case BLACK: return (ITiledTextureRegion) sideTrisResource.getTileBlack();
        case SPARK:return sideTrisResource.getTileSpark();
        case BOUMERANG: return sideTrisResource.getTileBoumerang();
  		default: return null;
	  	}
	}
  
	private TextureRegion getTexture(TileType type){
		switch(type){
		case BLUE: 	return sideTrisResource.getTileBlue();
		case GREEN: return sideTrisResource.getTileGreen();
		case RED: return sideTrisResource.getTileRed();
		case YELLOW: return sideTrisResource.getTileYellow();
		case VIOLET: return sideTrisResource.getTileViolet();
		case CYAN: return sideTrisResource.getTileCyan();
		case ORANGE: return sideTrisResource.getTileOrange();
		case PINK: return sideTrisResource.getTilePink();
		case DARKGREEN: return sideTrisResource.getTileDarkgreen();
		case DARKBLUE: return sideTrisResource.getTileDarkblue();
		case BROWN: return sideTrisResource.getTileBrown();
		}
		return null;
		
	}
}
