package com.mrvlmor.sidetris.Resource;

import org.andengine.audio.music.MusicFactory;
import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.util.debug.Debug;

import com.mrvlmor.sidetris.MainActivity;

public class GameResource extends SideTrisResource {

	private static Sound blueSound;
	private static Sound bombSound;
	private static Sound boumerangSound;
	private static Sound brownSound;
	private static Sound cyanSound;
	private static Sound darkblueSound;
	private static Sound darkgreenSound;
	private static Sound diamondSound;
	private static Sound greenSound;
	private static Sound mineSound;
	private static Sound orangeSound;
	private static Sound pinkSound;
	private static Sound redSound;
	private static Sound sawSound;
	private static Sound sparkSound;
	private static Sound violetSound;
	private static Sound yellowSound;
	private static Sound moveSound;
	private static Sound createSound;
	private static Sound createBombSound;
	private static Sound createBoumerangSound;
	private static Sound createDiamondSound;
	private static Sound createMineSound;
	private static Sound createSawSound;
	private static Sound createSparkSound;
	private static Sound newRecord;
	private static Sound gameOver;

	private static Object syncroot = new Object();
	private static boolean init = false;
	
	public GameResource(MainActivity activity) {
		super(activity);
		// TODO Auto-generated constructor stub
	}

	public Sound getNewRecord_sound(){
		return newRecord;
	}

	public Sound getGameOver_sound(){
		return gameOver;
	}
	
	public Sound getCreate_sound(){
		return createSound;
	}
	
	public Sound getCreateBomb_sound(){
		return createBombSound;
	}

	public Sound getCreateBoumerang_sound(){
		return createBoumerangSound;
	}

	public Sound getCreateDiamond_sound(){
		return createDiamondSound;
	}

	public Sound getCreateMine_sound(){
		return createMineSound;
	}
	
	public Sound getCreateSaw_sound(){
		return createSawSound;
	}
	
	public Sound getCreateSpark_sound(){
		return createSparkSound;
	}
	
    public Sound getBlue_sound(){
        return blueSound;
    }
	
    public Sound getBomb_sound(){
        return bombSound;
    }
    
    public Sound getBoumerang_sound(){
        return boumerangSound;
    }
    
    public Sound getBrown_sound(){
        return brownSound;
    }
    
    public Sound getCyan_sound(){
        return cyanSound;
    }
    
    public Sound getDarkblue_sound(){
        return darkblueSound;
    }
    
    public Sound getDarkgreen_sound(){
        return darkgreenSound;
    }
    
    public Sound getDiamond_sound(){
        return diamondSound;
    }
    
    public Sound getGreen_sound(){
        return greenSound;
    }
    
    public Sound getMine_sound(){
        return mineSound;
    }
    
    public Sound getOrange_sound(){
        return orangeSound;
    }
    
    public Sound getPink_sound(){
        return pinkSound;
    }
    
    public Sound getRed_sound(){
        return redSound;
    }
    
    public Sound getSaw_sound(){
        return sawSound;
    }
    
    public Sound getSpark_sound(){
        return sparkSound;
    }
    
    public Sound getViolet_sound(){
        return violetSound;
    }
    
    public Sound getYellow_sound(){
        return yellowSound;
    }
    
	public Sound getMove_sound(){
		return moveSound;
	}
    
	public void loadCore() {
		synchronized (syncroot) {
			if (init) return;
			super.loadCore();
			loadMfx();
			init = true;
		}
	
	}
	
	private void loadMfx(){
		SoundFactory.setAssetBasePath("mfx/");
		MusicFactory.setAssetBasePath("mfx/");
		try {
			blueSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "blue.ogg");
			bombSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "bomb.ogg");
			boumerangSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "boumerang.ogg");
			brownSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "brown.ogg");
			cyanSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "cyan.ogg");
			darkblueSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "darkblue.ogg");
			darkgreenSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "darkgreen.ogg");
			diamondSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "diamond.ogg");
			greenSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "green.ogg");
			mineSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "mine.ogg");
			orangeSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "orange.ogg");
			pinkSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "pink.ogg");
			redSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "red.ogg");
			sawSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "saw.ogg");
			sparkSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "spark.ogg");
			violetSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "violet.ogg");
			yellowSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "yellow.ogg");
       	    moveSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "move.ogg");
       	    createSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "create.ogg");
       	    createBombSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "createbomb.ogg");
       	    createBoumerangSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "createboumerang.ogg");
			createDiamondSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "creatediamond.ogg");
			createMineSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "createmine.ogg");
			createSawSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "createsaw.ogg");
			createSparkSound = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "spark.ogg");
			newRecord = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "newrecord.ogg");
			gameOver = SoundFactory.createSoundFromAsset(getActivity().getSoundManager(), getActivity().getApplicationContext(), "gameover.ogg");
       	    
       	    
//       	 	music = MusicFactory.createMusicFromAsset(getActivity().getMusicManager(), getActivity().getApplicationContext(), "2048.mid");

		} catch (final Exception e) {
			Debug.e(e);
		}
	}
	
}
