package com.mrvlmor.sidetris.Resource;

import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.debug.Debug;

import android.graphics.Color;

import com.mrvlmor.machinestate.BaseResource;
import com.mrvlmor.sidetris.MainActivity;

public class SideTrisResource extends BaseResource<MainActivity> {

	private static BuildableBitmapTextureAtlas sideTrisTextureAtlas;
	private static BuildableBitmapTextureAtlas sideTrisBackgroundTextureAtlas;
	private static BuildableBitmapTextureAtlas mainmenuBackgroundTextureAtlas;
	private static BuildableBitmapTextureAtlas gameoverBackgroundTextureAtlas;
	
	private static TextureRegion sideTrisBackground_region;
	private static TextureRegion mainmenuBackground_region;
	private static TextureRegion gameoverBackground_region;
	private static TextureRegion tileRed_region;
	private static TextureRegion tileGreen_region;
	private static TextureRegion tileYellow_region;
	private static TextureRegion tileBlue_region;
	private static TextureRegion tileViolet_region;
	private static TextureRegion tileCyan_region;
	private static TextureRegion tileOrange_region;

	private static TextureRegion tilePink_region;
	private static TextureRegion tileDarkgreen_region;
	private static TextureRegion tileDarkblue_region;
	private static TextureRegion tileBrown_region;
	
	private static ITiledTextureRegion tileBomb_region;
	private static ITiledTextureRegion tileSaw_region;
	private static ITiledTextureRegion tileDiamond_region;
	private static ITiledTextureRegion tileMine_region;
	private static ITiledTextureRegion tileBlack_region;
	private static ITiledTextureRegion tileSpark_region;
	private static ITiledTextureRegion tileBoumerang_region;
	private static ITiledTextureRegion menuSound_region;
	
	private static TextureRegion menu_region;
	private static TextureRegion menuTop_region;
	private static TextureRegion window_region;
	private static TextureRegion windowscore_region;
	private static TextureRegion windowprogress_region;
	private static TextureRegion progressbar_region;
	
	private static TextureRegion help_region;
	private static TextureRegion localMenu_region;
	
	private static BitmapTextureAtlas mDroidFontTexture;
	private static BitmapTextureAtlas mDroidFont4Texture;
	protected static Font fontMenu;
	protected static Font fontCongratulation;
	
	private static boolean init = false;
	
	public SideTrisResource(MainActivity activity) {
		super(activity);
	}

	public ITiledTextureRegion getMenuSound(){
		return menuSound_region;
	}
	
	public ITiledTextureRegion getTileSpark(){
		return tileSpark_region;
	}

	public TextureRegion getGameBackground(){
		return sideTrisBackground_region;
	}
	
	public TextureRegion getMainmenuBackground(){
		return mainmenuBackground_region;
	}
	
	public TextureRegion getGameoverBackground(){
		return gameoverBackground_region;
	}
	
	public TextureRegion getTileRed(){
		return tileRed_region;
	}
	
	public TextureRegion getTileGreen(){
		return tileGreen_region;
	}
	public TextureRegion getTileBlue(){
		return tileBlue_region;
	}
	public TextureRegion getTileYellow(){
		return tileYellow_region;
	}
	public TextureRegion getTileViolet(){
		return tileViolet_region;
	}
	public TextureRegion getTileCyan(){
		return tileCyan_region;
	}
	public TextureRegion getTileOrange(){
		return tileOrange_region;
	}
	public ITiledTextureRegion getTileBlack(){
		return tileBlack_region;
	}
	
	public TextureRegion getTilePink(){
		return tilePink_region;
	}
	
	public TextureRegion getTileDarkgreen(){
		return tileDarkgreen_region;
	}
	
	public TextureRegion getTileDarkblue(){
		return tileDarkblue_region;
	}
	
	public TextureRegion getTileBrown(){
		return tileBrown_region;
	}
		
	public ITiledTextureRegion getTileBoumerang(){
		return tileBoumerang_region;
	}
	
	public ITiledTextureRegion getTileBomb(){
		return tileBomb_region;
	}
	
	public ITiledTextureRegion getTileSaw(){
		return tileSaw_region;
	}
	
	public ITiledTextureRegion getTileDiamond(){
		return tileDiamond_region;
	}
	
	public ITiledTextureRegion getTileMine(){
		return tileMine_region;
	}
	
	public TextureRegion getWindow(){
		return window_region;
	}
	public TextureRegion getWindowScore(){
		return windowscore_region;
	}
	public TextureRegion getWindowProgress(){
		return windowprogress_region;
	}
	
	public TextureRegion getProgressBar(){
		return progressbar_region;
	}
	
	public TextureRegion getMenuBack(){
		return menu_region;
	}
	public TextureRegion getMenuFront(){
		return menuTop_region;
	}
	public Font getFontMenu(){
		return fontMenu;
	}
	public Font getFontCongratulation(){
		return fontCongratulation;
	}
	public TextureRegion getHelp(){
		return help_region;
	}
	public TextureRegion getLocalMenu(){
		return localMenu_region;
	}
	
	@Override
	protected void loadCore() {
		if (init) return;
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/game/");
		
		sideTrisBackgroundTextureAtlas = new BuildableBitmapTextureAtlas(
				getActivity().getTextureManager(), 1024, 2048,
				TextureOptions.BILINEAR);
		
		mainmenuBackgroundTextureAtlas = new BuildableBitmapTextureAtlas(
				getActivity().getTextureManager(), 1024, 2048,
				TextureOptions.BILINEAR);
		
		gameoverBackgroundTextureAtlas = new BuildableBitmapTextureAtlas(
				getActivity().getTextureManager(), 1024, 2048,
				TextureOptions.BILINEAR);
		
		sideTrisTextureAtlas = new BuildableBitmapTextureAtlas(
				getActivity().getTextureManager(), 1024, 1024,
				TextureOptions.BILINEAR);

		sideTrisBackground_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisBackgroundTextureAtlas, getActivity(), "background.png");
		mainmenuBackground_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(mainmenuBackgroundTextureAtlas, getActivity(), "mainmenubackground.png");
		gameoverBackground_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameoverBackgroundTextureAtlas, getActivity(), "gameover.png");
		
		tileRed_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "RedTile.png");
		tileGreen_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "GreenTile.png");
		tileYellow_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "YellowTile.png");
		tileBlue_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "BlueTile.png");
		tileViolet_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "VioletTile.png");
		tileCyan_region =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "CyanTile.png");
		tileOrange_region =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "OrangeTile.png");
		tilePink_region =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "PinkTile.png");
		tileSaw_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameoverBackgroundTextureAtlas, getActivity(), "SawTile.png", 10, 1);
		tileBomb_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sideTrisTextureAtlas, getActivity(), "BombTile.png", 5, 1);
		tileDarkgreen_region =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "DarkgreenTile.png");
		tileDarkblue_region =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "DarkblueTile.png");
		tileBrown_region =  BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "BrownTile.png");
		tileDiamond_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sideTrisTextureAtlas, getActivity(), "DiamondTile.png", 3, 2);
		tileMine_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sideTrisBackgroundTextureAtlas, getActivity(), "MineTile.png", 3, 2);
		tileBlack_region =  BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sideTrisTextureAtlas, getActivity(), "BlackTile.png", 1,1);
		tileSpark_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sideTrisBackgroundTextureAtlas, getActivity(), "SparkTile.png", 6,1);
		tileBoumerang_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(gameoverBackgroundTextureAtlas, getActivity(), "BoumerangTile.png", 1, 1);
				
		menu_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "menuback.png");
		menuTop_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "menufront.png");
		window_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "window.png");
		windowscore_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "windowscore.png");
		help_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "help.png");
		localMenu_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "mainmenu.png");
		windowprogress_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "windowprogress.png");
		progressbar_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(sideTrisTextureAtlas, getActivity(), "progressbar.png");
		menuSound_region = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(sideTrisTextureAtlas, getActivity(), "menusound.png", 2, 1);
		
		FontFactory.setAssetBasePath("gfx/game/");
		
		mDroidFontTexture = new BitmapTextureAtlas(
				getActivity().getTextureManager(), 512, 512);
		mDroidFont4Texture = new BitmapTextureAtlas(
				getActivity().getTextureManager(), 512, 512);

		fontMenu = FontFactory.createFromAsset(getActivity().getFontManager(),
				mDroidFontTexture, getActivity().getAssets(), "roboto.ttf",
				50, true, Color.YELLOW);
		fontMenu.load();
		
		fontCongratulation = FontFactory.createFromAsset(getActivity().getFontManager(),
				mDroidFont4Texture, getActivity().getAssets(), "BIP.ttf",
				80, true, Color.WHITE/*Color.rgb(250, 180, 90)*/);
		fontCongratulation.load();
		
		try {
			sideTrisBackgroundTextureAtlas
			.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
					0, 1, 0));
			mainmenuBackgroundTextureAtlas
			.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
					0, 1, 0));
			gameoverBackgroundTextureAtlas
			.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
					0, 1, 0));
			sideTrisTextureAtlas
			.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
					0, 1, 0));
			
			sideTrisBackgroundTextureAtlas.load();
			sideTrisTextureAtlas.load();
			mainmenuBackgroundTextureAtlas.load();
			gameoverBackgroundTextureAtlas.load();
			
		} catch (TextureAtlasBuilderException e) {
			Debug.e(e);
		}
		
		init = true;
		
	}

	
	
}
