package com.mrvlmor.sidetris;

import android.content.res.Resources;

import com.mrvlmor.machinestate.IHaveSizeCamera;
import com.mrvlmor.machinestate.helper.Languages;

public class Setting implements IHaveSizeCamera {
	
	private static int _height=1440;
	private static int _width=800;
	private static int _verticalOffset=640;
	private static int _horizontalOffset=0;
	private static int _cellWidth=100;
	private static int _cellHeight=100;
	private static Languages _language;
	private static int _heightCamera=0;
	private static int _startCellX=0;
	private static int _minCellX=50;
	//private static int _maxCellX=800-150-100;
	private static int _finishCellX=800-100; 
	private static int _startCellY=0;
	private static int _minCellY=50;
	//private static int _maxCellY=1440-150-100;
	private static int _finishCellY=50+50+100*6;
    private static float _volumeMusic=0.5f;
    private static float _volumeSound=0.8f;
	
	
	public static final int FPS_RUN = 30;
	public static final int FPS_STOP = 18;		
			
	private static final String _Save_Pref = "com.mrvlmor.sidetris.savePref";
	private static final String _Score = "com.mrvlmor.sidetris.score";
	private static final String _Double = "com.mrvlmor.sidetris.double";
	private static final String _Triple = "com.mrvlmor.sidetris.triple";
	private static final String _Quadruple = "com.mrvlmor.sidetris.quadruple";
	private static final String _Quintuple = "com.mrvlmor.sidetris.quintuple";
	private static final String _Hextuple = "com.mrvlmor.sidetris.hextuple";
	private static final String _Empty = "com.mrvlmor.sidetris.empty";
	private static final String _LastChance = "com.mrvlmor.sidetris.lastchance";
	private static final String _Combo = "com.mrvlmor.sidetris.combo";
	private static final String _CurrentDouble = "com.mrvlmor.sidetris.currentDouble";
	private static final String _CurrentTriple = "com.mrvlmor.sidetris.currentTriple";
	private static final String _CurrentQuadruple = "com.mrvlmor.sidetris.currentQuadruple";
	private static final String _CurrentQuintuple = "com.mrvlmor.sidetris.currentQuintuple";
	private static final String _CurrentHextuple = "com.mrvlmor.sidetris.currentHextuple";
	private static final String _CurrentCombo = "com.mrvlmor.sidetris.currentCombo";
	private static final String _CurrentLong = "com.mrvlmor.sidetris.currentLong";
	private static final String _CurrentEmpty = "com.mrvlmor.sidetris.currentEmpty";
	private static final String _CurrentLevel = "com.mrvlmor.sidetris.currentLevel";
	private static final String _CurrentStep = "com.mrvlmor.sidetris.currentStep";
	private static final String _Level = "com.mrvlmor.sidetris.Level";
	private static final String _Long = "com.mrvlmor.sidetris.long";
	private static final String _CurrentLastChance = "com.mrvlmor.sidetris.currentLastchance";
    private static final String _VolumeMusic = "com.mrvlmor.sidetris.volumeMusic";
    private static final String _VolumeSound = "com.mrvlmor.sidetris.volumeSound";
	
	private static final String _CurrentState = "com.mrvlmor.sidetris.currentState";
	private static final String _CurrentScore = "com.mrvlmor.sidetris.currentScore";
	private static final String _Field = "com.mrvlmor.sidetris.field";
	
	private static final int _verticalOffsetButtonGameover = 550;
	private static final int _verticalOffsetGameover1 = 780;
	private static final int _verticalOffsetGameover2 = _verticalOffsetGameover1-100;
		
	private static final int _verticalOffsetButton=-240;
	
	private static final int _horizontalOffsetTitle = 150;
	private static final int _verticalOffsetTitle = 100;
	
	private static final int _heightButton = 150;
	private static final int _widthButton = 300;
	
//	private static final int _verticalOffsetHeaderText=_verticalOffsetButton + 20;
//	private static final int _verticalOffsetScoreText=_verticalOffsetButton + 100;	
	private static final int _horizontalOffsetButtonScore = 50+300+100;
//	private static final int _horizontalOffsetTextScore = _horizontalOffsetButtonScore + 30;
	private static final int _horizontalOffsetButtonBest = 50;
//	private static final int _horizontalOffsetTextBest = _horizontalOffsetButtonBest + 30;
//	private static final int _horizontalTextGameOffset = 20;
//	private static final int _verticalTextGameOffset = _verticalOffsetButton + 240 + 10;
	private static final int _horizontalLocalMenuOffset = 800-100;
	private static final int _horizontalOffsetLevel = 0;
	private static final int _heighLevel = 100;
	private static final int _widthLevel = 100;
	private static final int _verticalLocalMenuOffset = _verticalOffsetTitle; // + (_heightButton/2 - 50);
	
	private static final int _horizontalOffsetWindowProgress = 100;
	private static final int _heightProgress = 120;
	public static final int WidthProgress = 600;

	public Setting(){ 
		
	}
	
	public void setSize(int width, int height, int verticalOffset, int horizontalOffset, int cellWidth, int cellHeight){
		_height = height;
		_width = width;
		_verticalOffset = verticalOffset;
		_horizontalOffset = horizontalOffset;
		_cellWidth = cellWidth;
		_cellHeight = cellHeight;
		 
	}
	
	public void setHeightCamera(int heightCamera){
		_heightCamera = heightCamera;
	}
	
	public void setLanguage(Resources resource, String language){
		_language = new Languages(resource, language);
	}
	
	public int getStartCellX(){
		return _startCellX;
	}
	
	public int getMinCellX(){
		return _minCellX;
	}
	
//	public int getMaxCellX(){
//		return _maxCellX;
//	}
	
	public int getFinishCellX(){
		return _finishCellX;
	}
	
	public int getStartCellY(){
		return _startCellY;
	}
	public int getMinCellY(){
		return _minCellY;
	}
	
//	public int getMaxCellY(){
//		return _maxCellY;
//	}
	
	public int getFinishCellY(){
		return _finishCellY;
	}
	
	public int getCellWidth(){
		return _cellWidth;
	}
	
	public int getCellHeight(){
		return _cellHeight;
	}
	
	public int getVerticalOffset(){
		return _verticalOffset - getGameOffset();
	}

	public int getHorizontalOffset(){
		return _horizontalOffset;
	}
	
	public int getWidthCamera() {
		return _width;
	}

	public int getHeightCamera(){
		return _heightCamera;
	}
	
	public int getGameOffset(){
		return getHeightGame() - getHeightCamera();
	}
	
	public int getHeightGame() {
		return _height;
	}
	
	public int getVerticalButtonOffset(){
		return getVerticalOffset() + _verticalOffsetButton;
	}
	
	public int getVerticalProgressOffset(){
		return getVerticalButtonOffset() + 80;
	}
	
	public int getHorizontalProgressOffset(){
		return _horizontalOffsetWindowProgress;
	}
	
	public int getWidthProgress(){
		return WidthProgress;
	}
	
	public int getHeightProgress(){
		return _heightProgress;
	}
	
	public int getHeightButton(){
		return _heightButton;
	}

	public int getWidthButton(){
		return _widthButton;
	}
	
	public int getVerticalOffsetButtonGameover(){
		return getVerticalOffset() + _verticalOffsetButtonGameover;
	}
	
	public int getVerticalOffsetGameover1(){
		return getVerticalOffsetButtonGameover() -  _verticalOffsetGameover1;
	}
	
	public int getVerticalOffsetGameover2(){
		return getVerticalOffsetButtonGameover() -  _verticalOffsetGameover2;
	}

	public int getVerticalOffsetHeaderResult(){
		return getVerticalOffset() - 90;
	}
	
	public int getVerticalOffsetLongResult(){
		return getVerticalOffsetHeaderResult() + 90;
	}
	
	public int getVerticalOffsetComboResult(){
		return getVerticalOffsetLongResult() + 90;
	}
	
	public int getVerticalOffsetDoubleResult(){
		return getVerticalOffsetComboResult() + 90;
	}
	
	public int getVerticalOffsetTripleResult(){
		return getVerticalOffsetDoubleResult() + 90;
	}
	
	public int getVerticalOffsetQuadrupleResult(){
		return getVerticalOffsetTripleResult() + 90;
	}
	
	public int getVerticalOffsetScoreResult(){
		return getVerticalOffsetQuadrupleResult() + 90;
	}
	
	public int getHorizontalOffsetTypeResult(){
		return 75;
	}
	
	public int getHorizontalOffsetCurrentResult(){
		return 320;
	}

	public int getHorizontalOffsetBestResult(){
		return 535;
	}

	public int getHorizontalOffsetTitle(){
		return _horizontalOffsetTitle;
	}
	
	public int getVerticalOffsetTitle(){
		return _verticalOffsetTitle;
	}
	
	public int getOffsetShadow(){
		return 4;
	}
	
//	public int getVerticalTextHeaderOffset(){
//		return getVerticalOffset() + _verticalOffsetHeaderText;
//	}
//	
//	public int getVerticalTextScoreOffset(){
//		return getVerticalOffset() + _verticalOffsetScoreText;
//	}
//	
	public int getHorizontalButtonBestOffset(){
		return _horizontalOffsetButtonBest;
	}
	
	public int getHorizontalButtonScoreOffset(){
		return _horizontalOffsetButtonScore;
	}
//	
//	public int getHorizontalTextScoreOffset(){
//		return _horizontalOffsetTextScore;
//	}
//	
//	public int getHorizontalTextBestOffset(){
//		return _horizontalOffsetTextBest;
//	}
//	
//	public int getHorizontalTextGameOffset(){
//		return _horizontalTextGameOffset;
//	}
//	
//	public int getVerticalTextGameOffset(){
//		return getVerticalOffset() + _verticalTextGameOffset;
//	}
//	
	public int getHorizontalLocalMenuOffset(){
		return _horizontalLocalMenuOffset;
	}
	
	public int getWidthLevel(){
		return _widthLevel;
	}
	
	public int getHeighLevel(){
		return _heighLevel;
	}
	
	public int getHorizontalLevelOffset(){
		return _horizontalOffsetLevel;
	}
	
	public int getVerticalLocalMenuOffset(){
		return _verticalLocalMenuOffset;
	}
	
    public float getVolumeSound(){
        return _volumeSound;
    }

    public float getVolumeMusic(){
        return _volumeMusic;
    }

    public void setVolumeSound(float volume){
        _volumeSound=volume;
    }

    public void setVolumeMusic(float volume){
        _volumeMusic=volume;
    }

	public Languages getLanguage(){
		return _language;
	}
	
	public String getSavePrefName(){
		return _Save_Pref;
	}
	
	public String getScoreName(){
		return _Score;
	}
	
	public String getDoubleName(){
		return _Double;
	}
	
	public String getTripleName(){
		return _Triple;
	}
	
	public String getQuadrupleName(){
		return _Quadruple;
	}
	
	public String getComboName(){
		return _Combo;
	}	
	
	public String getLongName(){
		return _Long;
	}	
	
	public String getCurrentLongName(){
		return _CurrentLong;
	}
	
	public String getCurrentDoubleName(){
		return _CurrentDouble;
	}
	
	public String getCurrentTripleName(){
		return _CurrentTriple;
	}
	
	public String getCurrentQuadrupleName(){
		return _CurrentQuadruple;
	}
	
	public String getCurrentQuintupleName(){
		return _CurrentQuintuple;
	}
	
	public String getQuintupleName(){
		return _Quintuple;
	}
	
	public String getHextupleName(){
		return _Hextuple;
	}

	public String getCurrentHextupleName(){
		return _CurrentHextuple;
	}

	public String getComboName(int index){
		return String.format("%s_%02d", _Combo, index);
	}	
	
	public String getCurrentComboName(int index){
		return String.format("%s_%02d", _CurrentCombo, index);
	}
	
	public String getCurrentComboName(){
		return _CurrentCombo;
	}
	
	public String getCurrentStateName(){
		return _CurrentState;
	}
	
	public String getCurrentScoreName(){
		return _CurrentScore;
	}
	
	public String getCurrentStepName(){
		return _CurrentStep;
	}
	
	public String getFieldName(int X, int Y){
		return String.format("%s_%02d_%02d", _Field, X, Y) ;
	}

	public String getCurrentEmptyName() {
		return _CurrentEmpty;
	}
	
	public String getEmptyName(){
		return _Empty;
	}
	
	public String getCurrentLevelName() {
		return _CurrentLevel;
	}
	
	public String getCurrentLastChanceName(){
		return _CurrentLastChance;
	}
	
	public String getLastChanceName(){
		return _LastChance;
	}
	
	public String getLevelName(){
		return _Level;
	}
    public String getVolumeMusicName(){
        return _VolumeMusic;
    }
    public String getVolumeSoundName(){
        return _VolumeSound;
    }
}