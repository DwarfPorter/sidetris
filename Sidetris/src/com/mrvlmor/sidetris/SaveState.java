package com.mrvlmor.sidetris;

import com.mrvlmor.sidetris.Entity.TileType;
import com.mrvlmor.sidetris.Entity.Tile.ISpriteTile;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SaveState {
	private Editor editor;
	private Setting setting;
	
	public SaveState(Activity activity){
		setting = new Setting();
		SharedPreferences sp =  activity.getSharedPreferences(setting.getSavePrefName(), android.content.Context.MODE_PRIVATE);
		editor = sp.edit();
	}
	
	public void commit(){
		editor.commit();
	}
	
    public void saveVolumeSound(){
        editor.putFloat(setting.getVolumeSoundName(), setting.getVolumeSound());
    }

   public void saveVolumeMusic(){
        editor.putFloat(setting.getVolumeMusicName(), setting.getVolumeMusic());
    }

	public void saveScore(long score){
		editor.putLong(setting.getScoreName(), score);
	}
	
	public void saveDouble(long score){
		editor.putLong(setting.getDoubleName(), score);
	}
		
	public void saveTriple(long score){
		editor.putLong(setting.getTripleName(), score);
	}
	
	public void saveQuadruple(long score){
		editor.putLong(setting.getQuadrupleName(), score);
	}
	
	public void saveCombo(long score){
		editor.putLong(setting.getComboName(), score);
	}
	
	public void saveCombo(long score, int index){
		editor.putLong(setting.getComboName(index), score);
	}
	
	public void saveCurrentCombo(long score, int index){
		editor.putLong(setting.getCurrentComboName(index), score);
	}
	
	public void saveLong(long score){
		editor.putLong(setting.getLongName(), score);
	}
		
	public void saveCurrentCombo(long score){
		editor.putLong(setting.getCurrentComboName(), score);
	}
	public void saveCurrentQuadruple(long score){
		editor.putLong(setting.getCurrentQuadrupleName(), score);
	}
	
	public void saveCurrentQuintuple(long score){
		editor.putLong(setting.getCurrentQuintupleName(), score);
	}
	
	public void saveQuintuple(long score){
		editor.putLong(setting.getQuintupleName(), score);
	}
	
	public void saveCurrentEmpty(long score){
		editor.putLong(setting.getCurrentEmptyName(), score);
	}
	
	public void saveEmpty(long score){
		editor.putLong(setting.getEmptyName(), score);
	}
	
	public void saveCurrentLevel(long score){
		editor.putLong(setting.getCurrentLevelName(), score);
	}
	public void saveCurrentStep(long score){
		editor.putLong(setting.getCurrentStepName(), score);
	}
	public void saveLevel(long score){
		editor.putLong(setting.getLevelName(), score);
	}
		
	public void saveHextuple(long score){
		editor.putLong(setting.getHextupleName(), score);
	}

	public void saveCurrentHextuple(long score){
		editor.putLong(setting.getCurrentHextupleName(), score);
	}
	
	public void saveLastChance(long score){
		editor.putLong(setting.getLastChanceName(), score);
	}

	public void saveCurrentLastChance(long score){
		editor.putLong(setting.getCurrentLastChanceName(), score);
	}
	
	public void saveCurrentTriple(long score){
		editor.putLong(setting.getCurrentTripleName(), score);
	}
	
	public void saveCurrentDouble(long score){
		editor.putLong(setting.getCurrentDoubleName(), score);
	}

	public void saveCurrentState(int state){
		editor.putInt(setting.getCurrentStateName(), state);
	}
	
	public void saveCurrentScore(long score){
		editor.putLong(setting.getCurrentScoreName(), score);
	}
	
	public void saveCurrentLong(long score){
		editor.putLong(setting.getCurrentLongName(), score);
	}
	
	public void saveField(ISpriteTile[][] field){
		for(int X=0; X < field.length; X++){
			for(int Y=0; Y < field[X].length; Y++){
				if (field[X][Y] == null)
					editor.putInt(setting.getFieldName( X, Y), 0);
				else
				{
					int tile;
					switch(field[X][Y].getTileType()){
					case BLUE: tile = 1; break;
					case GREEN: tile = 2; break;
					case RED: tile = 3; break;
					case YELLOW: tile = 4; break;
					case VIOLET: tile = 5; break;
					case CYAN: tile = 6; break;
					case ORANGE: tile = 7; break; 
					case BLACK: tile = 8; break;
					case PINK: tile = 9; break;
					case DARKGREEN: tile = 10; break;
					case DARKBLUE: tile = 11; break;
					case BROWN: tile = 12; break;
	                case BOMB: tile = 100; break;
	                case SAW: tile = 101; break;
	                case DIAMOND: tile = 102; break;
	                case MINE: tile = 103; break;
	                case SPARK: tile=104;break;
	                case BOUMERANG: tile=105; break;
					default:
						tile = 0;
					}
					editor.putInt(setting.getFieldName( X, Y), tile);
				}
			}
		}
		
	}
}
