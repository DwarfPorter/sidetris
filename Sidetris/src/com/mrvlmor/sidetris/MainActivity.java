package com.mrvlmor.sidetris;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.FillResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.ui.activity.LayoutGameActivity;

import com.mrvlmor.machinestate.helper.FlexibleFPSEngine;
import com.mrvlmor.machinestate.IEventSink;
import com.mrvlmor.machinestate.IHaveEngine;
import com.mrvlmor.machinestate.MachineState;
import com.mrvlmor.machinestate.SplashScene;
import com.mrvlmor.sidetris.Entity.Explode.PlaySound;
import com.mrvlmor.sidetris.States.GameState;
import com.mrvlmor.sidetris.States.MainMenuState;
import com.mrvlmor.sidetris.States.ISave;

import android.os.Bundle;
import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.support.v4.app.NavUtils;

public class MainActivity extends LayoutGameActivity implements IHaveEngine  {
	
	private Camera camera;	
	private int initState = 0;
	private MachineState<Events> machineState;
    private Object syncroot=new Object();
	private FlexibleFPSEngine flexEngine;
	
	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions) 
	{
		flexEngine = new FlexibleFPSEngine(pEngineOptions, Setting.FPS_RUN);
		return flexEngine;
	}
	
	public void setFramesPerSecond(final int pFramesPerSecond) {
		flexEngine.setFramesPerSecond(pFramesPerSecond);
	}
	
	public EngineOptions onCreateEngineOptions() {
		
		final DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(displayMetrics);
		
	    Setting setting = new Setting();
	    //setting.setSize(CAMERA_WIDTH, GAME_HEIGHT, VERTICALOFFSET, HORIZONTALOFFSET, CELL_SIZE, CELL_SIZE);
	    
	    setting.setLanguage(getResources(), getLanguage());
		
	    float coeff = setting.getWidthCamera() / (float) displayMetrics.widthPixels;
	    float height = displayMetrics.heightPixels * coeff;
	    setting.setHeightCamera(Math.round(height));
	    
        RestoreState restoreState = new RestoreState(this);
        restoreState.restoreVolumeSound();
        restoreState.restoreVolumeMusic();


	    camera = new Camera(0, 0, setting.getWidthCamera(), setting.getHeightCamera());
	    
        final EngineOptions engineOptions =
                new EngineOptions( true
                                 , ScreenOrientation.PORTRAIT_FIXED
                                 , new FillResolutionPolicy()
                                 , camera);
        engineOptions.getRenderOptions().setDithering(true);
	    engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
	    engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
	    engineOptions.getTouchOptions().setNeedsMultiTouch(true);

	    return engineOptions;
	}


	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception {
		pOnCreateResourcesCallback.onCreateResourcesFinished();
	}

	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception {

		machineState = new MachineState<Events>(this);
				
		SplashScene splashScene = new SplashScene(this, new Setting());
		
		pOnCreateSceneCallback.onCreateSceneFinished(splashScene);
		
	}

	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
		final MainActivity activity = this;
		mEngine.registerUpdateHandler(new TimerHandler(0.3f,
				new ITimerCallback() {
					public void onTimePassed(final TimerHandler pTimerHandler) {
	
					mEngine.unregisterUpdateHandler(pTimerHandler);
					MainMenuState firstState = new MainMenuState(machineState, activity);
					GameState gameState = new GameState(machineState, activity);
					machineState.addEdge(firstState, Events.PLAY, gameState);
					machineState.addEdge(gameState, Events.BACK, firstState);
					//firstState.setScene(firstState);
					int stateNumber = 0;
					RestoreState restoreState = new RestoreState(activity);
					stateNumber = restoreState.restoreCurrentState();
					switch(stateNumber){
					case 1:
						gameState.setScene(gameState);
						break;
					default:
						firstState.setScene(firstState);
					}
				
					incInitState();
					}
				}));
		incInitState();
		pOnPopulateSceneCallback.onPopulateSceneFinished();
		
	}

	private void incInitState(){
		synchronized (syncroot){
				initState++;
		}
	}
	
	@Override
	protected int getLayoutID() {

		return R.layout.activity_main;
	}

	@Override
	protected int getRenderSurfaceViewID() {
		return R.id.SurfaceViewId;
	}
   
	private int getLanguageID() {
		return R.string.language;
	}

	private String getLanguage() {
		return this.getString(getLanguageID());
	}

	public Camera getCamera() {
		return camera;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (initState!=2) return true; 
		if (keyCode == KeyEvent.KEYCODE_BACK){
			if (machineState.canReturn()){
				machineState.onKeyPressed(keyCode);
			}
			return true;
		}
		
		if (machineState.onKeyPressed(keyCode)){
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		PlaySound.stopPlay();
		save();
	}
	
	public void save(){
		if (machineState == null) return;
		IEventSink<Events> currentState = machineState.getCurrentState();
		if (currentState == null) return;
		String currState = currentState.getClass().getName();
		int stateNumber = 0;
		if (currState.compareTo("com.mrvlmor.sidetris.States.GameState") == 0) stateNumber = 1;
		if (currState.compareTo("com.mrvlmor.sidetris.States.MainMenuState") == 0) stateNumber = 2;
		SaveState saveState = new SaveState(this);
		saveState.saveCurrentState(stateNumber);
		saveState.commit();
		
		if(currentState instanceof ISave){
			((ISave)currentState).save();
		}
	}
	
}